/**
 * @file
 * Contains JavaScript used in Dolphin theme.
 */

(function ($) {

  'use strict';

  // Responsive main menu.
  $('#main-menu').smartmenus();

  // Main menu toggle.
  $('.navbar-toggle').click(function () {
    $('.region-primary-menu').slideToggle();
  });

  // Hide dropdown menu.
  if ($(window).width() < 768) {
    $('.region-primary-menu li a:not(.has-submenu)').click(function () {
      $('.region-primary-menu').hide();
    });
  }

})(jQuery);
