(function ($) {
 Drupal.behaviors.yg_app = {
   attach: function (context, settings) {
	
	'use strict';

	var owlCarousel = function(){

        new WOW().init();

        $('.owl-carousel').owlCarousel({
            items : 4,
            dots : true,
            dotsEach : true,
            loop  : true,
            margin : 170,
            center : true,
            smartSpeed :900,
            nav:true,
            navText: [
                "<i class='fa carousel-left-arrow fa-chevron-left'></i>",
                "<i class='fa carousel-right-arrow fa-chevron-right'></i>"
            ],responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:1,
                    nav:true,
                    margin : 120,
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:true,
                    autoplay: true,
                    autoplayTimeout: 1500,
                    navText: [
                        "<i class='fa carousel-left-arrow fa-chevron-left'></i>",
                        "<i class='fa carousel-right-arrow fa-chevron-right'></i>"
                    ],
                }
            }
        });

	};

    $.fn.goTo = function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 'slow');
        return this; // for chaining...
    }
    if (!$("body").hasClass("path-frontpage")) {
            $(".navbar a.nav-link ").not(".checked").each(function() {
                $(this).addClass("checked");
                var href_attr=$(this).attr("href");
                href_attr.replace("yg_app",'');
                $(this).attr("href", "/yg_app/"+href_attr);
            });
    }
    $(".nav-link").click(function(){

        $('.navbar-collapse').removeClass('show');
        var href_attr=$(this).attr("href");
        $(href_attr).goTo();
    });
    $.fn.clickOff = function(callback, selfDestroy) {
        var clicked = false;
        var parent = this;
        var destroy = selfDestroy || true;
        parent.click(function() {
            clicked = true;
        });     
        $(document).click(function(event) { 
            if (!clicked) {
                callback(parent, event);
            }
            clicked = false;
        });
    };
    $('.navbar-toggler').clickOff(function(){
        $('.navbar-collapse').removeClass('show');
    });

    var socialicon = $( ".social-icons-header.clone" ).clone();
    $(".social-icons-header.clone" ).remove();
    $("#navbarSupportedContent" ).append(socialicon);
    $(".social-icons-header").removeClass('clone');
	
    $(function(){
		owlCarousel();
	});
}}})(jQuery, Drupal);// End of use strict