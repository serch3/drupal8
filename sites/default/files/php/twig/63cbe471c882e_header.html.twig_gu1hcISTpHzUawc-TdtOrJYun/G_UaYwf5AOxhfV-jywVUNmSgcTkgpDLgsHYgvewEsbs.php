<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @electra/includes/header.html.twig */
class __TwigTemplate_c89e071378036dcaf7c56fbf9e17d1da6da084af109919fae3b460b1e95f009a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "if" => 17];
        $filters = ["escape" => 15];
        $functions = ["create_attribute" => 2];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape'],
                ['create_attribute']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- =============================== header ===============================-->
";
        // line 2
        $context["header_attribute"] = $this->env->getExtension('Drupal\Core\Template\TwigExtension')->createAttribute();
        // line 4
        $context["header_classes"] = [0 => "top-header", 1 => "navbar", 2 => "navbar-expand-lg", 3 => $this->getAttribute(        // line 8
($context["electra"] ?? null), "header_link_style", []), 4 => "header-menu", 5 => $this->getAttribute(        // line 10
($context["electra"] ?? null), "header_type", []), 6 => $this->getAttribute(        // line 11
($context["electra"] ?? null), "header_position", [])];
        // line 14
        echo "
<nav ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["header_attribute"] ?? null), "addClass", [0 => ($context["header_classes"] ?? null)], "method")), "html", null, true);
        echo " ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute(($context["header_attribute"] ?? null), "setAttribute", [0 => "data-fixed", 1 => $this->getAttribute(($context["electra"] ?? null), "header_type_sticky", [])], "method"), "setAttribute", [0 => "data-resize", 1 => $this->getAttribute(($context["electra"] ?? null), "header_type_sticky_resize", [])], "method"), "setAttribute", [0 => "data-bgcolor", 1 => $this->getAttribute(($context["electra"] ?? null), "header_color", [])], "method")), "html", null, true);
        echo ">
  <div class=\"container\">
    ";
        // line 17
        if ($this->getAttribute(($context["page"] ?? null), "header_left", [])) {
            // line 18
            echo "      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_left", [])), "html", null, true);
            echo "
    ";
        }
        // line 20
        echo "    <!-- end of navbar-brand -->
    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navigation-group\" aria-controls=\"navigation-group\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
      <span class=\"navbar-toggler-icon\"></span>
    </button>
    <!-- end of navbar-toggler -->
    <div class=\"collapse navbar-collapse\" id=\"navigation-group\">

      <div class=\"navbar-right m-nav\">
        ";
        // line 28
        if ($this->getAttribute(($context["page"] ?? null), "navigation", [])) {
            // line 29
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
            echo "
        ";
        }
        // line 31
        echo "      </div>

      <div class=\"navbar-right m-account\">

        ";
        // line 35
        if ($this->getAttribute(($context["page"] ?? null), "header_right", [])) {
            // line 36
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_right", [])), "html", null, true);
            echo "
        ";
        }
        // line 38
        echo "
      </div>
      <!-- end of navbar-right -->
    </div>
    <!-- end of collapse -->
  </div>
  <!-- end of container -->
</nav>
<!-- end of nav -->
";
    }

    public function getTemplateName()
    {
        return "@electra/includes/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 38,  109 => 36,  107 => 35,  101 => 31,  95 => 29,  93 => 28,  83 => 20,  77 => 18,  75 => 17,  68 => 15,  65 => 14,  63 => 11,  62 => 10,  61 => 8,  60 => 4,  58 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@electra/includes/header.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\electra\\templates\\includes\\header.html.twig");
    }
}
