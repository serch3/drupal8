<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_verb/templates/page--front.html.twig */
class __TwigTemplate_d232629dd8a8c562aeff212898f88d73a2f26a370941332393c36af2e080e8de extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 56];
        $filters = ["escape" => 4, "render" => 56];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape', 'render'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
  <div class=\"gtco-loader\"></div>
  <div id=\"page\">
  ";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "tabs", [])), "html", null, true);
        echo "
  <!-- menu -->
  <div id=\"gtco-offcanvas\"></div>
  <a href=\"#\" class=\"js-gtco-nav-toggle gtco-nav-toggle gtco-nav-white\"><i></i></a>
  <nav class=\"gtco-nav\" role=\"navigation\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-xs-2 text-left\">
          ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "site_branding", [])), "html", null, true);
        echo "
        </div>
        <div class=\"col-xs-10 text-right menu-1\">
          ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
        echo "
        </div>
      </div>
      
    </div>
  </nav>

  <!-- header -->
    <header id=\"gtco-header\" class=\"gtco-cover gtco-cover-sm\" role=\"banner\" style=\"background-image:url('";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bg_image_url"] ?? null)), "html", null, true);
        echo "');\" data-stellar-background-ratio=\"0.5\">
    <div class=\"overlay\"></div>
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-7 text-left\">
          <div class=\"display-t\">
            <div class=\"display-tc animate-box\" data-animate-effect=\"fadeInUp\">
              <span class=\"date-post\">";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_name"] ?? null)), "html", null, true);
        echo "</span>
              <h1 class=\"mb30\"><a href=\"#\">";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "#title", [], "array")), "html", null, true);
        echo "</a></h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- header end -->

  ";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "latest_blog", [])), "html", null, true);
        echo "


  <!-- Footer  -->
  <footer id=\"gtco-footer\" role=\"contentinfo\">
    <div class=\"container\">
      ";
        // line 46
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "most_popular", [])), "html", null, true);
        echo "
      <!-- copyright -->
      <div class=\"row copyright\">
        <div class=\"col-md-12 text-center\">
          <p>
            <small class=\"block\">&copy; 2018 <a href=\"#\">YG Verb</a>. All Rights Reserved.
            </small><small class=\"block\">Designed by <a href=\"http://freehtml5.co/\" target=\"_blank\">FREEHTML5.co </a>Theme By<a href=\"https://www.drupaldevelopersstudio.com/\" target=\"_blank\"> Drupal Developers Studio</a>, A Division of <a href=\"http://www.youngglobes.com\" target=\"_blank\">Young Globes</a></small>
          </p>
          <p>
            <ul class=\"gtco-social-icons\">
              ";
        // line 56
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["twitter_url"] ?? null)))) {
            // line 57
            echo "                <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-twitter\"></i></a></li>
              ";
        }
        // line 59
        echo "              ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["facebook_url"] ?? null)))) {
            // line 60
            echo "                <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-facebook\"></i></a></li>
              ";
        }
        // line 62
        echo "              ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["linkedin_url"] ?? null)))) {
            // line 63
            echo "                <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-linkedin\"></i></a></li>
              ";
        }
        // line 65
        echo "              ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["dribbble_url"] ?? null)))) {
            // line 66
            echo "                <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["dribbble_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-dribbble\"></i></a></li>
              ";
        }
        // line 68
        echo "            </ul>
          </p>
        </div>
      </div>
      <!-- copyright end -->
    </div>
  </footer>
</div>


  <!-- goto-top -->
  <div class=\"gototop js-top\">
    <a href=\"#\" class=\"js-gotop\"><i class=\"icon-arrow-up\"></i></a>
  </div>
";
    }

    public function getTemplateName()
    {
        return "themes/yg_verb/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 68,  165 => 66,  162 => 65,  156 => 63,  153 => 62,  147 => 60,  144 => 59,  138 => 57,  136 => 56,  123 => 46,  114 => 40,  102 => 31,  98 => 30,  88 => 23,  77 => 15,  71 => 12,  60 => 4,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_verb/templates/page--front.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\yg_verb\\templates\\page--front.html.twig");
    }
}
