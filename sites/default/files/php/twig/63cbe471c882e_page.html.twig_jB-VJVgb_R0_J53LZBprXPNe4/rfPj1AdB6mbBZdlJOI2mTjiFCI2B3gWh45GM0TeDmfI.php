<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/sidus_real_estate/templates/layout/page.html.twig */
class __TwigTemplate_9180a91a203db09c39787b2dee03295af5181561e62695e74ba192309ce195c8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 64];
        $filters = ["escape" => 66];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 57
        echo "




  <!-- ======= Top Bar ======= -->
  <!-- Start: Top Bar -->
  ";
        // line 64
        if ($this->getAttribute(($context["page"] ?? null), "topbar", [])) {
            // line 65
            echo "  <section id=\"topbar\" class=\"d-none d-lg-block\">
    ";
            // line 66
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topbar", [])), "html", null, true);
            echo "
  </section>
  ";
        }
        // line 69
        echo "  <!-- End: Top Bar -->

  <!-- ======= Header ======= -->
  <header id=\"header\">
    <div class=\"container d-flex\">

      <div class=\"logo mr-auto\">
        <!-- Start: Header -->
        ";
        // line 77
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 78
            echo "      \t    <div>
          ";
            // line 79
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
            </div>
        ";
        }
        // line 82
        echo "        <!-- End: Header --> 
      </div>

        <!-- Start: Primary Menu -->
        ";
        // line 86
        if ($this->getAttribute(($context["page"] ?? null), "primary_menu", [])) {
            // line 87
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
        ";
        }
        // line 89
        echo "        <!-- End: Primary Menu -->         
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <!-- Start: Slider Section -->
  ";
        // line 95
        if ($this->getAttribute(($context["page"] ?? null), "slider", [])) {
            // line 96
            echo "  <section id=\"hero\">
    <div class=\"hero-container\">
      <div id=\"heroCarousel\" class=\"carousel slide carousel-fade\" data-ride=\"carousel\">

        <ol class=\"carousel-indicators\" id=\"hero-carousel-indicators\"></ol>

        <div class=\"carousel-inner\" role=\"listbox\">
          ";
            // line 103
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slider", [])), "html", null, true);
            echo "          
        </div>

        <a class=\"carousel-control-prev\" href=\"#heroCarousel\" role=\"button\" data-slide=\"prev\">
          <span class=\"carousel-control-prev-icon icofont-rounded-left\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Previous</span>
        </a>

        <a class=\"carousel-control-next\" href=\"#heroCarousel\" role=\"button\" data-slide=\"next\">
          <span class=\"carousel-control-next-icon icofont-rounded-right\" aria-hidden=\"true\"></span>
          <span class=\"sr-only\">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->
  ";
        }
        // line 120
        echo "  <!-- End: Slider Section -->



  <main id=\"main\">
    
     <!-- ======= Breadcrumbs ======= -->
    <!--- Start: Breadcrumb -->
    ";
        // line 128
        if ($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])) {
            // line 129
            echo "    <section id=\"breadcrumbs\" class=\"breadcrumbs\">
      <div class=\"container\">
        ";
            // line 131
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
            echo "
        <h2>";
            // line 132
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "#title", [], "array")), "html", null, true);
            echo "</h2>
      </div>
    </section>
    ";
        }
        // line 136
        echo "    <!-- End: Breadcrumb -->
    <!-- End Breadcrumbs -->



    <!-- ======= Featured Section ======= -->
    <section id=\"featured\" class=\"featured\">
      <div class=\"container\">

        <div class=\"row\">
          
          <!--- Start: Top First Container -->
          ";
        // line 148
        if ($this->getAttribute(($context["page"] ?? null), "content_top_first", [])) {
            // line 149
            echo "          <div class=\"col-lg-4\">
            ";
            // line 150
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top_first", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 153
        echo "          <!-- End: Top First Container -->

          <!--- Start: Top Second Container -->
          ";
        // line 156
        if ($this->getAttribute(($context["page"] ?? null), "content_top_second", [])) {
            // line 157
            echo "          <div class=\"col-lg-4 mt-4 mt-lg-0\">
            ";
            // line 158
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top_second", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 161
        echo "          <!-- End: Top Second Container -->

          <!--- Start: Top Third Container -->
          ";
        // line 164
        if ($this->getAttribute(($context["page"] ?? null), "content_top_third", [])) {
            // line 165
            echo "          <div class=\"col-lg-4 mt-4 mt-lg-0\">
            ";
            // line 166
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top_third", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 169
        echo "          <!-- End: Top Third Container -->

        </div>

      </div>
    </section><!-- End Featured Section -->

    <section id=\"about1\" class=\"about1\">
      <!--- Start: content -->
      ";
        // line 178
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 179
            echo "         <div class=\"container\">
            ";
            // line 180
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 183
        echo "      <!---End: content -->
     </section>

    <div class=\"clearfix\"></div>


    <!-- ======= Services Section ======= -->
    <!-- Start: General Section Container -->
    ";
        // line 191
        if ($this->getAttribute(($context["page"] ?? null), "general_sections", [])) {
            // line 192
            echo "    <section id=\"services\" class=\"services\">
      <div class=\"container\">
        ";
            // line 194
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "general_sections", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 198
        echo "    <!-- End: General Section Container -->
    <!-- End Services Section -->

    <!-- ======= Clients Section ======= -->
    <!-- Start: Our Clients -->
    ";
        // line 203
        if ($this->getAttribute(($context["page"] ?? null), "clients", [])) {
            // line 204
            echo "    <section id=\"clients\" class=\"clients\">
      <div class=\"container\">
        ";
            // line 206
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "clients", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 210
        echo "    <!-- End: Our Clients -->
    <!-- End Clients Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id=\"footer\">
    <div class=\"footer-top\">
      <div class=\"container\">
        <div class=\"row\">
          
          <!-- Start: Footer First Container -->
          ";
        // line 222
        if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
            // line 223
            echo "          <div class=\"col-lg-3 col-md-6 footer-links\">
              ";
            // line 224
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 227
        echo "          <!-- End: Footer First Container -->

          <!-- Start: Footer Second Container -->
          ";
        // line 230
        if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
            // line 231
            echo "          <div class=\"col-lg-3 col-md-6 footer-links\">
            ";
            // line 232
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 235
        echo "          <!-- End: Footer Second Container -->

          <!-- Start: Footer Third Container -->
          ";
        // line 238
        if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
            // line 239
            echo "          <div class=\"col-lg-3 col-md-6 footer-contact\">
            ";
            // line 240
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 243
        echo "          <!-- End: Footer Third Container -->

          <!-- Start: Footer Forth Container -->
          ";
        // line 246
        if ($this->getAttribute(($context["page"] ?? null), "footer_forth", [])) {
            // line 247
            echo "          <div class=\"col-lg-3 col-md-6 footer-info\">
            ";
            // line 248
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_forth", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 251
        echo "          <!-- End: Footer Forth Container -->

        </div>
      </div>
    </div>

   <!-- Start: Footer Copyright -->
   ";
        // line 258
        if ($this->getAttribute(($context["page"] ?? null), "footer_copyright", [])) {
            // line 259
            echo "    <div class=\"container\">
      ";
            // line 260
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_copyright", [])), "html", null, true);
            echo " 
    </div>
   ";
        }
        // line 263
        echo "   <!-- End: Footer Copyright -->

  </footer><!-- End Footer -->

  <a href=\"#\" class=\"back-to-top\"><i class=\"icofont-simple-up\"></i></a>

 
";
    }

    public function getTemplateName()
    {
        return "themes/sidus_real_estate/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  391 => 263,  385 => 260,  382 => 259,  380 => 258,  371 => 251,  365 => 248,  362 => 247,  360 => 246,  355 => 243,  349 => 240,  346 => 239,  344 => 238,  339 => 235,  333 => 232,  330 => 231,  328 => 230,  323 => 227,  317 => 224,  314 => 223,  312 => 222,  298 => 210,  291 => 206,  287 => 204,  285 => 203,  278 => 198,  271 => 194,  267 => 192,  265 => 191,  255 => 183,  249 => 180,  246 => 179,  244 => 178,  233 => 169,  227 => 166,  224 => 165,  222 => 164,  217 => 161,  211 => 158,  208 => 157,  206 => 156,  201 => 153,  195 => 150,  192 => 149,  190 => 148,  176 => 136,  169 => 132,  165 => 131,  161 => 129,  159 => 128,  149 => 120,  129 => 103,  120 => 96,  118 => 95,  110 => 89,  104 => 87,  102 => 86,  96 => 82,  90 => 79,  87 => 78,  85 => 77,  75 => 69,  69 => 66,  66 => 65,  64 => 64,  55 => 57,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/sidus_real_estate/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\sidus_real_estate\\templates\\layout\\page.html.twig");
    }
}
