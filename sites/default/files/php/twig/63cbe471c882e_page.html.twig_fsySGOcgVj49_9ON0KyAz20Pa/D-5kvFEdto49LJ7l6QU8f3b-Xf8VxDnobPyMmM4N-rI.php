<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_app/templates/layout/page.html.twig */
class __TwigTemplate_9175bf134e01ae86a6f0baf625873f32db9253225e94da68fa37e88dae962650 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 13, "set" => 24];
        $filters = ["escape" => 6, "render" => 25];
        $functions = ["url" => 24];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'set'],
                ['escape', 'render'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
<div id=\"page-wrap\">

    <nav class=\"navbar navbar-expand-lg main-navbar-nav navbar-light fixed-top\">
      <div class=\"container\">
        ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "logo", [])), "html", null, true);
        echo "
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
          <span class=\"navbar-toggler-icon\"></span>
        </button>
        ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
        echo "
      </div>
      <div class=\"social-icons-header clone\">
        ";
        // line 13
        if (($context["facebook"] ?? null)) {
            // line 14
            echo "          <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook"] ?? null)), "html", null, true);
            echo "\"><i class=\"fab fa-facebook-f\"></i></a>
        ";
        }
        // line 16
        echo "        ";
        if (($context["instagram"] ?? null)) {
            // line 17
            echo "          <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram"] ?? null)), "html", null, true);
            echo "\"><i class=\"fab fa-instagram\"></i></a>
        ";
        }
        // line 19
        echo "        ";
        if (($context["twitter"] ?? null)) {
            // line 20
            echo "          <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter"] ?? null)), "html", null, true);
            echo "\"><i class=\"fab fa-twitter\"></i></a>
        ";
        }
        // line 22
        echo "      </div>
    </nav>
  ";
        // line 24
        $context["url"] = $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<current>");
        // line 25
        echo "  ";
        if (twig_in_filter("user", $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["url"] ?? null))))) {
            echo "  
    <div id=\"fh5co-hero-wrapper\" class=\"user\" style=\"background-image: url(";
            // line 26
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_image"] ?? null)), "html", null, true);
            echo ");\" >
      <div class=\"container fh5co-hero-inner\">
        <h1 class=\"animated fadeIn wow\" data-wow-delay=\"0.4s\">";
            // line 28
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "#title", [], "array")), "html", null, true);
            echo "</h1>    
      </div>
    </div>
  ";
        }
        // line 31
        echo "  
    ";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "


  <footer class=\"footer-outer\">
    <div class=\"container footer-inner\">

      <div class=\"footer-three-grid wow fadeIn animated\" data-wow-delay=\"0.66s\">
        <div class=\"column-1-3\">
          <h1>";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_title"] ?? null)), "html", null, true);
        echo "</h1>
        </div>
        <div class=\"column-2-3\">
          <nav class=\"footer-nav\">
            ";
        // line 44
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
          </nav>
        </div>
        <div class=\"column-3-3\">
          <div class=\"social-icons-footer\">
            ";
        // line 49
        if (($context["facebook"] ?? null)) {
            // line 50
            echo "              <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook"] ?? null)), "html", null, true);
            echo "\"><i class=\"fab fa-facebook-f\"></i></a>
            ";
        }
        // line 52
        echo "            ";
        if (($context["instagram"] ?? null)) {
            // line 53
            echo "              <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram"] ?? null)), "html", null, true);
            echo "\"><i class=\"fab fa-instagram\"></i></a>
            ";
        }
        // line 55
        echo "            ";
        if (($context["twitter"] ?? null)) {
            // line 56
            echo "              <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter"] ?? null)), "html", null, true);
            echo "\"><i class=\"fab fa-twitter\"></i></a>
            ";
        }
        // line 58
        echo "          </div>
        </div>
      </div>

      <span class=\"border-bottom-footer\"></span>

      <p class=\"copyright\">&copy; 2018 YG App. All rights reserved.<br> Design by <a href=\"https://freehtml5.co\" target=\"_blank\">FreeHTML5</a>.Theme By <a href=\"https://www.drupaldeveloperstudio.com\" target=\"_blank\" class=\"font_14_roboto\">Drupal Developers Studio</a>. A Division of <a href=\"https://www.youngglobes.com\" target=\"_blank\" class=\"font_14_roboto\">Young Globes.</a></p>

    </div>
  </footer>
</div>";
    }

    public function getTemplateName()
    {
        return "themes/yg_app/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 58,  173 => 56,  170 => 55,  164 => 53,  161 => 52,  155 => 50,  153 => 49,  145 => 44,  138 => 40,  127 => 32,  124 => 31,  117 => 28,  112 => 26,  107 => 25,  105 => 24,  101 => 22,  95 => 20,  92 => 19,  86 => 17,  83 => 16,  77 => 14,  75 => 13,  69 => 10,  62 => 6,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_app/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\yg_app\\templates\\layout\\page.html.twig");
    }
}
