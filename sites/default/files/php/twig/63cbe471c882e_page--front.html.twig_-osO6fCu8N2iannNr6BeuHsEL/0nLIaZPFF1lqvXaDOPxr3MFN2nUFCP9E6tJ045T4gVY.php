<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/photogenictheme/templates/page--front.html.twig */
class __TwigTemplate_ffa777c2dd50e0bcc121e3a5a0a13d61b69fd363f321a27ec93900b0c62e8f98 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 86, "set" => 156];
        $filters = ["escape" => 58, "raw" => 202];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'set'],
                ['escape', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 54
        echo "<div class=\"templatemo-top-bar\" id=\"templatemo-top\">
    <div class=\"container\">
        <div class=\"subheader\">
            <div id=\"phone\" class=\"pull-left\">
                <img src=\"";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_contact_image"] ?? null)), "html", null, true);
        echo "\"/>
                ";
        // line 59
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_contact"] ?? null)), "html", null, true);
        echo "
            </div>
            <div id=\"email\" class=\"pull-right\">
                <img src=\"";
        // line 62
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_email_image"] ?? null)), "html", null, true);
        echo "\"/>
                ";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_email"] ?? null)), "html", null, true);
        echo "
            </div>
        </div>
    </div>
</div>

<div class=\"templatemo-top-menu\">
    <div class=\"container\">
        <!-- Static navbar -->
        <div class=\"navbar navbar-default\" role=\"navigation\">
            <div class=\"container\">
                <div class=\"navbar-header\">
                    ";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    </button>
                </div>
                <div class=\"navbar-collapse collapse\" id=\"templatemo-nav-bar\">
                    <ul class=\"nav navbar-nav navbar-right\">
                        <!-- Home -->
                        ";
        // line 86
        if ($this->getAttribute(($context["page"] ?? null), "main_navigation", [])) {
            // line 87
            echo "                            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_navigation", [])), "html", null, true);
            echo "
                        ";
        }
        // line 88
        echo " 
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </div><!--/.navbar -->
    </div> <!-- /container -->
</div>
        

<!-- Carousel -->
<div id=\"templatemo-carousel\" class=\"carousel slide\" data-ride=\"carousel\">
    <!-- Indicators -->
    <ol class=\"carousel-indicators\">
        <li data-target=\"#templatemo-carousel\" data-slide-to=\"0\" class=\"active\"></li>
        <li data-target=\"#templatemo-carousel\" data-slide-to=\"1\"></li>
        <li data-target=\"#templatemo-carousel\" data-slide-to=\"2\"></li>
    </ol>
    <div class=\"carousel-inner\">
        <div class=\"item active\">
            <div class=\"container\">
                <div class=\"carousel-caption\">
                    <h1>";
        // line 109
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["first_banner_heading"] ?? null)), "html", null, true);
        echo "</h1>
                    <p>";
        // line 110
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["first_banner_description"] ?? null)), "html", null, true);
        echo "</p>
                    <p><a class=\"btn btn-lg btn-green\" href=\"";
        // line 111
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["first_banner_button_one_link_url"] ?? null)), "html", null, true);
        echo "\" role=\"button\" style=\"margin: 20px;\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["first_banner_button_one_link_title"] ?? null)), "html", null, true);
        echo "</a> 
                        <a class=\"btn btn-lg btn-orange\" href=\"";
        // line 112
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["first_banner_button_two_link_url"] ?? null)), "html", null, true);
        echo "\" role=\"button\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["first_banner_button_two_link_title"] ?? null)), "html", null, true);
        echo "</a></p>
                </div>
            </div>
        </div>
        
        <div class=\"item\">
            <div class=\"container\">
                    <div class=\"carousel-caption\">
                        <div class=\"col-sm-6 col-md-6\">
                            <h1>";
        // line 121
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["second_banner_heading_one"] ?? null)), "html", null, true);
        echo "</h1>
                            <p>";
        // line 122
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["second_banner_description_one"] ?? null)), "html", null, true);
        echo "</p>
                        </div>
                        <div class=\"col-sm-6 col-md-6\">
                            <h1>";
        // line 125
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["second_banner_heading_two"] ?? null)), "html", null, true);
        echo "</h1>
                            <p>";
        // line 126
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["second_banner_description_two"] ?? null)), "html", null, true);
        echo "</p>
                        </div>
                    </div>
            </div>
        </div>
        <div class=\"item\">
            <div class=\"container\">
                <div class=\"carousel-caption\">
                    <h1>";
        // line 134
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["third_banner_heading"] ?? null)), "html", null, true);
        echo "</h1>
                    <div class=\"col-sm-12 col-md-12\">
                        <p>";
        // line 136
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["third_banner_description"] ?? null)), "html", null, true);
        echo "</p>
                    <p><a class=\"btn btn-lg btn-orange\" href=\"";
        // line 137
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["third_banner_button_link_url"] ?? null)), "html", null, true);
        echo "\" role=\"button\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["third_banner_button_link_title"] ?? null)), "html", null, true);
        echo "</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class=\"left carousel-control\" href=\"#templatemo-carousel\" data-slide=\"prev\"><span class=\"glyphicon glyphicon-chevron-left\"></span></a>
    <a class=\"right carousel-control\" href=\"#templatemo-carousel\" data-slide=\"next\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a>
</div><!-- /#templatemo-carousel -->

<!-- main content section start -->
<div class=\"templatemo-welcome\" id=\"templatemo-welcome\">
    <div class=\"container\">
        <div class=\"templatemo-slogan text-center\">
            <span class=\"txt_darkgrey\">";
        // line 151
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["welcome_heading_one"] ?? null)), "html", null, true);
        echo " </span><span class=\"txt_orange\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["welcome_heading_two"] ?? null)), "html", null, true);
        echo "</span>
            <p class=\"txt_slogan\"><i>";
        // line 152
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["welcome_description"] ?? null)), "html", null, true);
        echo "</i></p>
        </div>
        <div class=\"row\">
            ";
        // line 155
        if (($this->getAttribute(($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) {
            // line 156
            echo "                ";
            $context["primary_col"] = 6;
            // line 157
            echo "            ";
        } elseif (($this->getAttribute(($context["page"] ?? null), "sidebar_first", []) || $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) {
            // line 158
            echo "                ";
            $context["primary_col"] = 9;
            // line 159
            echo "            ";
        } else {
            // line 160
            echo "                ";
            $context["primary_col"] = 12;
            // line 161
            echo "            ";
        }
        // line 162
        echo "            
            ";
        // line 163
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 164
            echo "                <div id=\"sidebar-first\" class=\"col-md-3\">
                    <aside class=\"section\">
                        ";
            // line 166
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
                    </aside>
                </div>
            ";
        }
        // line 170
        echo "            
            <div class=\"";
        // line 171
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("content-area col-md-" . $this->sandbox->ensureToStringAllowed(($context["primary_col"] ?? null))), "html", null, true);
        echo "\">
                ";
        // line 172
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
            </div>

            ";
        // line 175
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 176
            echo "                <div id=\"sidebar-second\" class=\"col-md-3\">
                    <aside class=\"section\">
                        ";
            // line 178
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
                    </aside>
                </div>
            ";
        }
        // line 182
        echo "        </div>  
    </div>
</div>

<div class=\"templatemo-service\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"templatemo-line-header\">
                <div class=\"text-center\">
                    <hr class=\"team_hr team_hr_left hr_gray\"/><span class=\"txt_darkgrey\">";
        // line 191
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_heading"] ?? null)), "html", null, true);
        echo "</span>
                    <hr class=\"team_hr team_hr_right hr_gray\"/>
                </div>
            </div>
            <div class=\"col-md-4\">
                <div class=\"templatemo-service-item\">
                    <div>
                        <img src=\"";
        // line 198
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_one_image"] ?? null)), "html", null, true);
        echo "\" alt=\"icon\"/>
                        <span class=\"templatemo-service-item-header\">";
        // line 199
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_one_heading"] ?? null)), "html", null, true);
        echo "</span>
                    </div>
                    ";
        // line 201
        if (($context["service_one_description"] ?? null)) {
            // line 202
            echo "                        <p>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["service_one_description"] ?? null)));
            echo "</p>
                    ";
        } else {
            // line 204
            echo "                        ";
            if (($context["service_one_sort_description"] ?? null)) {
                // line 205
                echo "                            <p>";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["service_one_sort_description"] ?? null)));
                echo "</p>
                        ";
            }
            // line 207
            echo "                    ";
        }
        // line 208
        echo "                    <div class=\"text-left\">
                        <a href=\"";
        // line 209
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_one_button_link_url"] ?? null)), "html", null, true);
        echo "\" class=\"templatemo-btn-read-more btn btn-orange\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_one_button_link_title"] ?? null)), "html", null, true);
        echo "</a>
                    </div>
                    <br class=\"clearfix\"/>
                </div>
                <div class=\"clearfix\"></div>
            </div>
            
            <div class=\"col-md-4\">
                <div class=\"templatemo-service-item\">
                    <div>
                        <img src=\"";
        // line 219
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_two_image"] ?? null)), "html", null, true);
        echo "\" alt=\"icon\"/>
                        <span class=\"templatemo-service-item-header\">";
        // line 220
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_two_heading"] ?? null)), "html", null, true);
        echo "</span>
                    </div>
                    ";
        // line 222
        if (($context["service_two_description"] ?? null)) {
            // line 223
            echo "                        <p>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["service_two_description"] ?? null)));
            echo "</p>
                    ";
        } else {
            // line 225
            echo "                        ";
            if (($context["service_two_sort_description"] ?? null)) {
                // line 226
                echo "                            <p>";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["service_two_sort_description"] ?? null)));
                echo "</p>
                        ";
            }
            // line 228
            echo "                    ";
        }
        // line 229
        echo "                    <div class=\"text-left\">
                        <a href=\"";
        // line 230
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_two_button_link_url"] ?? null)), "html", null, true);
        echo "\" class=\"templatemo-btn-read-more btn btn-orange\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_two_button_link_title"] ?? null)), "html", null, true);
        echo "</a>
                    </div>
                    <br class=\"clearfix\"/>
                </div>                
            </div>
            
            <div class=\"col-md-4\">
                <div class=\"templatemo-service-item\">
                    <div>
                        <img src=\"";
        // line 239
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_three_image"] ?? null)), "html", null, true);
        echo "\" alt=\"icon\"/>
                        <span class=\"templatemo-service-item-header\">";
        // line 240
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_three_heading"] ?? null)), "html", null, true);
        echo "</span>
                    </div>
                    ";
        // line 242
        if (($context["service_three_description"] ?? null)) {
            // line 243
            echo "                        <p>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["service_three_description"] ?? null)));
            echo " </p>
                    ";
        } else {
            // line 245
            echo "                        ";
            if (($context["service_three_sort_description"] ?? null)) {
                // line 246
                echo "                            <p>";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["service_three_sort_description"] ?? null)));
                echo "</p>
                        ";
            }
            // line 248
            echo "                    ";
        }
        // line 249
        echo "                    <div class=\"text-left\">
                        <a href=\"";
        // line 250
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_three_button_link_url"] ?? null)), "html", null, true);
        echo "\" class=\"templatemo-btn-read-more 
                        btn btn-orange\">";
        // line 251
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["service_three_button_link_title"] ?? null)), "html", null, true);
        echo "</a>
                    </div>
                    <br class=\"clearfix\"/>
                </div>
                <br class=\"clearfix\"/>
            </div>
        </div>
    </div>
</div>

<div class=\"templatemo-team\" id=\"templatemo-about\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"templatemo-line-header\">
                <div class=\"text-center\">
                    <hr class=\"team_hr team_hr_left\"/><span>";
        // line 266
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_heading"] ?? null)), "html", null, true);
        echo "</span>
                    <hr class=\"team_hr team_hr_right\" />
                </div>
            </div>
        </div>
        <div class=\"clearfix\"></div>
            <ul class=\"row row_team\">
                <li class=\"col-lg-3 col-md-3 col-sm-6 \">
                    <div class=\"text-center\">
                        <div class=\"member-thumb\">
                            <img src=\"";
        // line 276
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_one_person_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"member 1\" />
                        </div>
                        <div class=\"team-inner\">
                            <p class=\"team-inner-header\">";
        // line 279
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_one_person_name"] ?? null)), "html", null, true);
        echo "</p>
                            <p class=\"team-inner-subtext\">";
        // line 280
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_one_person_profession"] ?? null)), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </li>
                <li class=\"col-lg-3 col-md-3 col-sm-6 \">
                    <div class=\"text-center\">
                        <div class=\"member-thumb\">
                            <img src=\"";
        // line 287
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_two_person_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"member 2\" />
                        </div>
                        <div class=\"team-inner\">
                            <p class=\"team-inner-header\">";
        // line 290
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_two_person_name"] ?? null)), "html", null, true);
        echo "</p>
                            <p class=\"team-inner-subtext\">";
        // line 291
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_two_person_profession"] ?? null)), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </li>
                <li class=\"col-lg-3 col-md-3 col-sm-6 \">
                    <div class=\"text-center\">
                        <div class=\"member-thumb\">
                            <img src=\"";
        // line 298
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_three_person_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"member 3\" />
                        </div>
                        <div class=\"team-inner\">
                            <p class=\"team-inner-header\">";
        // line 301
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_three_person_name"] ?? null)), "html", null, true);
        echo "</p>
                            <p class=\"team-inner-subtext\">";
        // line 302
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_three_person_profession"] ?? null)), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </li>
                <li class=\"col-lg-3 col-md-3 col-sm-6 \">
                    <div class=\"text-center\">
                        <div class=\"member-thumb\">
                            <img src=\"";
        // line 309
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_four_person_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"member 4\" />
                        </div>
                        <div class=\"team-inner\">
                            <p class=\"team-inner-header\">";
        // line 312
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_four_person_name"] ?? null)), "html", null, true);
        echo "</p>
                            <p class=\"team-inner-subtext\">";
        // line 313
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["team_four_person_profession"] ?? null)), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </li>
            </ul>
    </div>
</div><!-- /.templatemo-team -->

<div id=\"templatemo-portfolio\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"templatemo-line-header\">
                <div class=\"text-center\">
                    <hr class=\"team_hr team_hr_left hr_gray\"/><span class=\"txt_darkgrey\">";
        // line 326
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_heading"] ?? null)), "html", null, true);
        echo "</span>
                    <hr class=\"team_hr team_hr_right hr_gray\"/>
                </div>
            </div>
            <div class=\"clearfix\"></div>
            <div class=\"templatemo-gallery-category\" style=\"font-size:16px; margin-top:40px;\">
                <div class=\"text-center\">
                    <a class=\"active\" href=\".gallery\">";
        // line 333
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_menu_one"] ?? null)), "html", null, true);
        echo "</a> / <a href=\".gallery-design\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_menu_two"] ?? null)), "html", null, true);
        echo "</a> / <a href=\".gallery-graphic\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_menu_three"] ?? null)), "html", null, true);
        echo "</a> / <a href=\".gallery-inspiration\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_menu_four"] ?? null)), "html", null, true);
        echo "</a> / <a href=\".gallery-creative\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_menu_five"] ?? null)), "html", null, true);
        echo "</a>                           
                </div>
            </div>
        </div> <!-- /.row -->


        <div class=\"clearfix\"></div>
        <div class=\"text-center\">
            <ul class=\"templatemo-project-gallery\">
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-graphic\">
                    <a class=\"colorbox\" href=\"";
        // line 343
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_one_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-graphic\">
                        <div class=\"templatemo-project-box\">

                            <img src=\"";
        // line 346
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_one_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>

                            <div class=\"project-overlay\">
                                <h5>";
        // line 349
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_one_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 351
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_one_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-creative\">
                    <a class=\"colorbox\" href=\"";
        // line 357
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_two_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-creative\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 360
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_two_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 363
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_two_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 365
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_two_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-inspiration\">
                    <a class=\"colorbox\" href=\"";
        // line 371
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_three_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-inspiration\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 374
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_three_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\" />
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 377
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_three_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr />
                                <h4>";
        // line 379
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_three_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-design\">
                    <a class=\"colorbox\" href=\"";
        // line 385
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_four_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-design\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 388
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_four_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 391
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_four_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 393
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_four_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-inspiration\">
                    <a class=\"colorbox\" href=\"";
        // line 399
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_five_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-inspiration\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 402
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_five_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 405
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_five_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 407
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_five_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-inspiration\">
                    <a class=\"colorbox\" href=\"";
        // line 413
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_six_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-inspiration\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 416
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_six_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 419
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_six_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 421
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_six_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>               
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-design\">
                    <a class=\"colorbox\" href=\"";
        // line 427
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_seven_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-design\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 430
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_seven_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 433
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_seven_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 435
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_seven_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>               
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-creative\">
                    <a class=\"colorbox\" href=\"";
        // line 441
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_eight_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-creative\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 444
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_eight_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 447
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_eight_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 449
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_eight_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-graphic\">
                    <a class=\"colorbox\" href=\"";
        // line 455
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_nine_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-graphic\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 458
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_nine_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 461
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_nine_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 463
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_nine_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
                <li class=\"col-lg-2 col-md-2 col-sm-2 gallery gallery-inspiration\">
                    <a class=\"colorbox\" href=\"";
        // line 469
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_ten_full_image_link_url"] ?? null)), "html", null, true);
        echo "\" data-group=\"gallery-inspiration\">
                        <div class=\"templatemo-project-box\">
                            
                            <img src=\"";
        // line 472
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_ten_image"] ?? null)), "html", null, true);
        echo "\" class=\"img-responsive\" alt=\"gallery\"/>
                            
                            <div class=\"project-overlay\">
                                <h5>";
        // line 475
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_ten_overlay_heading_one"] ?? null)), "html", null, true);
        echo "</h5>
                                <hr/>
                                <h4>";
        // line 477
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_ten_overlay_heading_two"] ?? null)), "html", null, true);
        echo "</h4>
                            </div>
                        </div>
                    </a>
                </li>
            </ul><!-- /.gallery -->
        </div>
        <div class=\"clearfix\"></div>
        <div class=\"row text-center\">
            <a class=\"btn_loadmore btn btn-lg btn-orange\" href=\"";
        // line 486
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_button_link_url"] ?? null)), "html", null, true);
        echo "\" role=\"button\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["portfolio_button_link_title"] ?? null)), "html", null, true);
        echo "</a>
        </div>
    </div><!-- /.container -->
</div> <!-- /.templatemo-portfolio -->

<div id=\"templatemo-contact\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"templatemo-line-header head_contact\">
                <div class=\"text-center\">
                    <hr class=\"team_hr team_hr_left hr_gray\"/><span class=\"txt_darkgrey\">";
        // line 496
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_heading"] ?? null)), "html", null, true);
        echo "</span>
                    <hr class=\"team_hr team_hr_right hr_gray\"/>
                </div>
            </div>
            
            <div class=\"col-md-12\">
                <p>";
        // line 502
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_description"] ?? null)), "html", null, true);
        echo "</p>
                <p><img src=\"";
        // line 503
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_address_icon"] ?? null)), "html", null, true);
        echo "\" alt=\"icon 1\" />";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_address"] ?? null)), "html", null, true);
        echo "</p>
                <p><img src=\"";
        // line 504
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_phone_icon"] ?? null)), "html", null, true);
        echo "\"  alt=\"icon 2\" />";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_phone_number"] ?? null)), "html", null, true);
        echo "</p>
                <form class=\"form-horizontal contact-center\" action=\"#\">
                    <div class=\"form-group\">
                        <input type=\"email\" class=\"form-control\" placeholder=\"Enter Your Name\" maxlength=\"40\" />
                    </div>
                    <div class=\"form-group\">
                        <input type=\"email\" class=\"form-control\" placeholder=\"Enter Your Email\" maxlength=\"40\" />
                    </div>
                    <div class=\"form-group\">
                        <textarea  class=\"form-control\" style=\"height: 130px;\" placeholder=\"Write down your message\"></textarea>
                    </div>
                    <button type=\"submit\" class=\"btn btn-orange\">";
        // line 515
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contact_button_title"] ?? null)), "html", null, true);
        echo "</button>
                </form>               
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /#templatemo-contact -->

<div class=\"templatemo-tweets\">
    <div class=\"container\">
        <div class=\"row\" style=\"margin-top:20px;\">
                <div class=\"col-md-1\">
                    <img src=\"";
        // line 526
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["tweet_image"] ?? null)), "html", null, true);
        echo "\" alt=\"icon\" />
                </div>
                <div class=\"col-md-11 tweet_txt\" >
                    <span>";
        // line 529
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["tweet_message"] ?? null)), "html", null, true);
        echo "</span>
                    <br/>
                    <span class=\"twitter_user\">";
        // line 531
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["tweet_user"] ?? null)), "html", null, true);
        echo "</span>
                </div>
                
                <div class=\"col-md-12\">
                    ";
        // line 535
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured", [])), "html", null, true);
        echo "
                </div>

         </div><!-- /.row -->
    </div><!-- /.container -->
</div>

<div class=\"templatemo-footer\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-4 col-sm-4\">
                <div class=\"footer_container\">
                    ";
        // line 547
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_one", [])), "html", null, true);
        echo " 
                </div>
            </div>

            <div class=\"col-md-4 col-sm-4\">
                <div class=\"footer_container\">
                    ";
        // line 553
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_two", [])), "html", null, true);
        echo "
                    <ul class=\"list-inline\">
                        <li>
                            <a href=\"";
        // line 556
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_facebook"] ?? null)), "html", null, true);
        echo "\">
                                <span class=\"social-icon-fb\"></span>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 561
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_twitter"] ?? null)), "html", null, true);
        echo "\">
                                <span class=\"social-icon-twitter\"></span>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 566
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_linkedin"] ?? null)), "html", null, true);
        echo "\">
                                <span class=\"social-icon-linkedin\"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class=\"col-md-4 col-sm-4\">
                <div class=\"footer_container\">
                    ";
        // line 576
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_three", [])), "html", null, true);
        echo "
                </div>
            </div>                
        </div>
    </div>
</div>

<footer class=\"copyright-section\">
    <div class=\"container text-center\">
        <div class=\"copyright-info\">
            <span>Copyright © 2020 Photogenic Theme. All Rights Reserved.</span>
        </div>
    </div><!-- /.container -->
</footer>";
    }

    public function getTemplateName()
    {
        return "themes/photogenictheme/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  992 => 576,  979 => 566,  971 => 561,  963 => 556,  957 => 553,  948 => 547,  933 => 535,  926 => 531,  921 => 529,  915 => 526,  901 => 515,  885 => 504,  879 => 503,  875 => 502,  866 => 496,  851 => 486,  839 => 477,  834 => 475,  828 => 472,  822 => 469,  813 => 463,  808 => 461,  802 => 458,  796 => 455,  787 => 449,  782 => 447,  776 => 444,  770 => 441,  761 => 435,  756 => 433,  750 => 430,  744 => 427,  735 => 421,  730 => 419,  724 => 416,  718 => 413,  709 => 407,  704 => 405,  698 => 402,  692 => 399,  683 => 393,  678 => 391,  672 => 388,  666 => 385,  657 => 379,  652 => 377,  646 => 374,  640 => 371,  631 => 365,  626 => 363,  620 => 360,  614 => 357,  605 => 351,  600 => 349,  594 => 346,  588 => 343,  567 => 333,  557 => 326,  541 => 313,  537 => 312,  531 => 309,  521 => 302,  517 => 301,  511 => 298,  501 => 291,  497 => 290,  491 => 287,  481 => 280,  477 => 279,  471 => 276,  458 => 266,  440 => 251,  436 => 250,  433 => 249,  430 => 248,  424 => 246,  421 => 245,  415 => 243,  413 => 242,  408 => 240,  404 => 239,  390 => 230,  387 => 229,  384 => 228,  378 => 226,  375 => 225,  369 => 223,  367 => 222,  362 => 220,  358 => 219,  343 => 209,  340 => 208,  337 => 207,  331 => 205,  328 => 204,  322 => 202,  320 => 201,  315 => 199,  311 => 198,  301 => 191,  290 => 182,  283 => 178,  279 => 176,  277 => 175,  271 => 172,  267 => 171,  264 => 170,  257 => 166,  253 => 164,  251 => 163,  248 => 162,  245 => 161,  242 => 160,  239 => 159,  236 => 158,  233 => 157,  230 => 156,  228 => 155,  222 => 152,  216 => 151,  197 => 137,  193 => 136,  188 => 134,  177 => 126,  173 => 125,  167 => 122,  163 => 121,  149 => 112,  143 => 111,  139 => 110,  135 => 109,  112 => 88,  106 => 87,  104 => 86,  90 => 75,  75 => 63,  71 => 62,  65 => 59,  61 => 58,  55 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/photogenictheme/templates/page--front.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\photogenictheme\\templates\\page--front.html.twig");
    }
}
