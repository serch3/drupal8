<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/sidus_real_estate/templates/navigation/menu.html.twig */
class __TwigTemplate_e316cb6434f4100fe701aca5670f2603202d9de322be74e727901b82207affe7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["import" => 24, "macro" => 32, "if" => 34, "for" => 40, "set" => 42];
        $filters = ["escape" => 36];
        $functions = ["link" => 52];

        try {
            $this->sandbox->checkSecurity(
                ['import', 'macro', 'if', 'for', 'set'],
                ['escape'],
                ['link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 21
        echo "
<nav class=\"nav-menu d-none d-lg-block\">

";
        // line 24
        $context["menus"] = $this;
        // line 25
        echo "
";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0));
        echo "

";
        // line 62
        echo "</nav>


<!--header id=\"header\">
    <div class=\"container d-flex\">

      <div class=\"logo mr-auto\">
        <h1 class=\"text-light\"><a href=\"index.html\"><span>Eterna</span></a></h1>
       
         <a href=\"index.html\"><img src=\"themes/custom/alextoby/img/logo.png\" alt=\"\" class=\"img-fluid\"></a>
      </div>

      <nav class=\"nav-menu d-none d-lg-block\">
        <ul>
          <li class=\"active\"><a href=\"index.html\">Home</a></li>

          <li class=\"drop-down\"><a href=\"#\">About</a>
            <ul>
              <li><a href=\"about.html\">About Us</a></li>
              <li><a href=\"team.html\">Team</a></li>

              <li class=\"drop-down\"><a href=\"#\">Drop Down 2</a>
                <ul>
                  <li><a href=\"#\">Deep Drop Down 1</a></li>
                  <li><a href=\"#\">Deep Drop Down 2</a></li>
                  <li><a href=\"#\">Deep Drop Down 3</a></li>
                  <li><a href=\"#\">Deep Drop Down 4</a></li>
                  <li><a href=\"#\">Deep Drop Down 5</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li><a href=\"services.html\">Services</a></li>
          <li><a href=\"portfolio.html\">Portfolio</a></li>
          <li><a href=\"pricing.html\">Pricing</a></li>
          <li><a href=\"blog.html\">Blog</a></li>
          <li><a href=\"contact.html\">Contact</a></li>

        </ul>
      </nav>

    </div>
  </header-->






 <!--header id=\"header\" class=\"fixed-top \">
    <div class=\"container-fluid d-flex align-items-center justify-content-between\">

      <h1 class=\"logo\"><a href=\"#\">Company</a></h1-->
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href=\"index.html\" class=\"logo\"><img src=\"themes/custom/contours/img/logo.png\" alt=\"\" class=\"img-fluid\"></a>-->

      <!--nav class=\"nav-menu d-none d-lg-block\">
        <ul>
          <li class=\"active\"><a href=\"#\">Home</a></li>
          <li><a href=\"about\">About</a></li>
          <li><a href=\"services\">Services</a></li>
          <li><a href=\"portfolio\">Portfolio</a></li>
          <li><a href=\"team\">Team</a></li>
          <li class=\"drop-down\"><a href=\"\">Drop Down</a>
            <ul>
              <li><a href=\"#\">Drop Down 1</a></li>
              <li class=\"drop-down\"><a href=\"#\">Deep Drop Down</a>
                <ul>
                  <li><a href=\"#\">Deep Drop Down 1</a></li>
                  <li><a href=\"#\">Deep Drop Down 2</a></li>
                  <li><a href=\"#\">Deep Drop Down 3</a></li>
                  <li><a href=\"#\">Deep Drop Down 4</a></li>
                  <li><a href=\"#\">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href=\"#\">Drop Down 2</a></li>
              <li><a href=\"#\">Drop Down 3</a></li>
              <li><a href=\"#\">Drop Down 4</a></li>
            </ul>
          </li>
          <li><a href=\"#contact\">Contact</a></li>

        </ul>
      </nav--><!-- .nav-menu -->

      <!--a href=\"#about\" class=\"get-started-btn scrollto\">Get Started</a>

    </div>
  </header--><!-- End Header -->
";
    }

    // line 32
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 33
            echo "  ";
            $context["menus"] = $this;
            // line 34
            echo "  ";
            if (($context["items"] ?? null)) {
                // line 35
                echo "    ";
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 36
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "menu"], "method")), "html", null, true);
                    echo ">
    ";
                } else {
                    // line 38
                    echo "      <ul class=\"menu\">
    ";
                }
                // line 40
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 41
                    echo "      ";
                    // line 42
                    $context["classes"] = [0 => "menu-item", 1 => (($this->getAttribute(                    // line 44
$context["item"], "is_expanded", [])) ? ("menu-item--expanded drop-down") : ("")), 2 => (($this->getAttribute(                    // line 45
$context["item"], "is_collapsed", [])) ? ("menu-item--collapsed") : ("")), 3 => (($this->getAttribute(                    // line 46
$context["item"], "in_active_trail", [])) ? ("menu-item--active-trail") : (""))];
                    // line 49
                    echo "      
      <li";
                    // line 50
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
                    echo ">
      <!--li";
                    // line 51
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => "drop-down"], "method")), "html", null, true);
                    echo "-->
        ";
                    // line 52
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", []))), "html", null, true);
                    echo "
        ";
                    // line 53
                    if ($this->getAttribute($context["item"], "below", [])) {
                        // line 54
                        echo "          ";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)));
                        echo "
        ";
                    }
                    // line 56
                    echo "      </li>
      
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "    </ul>
  ";
            }
        } catch (\Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (\Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/sidus_real_estate/templates/navigation/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 59,  232 => 56,  226 => 54,  224 => 53,  220 => 52,  216 => 51,  212 => 50,  209 => 49,  207 => 46,  206 => 45,  205 => 44,  204 => 42,  202 => 41,  197 => 40,  193 => 38,  187 => 36,  184 => 35,  181 => 34,  178 => 33,  164 => 32,  70 => 62,  65 => 30,  62 => 25,  60 => 24,  55 => 21,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/sidus_real_estate/templates/navigation/menu.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\sidus_real_estate\\templates\\navigation\\menu.html.twig");
    }
}
