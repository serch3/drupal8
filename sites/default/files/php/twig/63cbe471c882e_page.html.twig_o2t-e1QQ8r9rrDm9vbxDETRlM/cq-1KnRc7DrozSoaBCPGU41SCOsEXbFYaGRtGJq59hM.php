<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/druppio_monopage/templates/system/page.html.twig */
class __TwigTemplate_5b6c04d2221bdd40ec74d50f261e2932e2086a20e2239f6f66656e1fd44c7985 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'action_links' => [$this, 'block_action_links'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 64, "if" => 67, "block" => 68];
        $filters = ["escape" => 115, "clean_class" => 73, "t" => 90];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape', 'clean_class', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 64
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "fluid_container", [])) ? ("container-fluid") : ("container"));
        // line 66
        echo "
";
        // line 67
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", []))) {
            // line 68
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 112
        echo "
";
        // line 114
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "slideshow", [])) {
            // line 115
            echo "        ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slideshow", [])), "html", null, true);
            echo "
";
        }
        // line 117
        echo "

        ";
        // line 120
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "top_bar", [])) {
            // line 121
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_bar", [])), "html", null, true);
            echo "
        ";
        }
        // line 123
        echo "
        ";
        // line 125
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "content_top", [])) {
            // line 126
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top", [])), "html", null, true);
            echo "
        ";
        }
        // line 128
        echo "
 ";
        // line 130
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "content_bottom", [])) {
            // line 131
            echo "<div class=\"content-bottom-wrapper\">
    <div class=\"content-bottom-inner\">
        <div class=\"container\">
            <div class=\"col-sm-12\">

            ";
            // line 136
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom", [])), "html", null, true);
            echo "

            </div>
        </div>
      </div>
    </div>
";
        }
        // line 143
        echo "
";
        // line 145
        $this->displayBlock('main', $context, $blocks);
        // line 223
        echo "
 ";
        // line 225
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "bottom_bar", [])) {
            // line 226
            echo "<div class=\"container\">
    <div class=\"col-sm-12\">

            ";
            // line 229
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_bar", [])), "html", null, true);
            echo "

    </div>
</div>
";
        }
        // line 234
        echo "


";
        // line 237
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 238
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
    }

    // line 68
    public function block_navbar($context, array $blocks = [])
    {
        // line 69
        echo "    ";
        // line 70
        $context["navbar_classes"] = [0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 72
($context["theme"] ?? null), "settings", []), "navbar_inverse", [])) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 73
($context["theme"] ?? null), "settings", []), "navbar_position", [])) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "navbar_position", []))))) : (($context["container"] ?? null)))];
        // line 76
        echo "

    <header class=\"navbar navbar-default navbar-fixed-top\" id=\"navbar\" role=\"banner\">
      <div class=\"container\">
      ";
        // line 81
        echo "
          <div class=\"navbar-header col-sm-4\">
              <div class=\"col-xs-8 col-sm-12\">
            ";
        // line 84
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
                  </div>
            ";
        // line 87
        echo "            ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 88
            echo "                  <div class=\"col-xs-4\">
              <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">";
            // line 90
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
              </button>
                      </div>
            ";
        }
        // line 97
        echo "          </div>


          ";
        // line 101
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 102
            echo "            <div class=\"navbar-collapse collapse navbar-right col-sm-8\">
              ";
            // line 103
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 106
        echo "
           </div>
    </header>

  ";
    }

    // line 145
    public function block_main($context, array $blocks = [])
    {
        // line 146
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " js-quickedit-main-content\">
    <div class=\"row\">
      ";
        // line 149
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 150
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 155
            echo "      ";
        }
        // line 156
        echo "
      ";
        // line 158
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 159
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 164
            echo "      ";
        }
        // line 165
        echo "
      ";
        // line 167
        echo "      ";
        // line 168
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 169
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 170
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 171
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 172
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 175
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 178
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 179
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 182
            echo "        ";
        }
        // line 183
        echo "
        ";
        // line 185
        echo "        ";
        if (($context["breadcrumb"] ?? null)) {
            // line 186
            echo "          ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 189
            echo "        ";
        }
        // line 190
        echo "
        ";
        // line 192
        echo "        ";
        if (($context["action_links"] ?? null)) {
            // line 193
            echo "          ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 196
            echo "        ";
        }
        // line 197
        echo "
        ";
        // line 199
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 200
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 203
            echo "        ";
        }
        // line 204
        echo "
        ";
        // line 206
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 210
        echo "      </section>

      ";
        // line 213
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 214
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 219
            echo "      ";
        }
        // line 220
        echo "    </div>
  </div>
";
    }

    // line 150
    public function block_header($context, array $blocks = [])
    {
        // line 151
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 152
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 159
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 160
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 161
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 179
    public function block_highlighted($context, array $blocks = [])
    {
        // line 180
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 186
    public function block_breadcrumb($context, array $blocks = [])
    {
        // line 187
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["breadcrumb"] ?? null)), "html", null, true);
        echo "
          ";
    }

    // line 193
    public function block_action_links($context, array $blocks = [])
    {
        // line 194
        echo "            <ul class=\"action-links\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["action_links"] ?? null)), "html", null, true);
        echo "</ul>
          ";
    }

    // line 200
    public function block_help($context, array $blocks = [])
    {
        // line 201
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 206
    public function block_content($context, array $blocks = [])
    {
        // line 207
        echo "          <a id=\"main-content\"></a>
          ";
        // line 208
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 214
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 215
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 216
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 238
    public function block_footer($context, array $blocks = [])
    {
        // line 239
        echo "<div class=\"footer-wrapper\">
    <div class=\"footer-inner\">
    <footer class=\"footer ";
        // line 241
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" role=\"contentinfo\">
         <div class=\"row footer-top\">
             ";
        // line 243
        if ((($this->getAttribute(($context["page"] ?? null), "footer", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", []))) {
            // line 244
            echo "             <div class=\"col-sm-5\">
                ";
            // line 245
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
                  </div>
             <div class=\"col-sm-5 col-sm-push-2\">
                ";
            // line 248
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
            echo "
                 </div>
             <div class=\"col-sm-12\">
                ";
            // line 251
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
            echo "
                 </div>
             ";
        }
        // line 254
        echo "             ";
        if ($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])) {
            // line 255
            echo "             <div class=\"col-sm-12\">
                    ";
            // line 256
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])), "html", null, true);
            echo "
                 </div>
             ";
        }
        // line 259
        echo "             </div>
            <div class=\"row footer-cc\">
             ";
        // line 261
        if (($this->getAttribute(($context["page"] ?? null), "footer_credits", []) || $this->getAttribute(($context["page"] ?? null), "footer_copyright", []))) {
            // line 262
            echo "             <div class=\"col-sm-6\">
                    ";
            // line 263
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_credits", [])), "html", null, true);
            echo "
                 </div>
             <div class=\"col-sm-6\">
                 ";
            // line 266
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_copyright", [])), "html", null, true);
            echo "
             </div>
             ";
        }
        // line 269
        echo "         </div>
    </footer>
    </div>
</div>
  ";
    }

    public function getTemplateName()
    {
        return "themes/druppio_monopage/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  517 => 269,  511 => 266,  505 => 263,  502 => 262,  500 => 261,  496 => 259,  490 => 256,  487 => 255,  484 => 254,  478 => 251,  472 => 248,  466 => 245,  463 => 244,  461 => 243,  456 => 241,  452 => 239,  449 => 238,  442 => 216,  439 => 215,  436 => 214,  430 => 208,  427 => 207,  424 => 206,  417 => 201,  414 => 200,  407 => 194,  404 => 193,  397 => 187,  394 => 186,  387 => 180,  384 => 179,  377 => 161,  374 => 160,  371 => 159,  364 => 152,  361 => 151,  358 => 150,  352 => 220,  349 => 219,  346 => 214,  343 => 213,  339 => 210,  336 => 206,  333 => 204,  330 => 203,  327 => 200,  324 => 199,  321 => 197,  318 => 196,  315 => 193,  312 => 192,  309 => 190,  306 => 189,  303 => 186,  300 => 185,  297 => 183,  294 => 182,  291 => 179,  288 => 178,  282 => 175,  280 => 172,  279 => 171,  278 => 170,  277 => 169,  276 => 168,  274 => 167,  271 => 165,  268 => 164,  265 => 159,  262 => 158,  259 => 156,  256 => 155,  253 => 150,  250 => 149,  244 => 146,  241 => 145,  233 => 106,  227 => 103,  224 => 102,  221 => 101,  216 => 97,  206 => 90,  202 => 88,  199 => 87,  194 => 84,  189 => 81,  183 => 76,  181 => 73,  180 => 72,  179 => 70,  177 => 69,  174 => 68,  168 => 238,  166 => 237,  161 => 234,  153 => 229,  148 => 226,  145 => 225,  142 => 223,  140 => 145,  137 => 143,  127 => 136,  120 => 131,  117 => 130,  114 => 128,  108 => 126,  105 => 125,  102 => 123,  96 => 121,  93 => 120,  89 => 117,  83 => 115,  80 => 114,  77 => 112,  73 => 68,  71 => 67,  68 => 66,  66 => 64,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/druppio_monopage/templates/system/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\druppio_monopage\\templates\\system\\page.html.twig");
    }
}
