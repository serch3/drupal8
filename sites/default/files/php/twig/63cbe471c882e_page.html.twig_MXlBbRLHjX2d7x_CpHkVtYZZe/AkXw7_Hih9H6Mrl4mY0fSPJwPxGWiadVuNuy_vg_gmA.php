<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/ebiz/templates/layout/page.html.twig */
class __TwigTemplate_9c6376a877100020283351ef7ff7056a524a82c71fcecb05266adf2ea8985a00 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 55, "set" => 150];
        $filters = ["escape" => 57, "striptags" => 81, "t" => 82];
        $functions = ["attach_library" => 73];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'set'],
                ['escape', 'striptags', 't'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 52
        echo "<div id=\"page\">
  <header id=\"masthead\" class=\"site-header container\" role=\"banner\">
    <div class=\"row\">
      ";
        // line 55
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 56
            echo "        <div id=\"logo\" class=\"site-branding col-sm-6\">
          ";
            // line 57
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 60
        echo "      <div class=\"col-sm-6 mainmenu\">
        <div class=\"mobilenavi\"></div>
        <nav id=\"navigation\" role=\"navigation\">
          <div id=\"main-menu\">
            ";
        // line 64
        if ($this->getAttribute(($context["page"] ?? null), "main_navigation", [])) {
            // line 65
            echo "                ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_navigation", [])), "html", null, true);
            echo "
            ";
        }
        // line 67
        echo "          </div>
        </nav>
      </div>
    </div>
  </header>
  ";
        // line 72
        if (($context["is_front"] ?? null)) {
            // line 73
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("ebiz_grazitti/slider-js"), "html", null, true);
            echo "
    ";
            // line 74
            if (($context["slideshow_display"] ?? null)) {
                // line 75
                echo "      <div id=\"slidebox\" class=\"flexslider\">
        <ul class=\"slides\">
          <li>
            <img src=\"";
                // line 78
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["slide1_image"] ?? null)), "html", null, true);
                echo "\"/>
            ";
                // line 79
                if ((($context["slide1_head"] ?? null) || ($context["slide1_desc"] ?? null))) {
                    // line 80
                    echo "              <div class=\"flex-caption\">
                <h2>";
                    // line 81
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide1_head"] ?? null))), "html", null, true);
                    echo "</h2>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide1_desc"] ?? null))), "html", null, true);
                    echo "
                <a class=\"frmore\" href=\"";
                    // line 82
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide1_url"] ?? null))), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Read More"));
                    echo "</a>
              </div>
            ";
                }
                // line 85
                echo "          </li>
          <li>
            <img src=\"";
                // line 87
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["slide2_image"] ?? null)), "html", null, true);
                echo "\"/>
            ";
                // line 88
                if ((($context["slide2_head"] ?? null) || ($context["slide2_desc"] ?? null))) {
                    // line 89
                    echo "              <div class=\"flex-caption\">
                <h2>";
                    // line 90
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide2_head"] ?? null))), "html", null, true);
                    echo "</h2>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide2_desc"] ?? null))), "html", null, true);
                    echo "
                <a class=\"frmore\" href=\"";
                    // line 91
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide2_url"] ?? null))), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Read More"));
                    echo "</a>
              </div>
            ";
                }
                // line 94
                echo "          </li>
          <li>
            <img src=\"";
                // line 96
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["slide3_image"] ?? null)), "html", null, true);
                echo "\"/>
            ";
                // line 97
                if ((($context["slide3_head"] ?? null) || ($context["slide3_desc"] ?? null))) {
                    // line 98
                    echo "              <div class=\"flex-caption\">
                <h2>";
                    // line 99
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide3_head"] ?? null))), "html", null, true);
                    echo "</h2>";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide3_desc"] ?? null))), "html", null, true);
                    echo "
                <a class=\"frmore\" href=\"";
                    // line 100
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["slide3_url"] ?? null))), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Read More"));
                    echo "</a>
              </div>
            ";
                }
                // line 103
                echo "          </li>
        </ul><!-- /slides -->
        <div class=\"doverlay\"></div>
      </div>
    ";
            }
            // line 108
            echo "  ";
        }
        // line 109
        echo "
  ";
        // line 110
        if ((($this->getAttribute(($context["page"] ?? null), "featured_first", []) || $this->getAttribute(($context["page"] ?? null), "featured_second", [])) || $this->getAttribute(($context["page"] ?? null), "featured_third", []))) {
            // line 111
            echo "    <div id=\"featured-area\">
      <div class=\"container\">
        <div class=\"row\">
          ";
            // line 114
            if ($this->getAttribute(($context["page"] ?? null), "featured_first", [])) {
                // line 115
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("featured-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["featured_col"] ?? null))), "html", null, true);
                echo "\">
              ";
                // line 116
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 119
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "featured_second", [])) {
                // line 120
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("featured-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["featured_col"] ?? null))), "html", null, true);
                echo "\">
              ";
                // line 121
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 124
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "featured_third", [])) {
                // line 125
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("featured-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["featured_col"] ?? null))), "html", null, true);
                echo "\">
              ";
                // line 126
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 129
            echo "        </div>
      </div>
    </div>
  ";
        }
        // line 133
        echo "
  ";
        // line 134
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 135
            echo "    <div id=\"highlighted-block\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-sm-12\">
            ";
            // line 139
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
          </div>
        </div>
      </div>
    </div>
  ";
        }
        // line 145
        echo "
  <div id=\"main-content\">
    <div class=\"container\">
      <div class=\"row\">
        ";
        // line 149
        if (($this->getAttribute(($context["page"] ?? null), "left_sidebar", []) && $this->getAttribute(($context["page"] ?? null), "right_sidebar", []))) {
            // line 150
            echo "          ";
            $context["primary_col"] = 6;
            // line 151
            echo "        ";
        } elseif (($this->getAttribute(($context["page"] ?? null), "left_sidebar", []) || $this->getAttribute(($context["page"] ?? null), "right_sidebar", []))) {
            // line 152
            echo "          ";
            $context["primary_col"] = 9;
            // line 153
            echo "        ";
        } else {
            // line 154
            echo "          ";
            $context["primary_col"] = 12;
            // line 155
            echo "        ";
        }
        // line 156
        echo "        ";
        // line 157
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "left_sidebar", [])) {
            // line 158
            echo "          <aside id=\"sidebar\" class=\"col-sm-3 col-md-3\" role=\"complementary\">
          ";
            // line 159
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "left_sidebar", [])), "html", null, true);
            echo "
          </aside>
        ";
        }
        // line 162
        echo "        ";
        // line 163
        echo "        <div id=\"primary\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("content-area col-sm-" . $this->sandbox->ensureToStringAllowed(($context["primary_col"] ?? null))), "html", null, true);
        echo "\">
          <section id=\"content\" role=\"main\" class=\"clearfix\">
            ";
        // line 165
        if (($context["show_breadcrumbs"] ?? null)) {
            // line 166
            echo "              ";
            if (($context["breadcrumb"] ?? null)) {
                // line 167
                echo "                <div id=\"breadcrumbs\">
                  ";
                // line 168
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["breadcrumb"] ?? null)), "html", null, true);
                echo "
                </div>
              ";
            }
            // line 171
            echo "            ";
        }
        // line 172
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["messages"] ?? null)), "html", null, true);
        echo "
            ";
        // line 173
        if ($this->getAttribute(($context["page"] ?? null), "content_top", [])) {
            // line 174
            echo "              <div id=\"content_top\">
                ";
            // line 175
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top", [])), "html", null, true);
            echo "
              </div>
            ";
        }
        // line 178
        echo "            <div id=\"content-wrap\">
              ";
        // line 179
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
            </div>
            ";
        // line 181
        if ($this->getAttribute(($context["page"] ?? null), "content_bottom", [])) {
            // line 182
            echo "              <div id=\"content_bottom\">
                ";
            // line 183
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom", [])), "html", null, true);
            echo "
              </div>
            ";
        }
        // line 186
        echo "          </section>
        </div>
        ";
        // line 189
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "right_sidebar", [])) {
            // line 190
            echo "          <aside id=\"sidebar\" class=\"col-sm-3 col-md-3\" role=\"complementary\">
           ";
            // line 191
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "right_sidebar", [])), "html", null, true);
            echo "
          </aside>
        ";
        }
        // line 194
        echo "      </div>
    </div>
  </div>

  ";
        // line 198
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 199
            echo "    <div id=\"footer-block\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-sm-12\">
            ";
            // line 203
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
          </div>
        </div>
      </div>
    </div>
  ";
        }
        // line 209
        echo "
  ";
        // line 210
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", [])) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", []))) {
            // line 211
            echo "    <div id=\"bottom\">
      <div class=\"container\">
        <div class=\"row\">
          ";
            // line 214
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
                // line 215
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
              ";
                // line 216
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 219
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
                // line 220
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
              ";
                // line 221
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 224
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
                // line 225
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
              ";
                // line 226
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 229
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])) {
                // line 230
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
              ";
                // line 231
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 234
            echo "        </div>
      </div>
    </div>
  ";
        }
        // line 238
        echo "
  <footer id=\"colophon\" class=\"site-footer\" role=\"contentinfo\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"fcred col-sm-12\">
          ";
        // line 243
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Copyright"));
        echo " &copy; ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["this_year"] ?? null)), "html", null, true);
        echo ", <a href=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["front_page"] ?? null)), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_name"] ?? null)), "html", null, true);
        echo "</a>. ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Theme by"));
        echo " <a href=\"http://www.grazitti.com\" target=\"_blank\">Grazitti Interactive</a>.
        </div> 
      </div>
    </div>
  </footer>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/ebiz/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  487 => 243,  480 => 238,  474 => 234,  468 => 231,  463 => 230,  460 => 229,  454 => 226,  449 => 225,  446 => 224,  440 => 221,  435 => 220,  432 => 219,  426 => 216,  421 => 215,  419 => 214,  414 => 211,  412 => 210,  409 => 209,  400 => 203,  394 => 199,  392 => 198,  386 => 194,  380 => 191,  377 => 190,  374 => 189,  370 => 186,  364 => 183,  361 => 182,  359 => 181,  354 => 179,  351 => 178,  345 => 175,  342 => 174,  340 => 173,  335 => 172,  332 => 171,  326 => 168,  323 => 167,  320 => 166,  318 => 165,  312 => 163,  310 => 162,  304 => 159,  301 => 158,  298 => 157,  296 => 156,  293 => 155,  290 => 154,  287 => 153,  284 => 152,  281 => 151,  278 => 150,  276 => 149,  270 => 145,  261 => 139,  255 => 135,  253 => 134,  250 => 133,  244 => 129,  238 => 126,  233 => 125,  230 => 124,  224 => 121,  219 => 120,  216 => 119,  210 => 116,  205 => 115,  203 => 114,  198 => 111,  196 => 110,  193 => 109,  190 => 108,  183 => 103,  175 => 100,  169 => 99,  166 => 98,  164 => 97,  160 => 96,  156 => 94,  148 => 91,  142 => 90,  139 => 89,  137 => 88,  133 => 87,  129 => 85,  121 => 82,  115 => 81,  112 => 80,  110 => 79,  106 => 78,  101 => 75,  99 => 74,  94 => 73,  92 => 72,  85 => 67,  79 => 65,  77 => 64,  71 => 60,  65 => 57,  62 => 56,  60 => 55,  55 => 52,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/ebiz/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\ebiz\\templates\\layout\\page.html.twig");
    }
}
