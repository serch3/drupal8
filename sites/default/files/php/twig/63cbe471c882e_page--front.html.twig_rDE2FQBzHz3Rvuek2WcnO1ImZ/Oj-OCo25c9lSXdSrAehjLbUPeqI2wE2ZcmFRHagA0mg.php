<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_creative/templates/layout/page--front.html.twig */
class __TwigTemplate_3c0e5ffaafd7083856be6c9230ca334aea670fe9ffa74cdc6ae510c50b5468ce extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 45];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- start header section -->
    <header id=\"header\">
      <div class=\"container\">
        <nav class=\"navbar navbar-expand-lg navbar-light navbar-fixed-top\" id=\"mainNav\">
          ";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "logo", [])), "html", null, true);
        echo "
          <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\"  aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
            <i class=\"fa fa-bars\"></i>
          </button>
            ";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
        echo "
          </nav>
      </div>
    </header>
<!-- End header section -->
";
        // line 15
        echo "  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
";
        // line 17
        echo "
<!-- FOOTER SECTION-->
<section id=\"footer\">
  <div class=\"container\">
    <div class=\"row footer-menu-social\">
      <div class=\"col-md-8 footer-menu wow fadeInDown\">
        <ul>
          ";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_menu", [])), "html", null, true);
        echo "
        </ul>
      </div>
      </div>
      <div class=\"row\">
      <div class=\"col-md-3 col-sm-12 footer-columns wow fadeInDown\" data-wow-delay=\"0.3s\">
          ";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_about", [])), "html", null, true);
        echo "
      </div>
      <div class=\"col-md-3 col-sm-12 footer-columns wow fadeInDown\" data-wow-delay=\"0.6s\">
        <h6>";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["find_us_title"] ?? null)), "html", null, true);
        echo "</h6>
        <p class=\"find_us_area\">";
        // line 34
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["find_us_area"] ?? null)), "html", null, true);
        echo "</p>
      </div>
      <div class=\"col-md-3 col-sm-12 footer-columns lets-talk wow fadeInDown\" data-wow-delay=\"0.9s\">
        <h6>";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["lets_talk_title"] ?? null)), "html", null, true);
        echo "</h6>
        <p><a href=\"tel:";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["lets_talk_phone_no"] ?? null)), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["lets_talk_phone_no"] ?? null)), "html", null, true);
        echo "</a></p>
        <p><a href=\"mailto:";
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["lets_talk_email"] ?? null)), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["lets_talk_email"] ?? null)), "html", null, true);
        echo "</a></p>
      </div>
      <div class=\"col-md-3 col-sm-12 footer-columns wow fadeInDown\" data-wow-delay=\"0.9s\">
        <h6>";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_link_title"] ?? null)), "html", null, true);
        echo "</h6>
        <div class=\"social\">
          <ul>
            ";
        // line 45
        if (($context["linkedin"] ?? null)) {
            // line 46
            echo "              <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin"] ?? null)), "html", null, true);
            echo "\"><i class=\"fa fa-linkedin\"></i></a></li>
            ";
        }
        // line 48
        echo "            ";
        if (($context["instagram"] ?? null)) {
            // line 49
            echo "              <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram"] ?? null)), "html", null, true);
            echo "\"><i class=\"fa fa-instagram\"></i></a></li>
            ";
        }
        // line 51
        echo "            ";
        if (($context["googleplus"] ?? null)) {
            // line 52
            echo "              <li>
                <a href=\"";
            // line 53
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["googleplus"] ?? null)), "html", null, true);
            echo "\"><i class=\"fa fa-google-plus\"></i></a>
              </li>
            ";
        }
        // line 56
        echo "            ";
        if (($context["facebook"] ?? null)) {
            // line 57
            echo "              <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook"] ?? null)), "html", null, true);
            echo "\"><i class=\"fa fa-facebook-f\"></i></a></li>
            ";
        }
        // line 59
        echo "            ";
        if (($context["twitter"] ?? null)) {
            // line 60
            echo "              <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter"] ?? null)), "html", null, true);
            echo "\"><i class=\"fa fa-twitter\"></i></a></li>
            ";
        }
        // line 62
        echo "          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- copyright -->
<section id=\"copyright\">
  <div class=\"container\">
    <div class=\"row\">  
      <div class=\"col-md-6 mx-auto text-center wow fadeInDown\" data-wow-delay=\"0.3s\"> 
        <p>© 2018 <span>YG Creative</span>. All Rights Reserved. </p>
        <p>Theme By<a href=\"https://www.drupaldevelopersstudio.com/\" target=\"_blank\"> Drupal Developers Studio</a>, A Division of <a href=\"https://www.youngglobes.com/\" target=\"_blank\">Young Globes</a></p>  
      </div>
    </div>
  </div>
</section>
<!-- End Copyright -->
";
    }

    public function getTemplateName()
    {
        return "themes/yg_creative/templates/layout/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 62,  180 => 60,  177 => 59,  171 => 57,  168 => 56,  162 => 53,  159 => 52,  156 => 51,  150 => 49,  147 => 48,  141 => 46,  139 => 45,  133 => 42,  125 => 39,  119 => 38,  115 => 37,  109 => 34,  105 => 33,  99 => 30,  90 => 24,  81 => 17,  76 => 15,  68 => 9,  61 => 5,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_creative/templates/layout/page--front.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\yg_creative\\templates\\layout\\page--front.html.twig");
    }
}
