<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @electra/includes/footer.html.twig */
class __TwigTemplate_dbdb7bad8f3cf291498ca17002a281d04c6c4934c723a0ab9fa78519c965affc extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 3, "if" => 13, "include" => 43];
        $filters = ["escape" => 20, "length" => 42];
        $functions = ["create_attribute" => 3];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'include'],
                ['escape', 'length'],
                ['create_attribute']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- =============================== footer ===============================-->

";
        // line 3
        $context["social_attribute"] = $this->env->getExtension('Drupal\Core\Template\TwigExtension')->createAttribute();
        // line 5
        $context["social_classes"] = [0 => "social-icons", 1 => "brand-bg-icons", 2 => "icon-sm", 3 => (($this->getAttribute(        // line 9
($context["electra"] ?? null), "display_social_round_icons", [])) ? ("round-icons") : (""))];
        // line 12
        echo "
";
        // line 13
        if (($this->getAttribute(($context["electra"] ?? null), "header_position", []) == "header-left")) {
            // line 14
            echo "  <footer class=\"page-footer bg-light container\">
";
        } else {
            // line 16
            echo "  <footer class=\"page-footer bg-light\">
";
        }
        // line 18
        echo "
  ";
        // line 19
        if ($this->getAttribute(($context["page"] ?? null), "footer_top", [])) {
            // line 20
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_top", [])), "html", null, true);
            echo "
  ";
        }
        // line 22
        echo "
  ";
        // line 23
        if ($this->getAttribute(($context["page"] ?? null), "footer_middle", [])) {
            // line 24
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_middle", [])), "html", null, true);
            echo "
  ";
        }
        // line 26
        echo "
  <div class=\"footer-bottom mt-3\">
    <div class=\"container\">
      <div class=\"row\">

        <!-- end of col-md-7 -->
        <div class=\"col-md-7 d-flex\">
          <div class=\"copyright\">
            <p>";
        // line 34
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["electra"] ?? null), "copyright_text", [])), "html", null, true);
        echo "
              ";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["electra"] ?? null), "theme_credits", [])), "html", null, true);
        echo "</p>
          </div>
          <!-- end of copyright -->
        </div>
        <!-- end of col-md-7 -->
        <div class=\"col-md-5 d-flex justify-content-end\">

          ";
        // line 42
        if ((twig_length_filter($this->env, $this->getAttribute(($context["electra"] ?? null), "social_profiles", [])) > 0)) {
            // line 43
            echo "            ";
            $this->loadTemplate("@electra/includes/social-icons.html.twig", "@electra/includes/footer.html.twig", 43)->display($context);
            // line 44
            echo "          ";
        } else {
            // line 45
            echo "            ";
            $this->loadTemplate("@electra/includes/social-icons-empty.html.twig", "@electra/includes/footer.html.twig", 45)->display($context);
            // line 46
            echo "          ";
        }
        // line 47
        echo "
          <!-- end of social-icons -->
        </div>
        <!-- end of col-md-3 -->
      </div>
      <!-- end of row -->
    </div>
    <!-- end of container -->
  </div>
  <!-- end of footer Bottom -->
</footer>
<!-- end of page-footer -->
";
    }

    public function getTemplateName()
    {
        return "@electra/includes/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 47,  134 => 46,  131 => 45,  128 => 44,  125 => 43,  123 => 42,  113 => 35,  109 => 34,  99 => 26,  93 => 24,  91 => 23,  88 => 22,  82 => 20,  80 => 19,  77 => 18,  73 => 16,  69 => 14,  67 => 13,  64 => 12,  62 => 9,  61 => 5,  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "@electra/includes/footer.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\electra\\templates\\includes\\footer.html.twig");
    }
}
