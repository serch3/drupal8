<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/dolphin_theme/templates/layout/page.html.twig */
class __TwigTemplate_3c1974bd6b6e5f9060227e2c34e05379eb4dd2f0cd0acda50bb21160c9e4b8bb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 74];
        $filters = ["escape" => 63];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 61
        echo "
<!-- Header and Navbar -->
<header class=\"main-header\" style=\"background-image: url(";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_large_bg"] ?? null)), "html", null, true);
        echo ")\" >
  <nav class=\"navbar topnav navbar-default\" role=\"navigation\">
    <div class=\"container p-0\">
      <div class=\"row col-12 m-0 p-0\">
      <div class=\"navbar-header col-md-4 p-0\">
        <button type=\"button\" class=\"navbar-toggle d-sm-block d-md-none\" data-toggle=\"collapse\" data-target=\"#main-navigation\">
          <span class=\"sr-only\">Toggle navigation</span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
        </button>
        ";
        // line 74
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 75
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
        ";
        }
        // line 77
        echo "      </div>

      <!-- Navigation -->
      <div class=\"col-md-8 p-0\">
        ";
        // line 81
        if ($this->getAttribute(($context["page"] ?? null), "primary_menu", [])) {
            // line 82
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
        ";
        }
        // line 83
        echo "      
      </div>
      <!--End Navigation -->

      </div>
    </div>
  </nav>

  <!-- banner-->
  ";
        // line 92
        if ((($context["is_front"] ?? null) && $this->getAttribute(($context["page"] ?? null), "promo", []))) {
            // line 93
            echo "    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          ";
            // line 96
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "promo", [])), "html", null, true);
            echo "
        </div>
      </div>
    </div>
  ";
        }
        // line 101
        echo "<!--End banner-->

</header>
<!--End Header & Navbar -->

<!--Highlighted-->
  ";
        // line 107
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 108
            echo "    <div class=\"highlighted-wrapper\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            ";
            // line 112
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
          </div>
        </div>
      </div>
    </div>
  ";
        }
        // line 118
        echo "<!--End Highlighted-->

<!--Help-->
  ";
        // line 121
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 122
            echo "    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          ";
            // line 125
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
            echo "
        </div>
      </div>
    </div>
  ";
        }
        // line 130
        echo "<!--End Help-->

<!-- layout -->
<div id=\"wrapper\">
  <!-- start: Container -->
  <div class=\"container-fluid\">
    
    <!--Start Content Top-->
    ";
        // line 138
        if ($this->getAttribute(($context["page"] ?? null), "content_top", [])) {
            // line 139
            echo "    <div class=\"content-top\">
        <div class=\"row\">
      <div class=\"col-md-12\">
            ";
            // line 142
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top", [])), "html", null, true);
            echo "
      </div>
        </div>
  </div>
    ";
        }
        // line 147
        echo "    <!--End Content Top-->
  
    <div class=\"row layout\">
      <!--- Start Left Sidebar -->
      ";
        // line 151
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 152
            echo "          <div class = \"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarfirst"] ?? null)), "html", null, true);
            echo " sidebar-first\">
            ";
            // line 153
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
          </div>
      ";
        }
        // line 156
        echo "      <!---End Left Sidebar -->

      <!--- Start content -->
      ";
        // line 159
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 160
            echo "          <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contentlayout"] ?? null)), "html", null, true);
            echo " content-layout\">
            
            <!-- Page Title -->
            ";
            // line 163
            if (($this->getAttribute(($context["page"] ?? null), "page_title", []) &&  !($context["is_front"] ?? null))) {
                // line 164
                echo "              <div id=\"page-title\">
                <div id=\"page-title-inner\">
                  <!-- start: Container -->
                  <div class=\"container-fluid\">
                    ";
                // line 168
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_title", [])), "html", null, true);
                echo "
                  </div>
                </div>
              </div>
            ";
            }
            // line 173
            echo "            <!-- End Page Title -->

            ";
            // line 175
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
          </div>
      ";
        }
        // line 178
        echo "      <!---End content -->

      <!--- Start Right Sidebar -->
      ";
        // line 181
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 182
            echo "          <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarsecond"] ?? null)), "html", null, true);
            echo " sidebar-second\">
            ";
            // line 183
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
          </div>

      ";
        }
        // line 187
        echo "      <!---End Right Sidebar -->
      
    </div>
    <!--End Content -->

    <!--Start Content Bottom-->
    ";
        // line 193
        if ($this->getAttribute(($context["page"] ?? null), "content_bottom", [])) {
            // line 194
            echo "    <div class=\"content-bottom\">
        <div class=\"row\">
      <div class=\"col-md-12\">
            ";
            // line 197
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom", [])), "html", null, true);
            echo "
      </div>
        </div>
  </div>
    ";
        }
        // line 202
        echo "    <!--End Content Bottom-->
  </div>
</div>
<!-- End layout -->

<!-- #footer-top -->
<div id=\"footer-top\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-12\">
        ";
        // line 212
        if ($this->getAttribute(($context["page"] ?? null), "footer_top", [])) {
            // line 213
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_top", [])), "html", null, true);
            echo "
        ";
        }
        // line 215
        echo "      </div>
    </div>
  </div>
</div>
<!-- #footer-top ends here -->

<!-- Start Footer -->
<div class=\"footer-wrap\">
  <div class=\"container\">

    ";
        // line 225
        if (((($this->getAttribute(($context["page"] ?? null), "footer_col_one", []) || $this->getAttribute(($context["page"] ?? null), "footer_col_two", [])) || $this->getAttribute(($context["page"] ?? null), "footer_col_three", [])) || $this->getAttribute(($context["page"] ?? null), "footer_col_four", []))) {
            // line 226
            echo "      <div class=\"footer-widgets\">
        <div class=\"row\">

          <!-- Start Footer Top One Region -->
          ";
            // line 230
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_one", [])) {
                // line 231
                echo "            <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
              ";
                // line 232
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_one", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 235
            echo "          <!-- End Footer Top One Region -->

          <!-- Start Footer Top Two Region -->
          ";
            // line 238
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_two", [])) {
                // line 239
                echo "            <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
              ";
                // line 240
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_two", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 243
            echo "          <!-- End Footer Top Two Region -->

          <!-- Start Footer Top Three Region -->
          ";
            // line 246
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_three", [])) {
                // line 247
                echo "            <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
              ";
                // line 248
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_three", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 251
            echo "          <!-- End Footer Top Three Region -->
\t\t  
\t\t      <!-- Start Footer Top Four Region -->
          ";
            // line 254
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_four", [])) {
                // line 255
                echo "          <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
            ";
                // line 256
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_four", [])), "html", null, true);
                echo "
          </div>
          ";
            }
            // line 259
            echo "\t\t      <!-- End Footer Top Four Region -->

        </div>
      </div>
    ";
        }
        // line 264
        echo "
    ";
        // line 265
        if (($context["show_footer_bottom_section"] ?? null)) {
            // line 266
            echo "      <div class=\"row footer-bottom\">
        <div class=\"col-md-6\">
          ";
            // line 268
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["copyright"] ?? null)), "html", null, true);
            echo "
        </div>
        <div class=\"col-md-6 footer-social\">
          ";
            // line 271
            if (($context["facebook_url"] ?? null)) {
                // line 272
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-facebook\"><i class=\"fa fa-facebook\"></i></a>
          ";
            }
            // line 274
            echo "          ";
            if (($context["google_plus_url"] ?? null)) {
                // line 275
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["google_plus_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-gplus\"><i class=\"fa fa-google-plus\"></i></a>
          ";
            }
            // line 277
            echo "          ";
            if (($context["twitter_url"] ?? null)) {
                // line 278
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-twitter\"><i class=\"fa fa-twitter\"></i></a>
          ";
            }
            // line 280
            echo "          ";
            if (($context["linkedin_url"] ?? null)) {
                // line 281
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-linkedin\"><i class=\"fa fa-linkedin\"></i></a>
          ";
            }
            // line 283
            echo "          ";
            if (($context["ytube_url"] ?? null)) {
                // line 284
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ytube_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-youtube\"><i class=\"fa fa-youtube-play\"></i></a>
          ";
            }
            // line 286
            echo "        </div>
  \t  </div>
    ";
        }
        // line 289
        echo "  </div>
</div>
<!--End Footer -->
";
    }

    public function getTemplateName()
    {
        return "themes/dolphin_theme/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  470 => 289,  465 => 286,  459 => 284,  456 => 283,  450 => 281,  447 => 280,  441 => 278,  438 => 277,  432 => 275,  429 => 274,  423 => 272,  421 => 271,  415 => 268,  411 => 266,  409 => 265,  406 => 264,  399 => 259,  393 => 256,  388 => 255,  386 => 254,  381 => 251,  375 => 248,  370 => 247,  368 => 246,  363 => 243,  357 => 240,  352 => 239,  350 => 238,  345 => 235,  339 => 232,  334 => 231,  332 => 230,  326 => 226,  324 => 225,  312 => 215,  306 => 213,  304 => 212,  292 => 202,  284 => 197,  279 => 194,  277 => 193,  269 => 187,  262 => 183,  257 => 182,  255 => 181,  250 => 178,  244 => 175,  240 => 173,  232 => 168,  226 => 164,  224 => 163,  217 => 160,  215 => 159,  210 => 156,  204 => 153,  199 => 152,  197 => 151,  191 => 147,  183 => 142,  178 => 139,  176 => 138,  166 => 130,  158 => 125,  153 => 122,  151 => 121,  146 => 118,  137 => 112,  131 => 108,  129 => 107,  121 => 101,  113 => 96,  108 => 93,  106 => 92,  95 => 83,  89 => 82,  87 => 81,  81 => 77,  75 => 75,  73 => 74,  59 => 63,  55 => 61,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/dolphin_theme/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\dolphin_theme\\templates\\layout\\page.html.twig");
    }
}
