<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/photogenictheme/templates/page.html.twig */
class __TwigTemplate_535d435fa7031d7eed4d883f9995692203102181586d60832b9c1fe085bb5c1b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 86, "set" => 101];
        $filters = ["escape" => 58];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 54
        echo "<div class=\"templatemo-top-bar\" id=\"templatemo-top\">
    <div class=\"container\">
        <div class=\"subheader\">
            <div id=\"phone\" class=\"pull-left\">
                    <img src=\"";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_contact_image"] ?? null)), "html", null, true);
        echo "\"/>
                    ";
        // line 59
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_contact"] ?? null)), "html", null, true);
        echo "
            </div>
            <div id=\"email\" class=\"pull-right\">
                    <img src=\"";
        // line 62
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_email_image"] ?? null)), "html", null, true);
        echo "\"/>
                    ";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_email"] ?? null)), "html", null, true);
        echo "
            </div>
        </div>
    </div>
</div>

<div class=\"templatemo-top-menu\">
    <div class=\"container\">
        <!-- Static navbar -->
        <div class=\"navbar navbar-default\" role=\"navigation\">
            <div class=\"container\">
                <div class=\"navbar-header\">
                    ";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    </button>
                </div>
                <div class=\"navbar-collapse collapse\" id=\"templatemo-nav-bar\">
                    <ul class=\"nav navbar-nav navbar-right\">
                        <!-- Home -->
                        ";
        // line 86
        if ($this->getAttribute(($context["page"] ?? null), "main_navigation", [])) {
            // line 87
            echo "                            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_navigation", [])), "html", null, true);
            echo "
                        ";
        }
        // line 88
        echo " 
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </div><!--/.navbar -->
    </div> <!-- /container -->
</div>

<!-- main content section start -->
<section class=\"templatemo-welcome\">
    <div class=\"container\">
        <div class=\"row\">
            ";
        // line 100
        if (($this->getAttribute(($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) {
            // line 101
            echo "                ";
            $context["primary_col"] = 6;
            // line 102
            echo "            ";
        } elseif (($this->getAttribute(($context["page"] ?? null), "sidebar_first", []) || $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) {
            // line 103
            echo "                ";
            $context["primary_col"] = 9;
            echo "            
            ";
        } else {
            // line 105
            echo "                ";
            $context["primary_col"] = 12;
            // line 106
            echo "            ";
        }
        // line 107
        echo "            
            ";
        // line 108
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 109
            echo "                <div id=\"sidebar-first\" class=\"col-md-3\">
                    <aside class=\"section\">
                        ";
            // line 111
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
                    </aside>
                </div>
            ";
        }
        // line 115
        echo "            
            <div class=\"";
        // line 116
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("content-area col-md-" . $this->sandbox->ensureToStringAllowed(($context["primary_col"] ?? null))), "html", null, true);
        echo "\">
                ";
        // line 117
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
            </div>

            ";
        // line 120
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 121
            echo "                <div id=\"sidebar-second\" class=\"col-md-3\">
                    <aside class=\"section\">
                        ";
            // line 123
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
                    </aside>
                </div>
            ";
        }
        // line 126
        echo "                                
        </div>
    </div>
</section>

<section class=\"templatemo-footer\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-4 col-sm-4\">
                <div class=\"footer_container\">
                    ";
        // line 136
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_one", [])), "html", null, true);
        echo " 
                </div>
            </div>

            <div class=\"col-md-4 col-sm-4\">
                <div class=\"footer_container\">
                    ";
        // line 142
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_two", [])), "html", null, true);
        echo "
                    <ul class=\"list-inline\">
                        <li>
                            <a href=\"";
        // line 145
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_facebook"] ?? null)), "html", null, true);
        echo "\">
                                <span class=\"social-icon-fb\"></span>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 150
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_twitter"] ?? null)), "html", null, true);
        echo "\">
                                <span class=\"social-icon-twitter\"></span>
                            </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 155
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_linkedin"] ?? null)), "html", null, true);
        echo "\">
                                <span class=\"social-icon-linkedin\"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class=\"col-md-4 col-sm-4\">
                <div class=\"footer_container\">
                    ";
        // line 165
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_three", [])), "html", null, true);
        echo "
                </div>
            </div>              
        </div>
    </div>
</section>

<footer class=\"copyright-section\">
    <div class=\"container text-center\">
        <div class=\"copyright-info\">
            <span>Copyright © 2020 Photogenic Theme. All Rights Reserved.</span>
        </div>
    </div><!-- /.container -->
</footer>";
    }

    public function getTemplateName()
    {
        return "themes/photogenictheme/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 165,  231 => 155,  223 => 150,  215 => 145,  209 => 142,  200 => 136,  188 => 126,  181 => 123,  177 => 121,  175 => 120,  169 => 117,  165 => 116,  162 => 115,  155 => 111,  151 => 109,  149 => 108,  146 => 107,  143 => 106,  140 => 105,  134 => 103,  131 => 102,  128 => 101,  126 => 100,  112 => 88,  106 => 87,  104 => 86,  90 => 75,  75 => 63,  71 => 62,  65 => 59,  61 => 58,  55 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/photogenictheme/templates/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\photogenictheme\\templates\\page.html.twig");
    }
}
