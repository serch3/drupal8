<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/dolphin_theme/templates/layout/page--front.html.twig */
class __TwigTemplate_ab778cb774c9f8f7f0e795b6f8c206318b49f28bf9889f47bc75482fa1ec4da8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 74, "for" => 123];
        $filters = ["escape" => 63, "raw" => 96];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 61
        echo "
<!-- Header and Navbar -->
<header class=\"main-header\" style=\"background-image: url(";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_large_bg"] ?? null)), "html", null, true);
        echo ")\" >
  <nav class=\"navbar topnav navbar-default\" role=\"navigation\">
    <div class=\"container p-0\">
      <div class=\"row col-12 m-0 p-0\">
      <div class=\"navbar-header col-md-4 p-0\">
        <button type=\"button\" class=\"navbar-toggle d-sm-block d-md-none\" data-toggle=\"collapse\" data-target=\"#main-navigation\">
          <span class=\"sr-only\">Toggle navigation</span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
        </button>
        ";
        // line 74
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 75
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
        ";
        }
        // line 77
        echo "      </div>

      <!-- Navigation -->
      <div class=\"col-md-8 p-0\">
        ";
        // line 81
        if ($this->getAttribute(($context["page"] ?? null), "primary_menu", [])) {
            // line 82
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
        ";
        }
        // line 83
        echo "      
      </div>
      <!--End Navigation -->

      </div>
    </div>
  </nav>

  <!-- Banner --> 
  <div class=\"container home-banner\">
    <div class=\"row\">
      <div class=\"col-md-6 banner-content\">
        <div class=\"sub-heading\">";
        // line 95
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_subheading"] ?? null)), "html", null, true);
        echo "</div>
        <div class=\"title\">";
        // line 96
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["banner_title"] ?? null)));
        echo "</div>
        <div class=\"description\">";
        // line 97
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_desc"] ?? null)), "html", null, true);
        echo "</div>
        <div class=\"cta\">
          <a href=\"";
        // line 99
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_read_more_link"] ?? null)), "html", null, true);
        echo "\" class=\"btn-large\">Read more Details</a>
        </div>
      </div>
      <div class=\"col-md-6 d-none d-sm-block home-banner-image\">
        ";
        // line 103
        if (($context["banner_small_bg"] ?? null)) {
            // line 104
            echo "          <img src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_small_bg"] ?? null)), "html", null, true);
            echo "\" />
        ";
        }
        // line 106
        echo "      </div>
      
    </div>
  </div>
  <!-- End Banner -->

</header>
<!--End Header & Navbar -->


<!-- Start Top Widget -->
";
        // line 117
        if (($context["show_promotional_cards"] ?? null)) {
            // line 118
            echo "  <div class=\"topwidget\">
    <!-- start: Container -->
    <div class=\"container\">
      <div class=\"row topwidget-inner\">

          ";
            // line 123
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["promotional_cards"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["card"]) {
                echo "          
            <a href=\"";
                // line 124
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["card"], "link", [])), "html", null, true);
                echo "\" class=\"col-xs-12 col-sm-6 col-md-3 topwidget-block card-";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                echo "\">

                <i class=\"fa fa-";
                // line 126
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["card"], "icon", [])), "html", null, true);
                echo "\" aria-hidden=\"true\"></i>
                <h3>";
                // line 127
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["card"], "title", [])), "html", null, true);
                echo "</h3>
                <p>";
                // line 128
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["card"], "desc", [])), "html", null, true);
                echo "</p>

            </a>
          ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['card'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "
      </div>
    </div>
  </div>
";
        }
        // line 137
        echo "<!--End Top Widget -->


<!--Home page promo -->
";
        // line 141
        if (($context["show_about_section"] ?? null)) {
            // line 142
            echo "<div class=\"promo\">
  <div class=\"container\">
    <div class=\"row col-12\">
        
      <div class=\"col-sm-12 col-md-6 promo-image\">
        <img src=\"";
            // line 147
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_section_img"] ?? null)), "html", null, true);
            echo "\" />
      </div>
      
      <div class=\"col-md-6 promo-content\">
        <div class=\"sub-heading\">";
            // line 151
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_subheading"] ?? null)), "html", null, true);
            echo "</div>
        <div class=\"title\">";
            // line 152
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["about_title"] ?? null)));
            echo "</div>
        <div class=\"description\">";
            // line 153
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_desc"] ?? null)), "html", null, true);
            echo "</div>
        <div class=\"cta\">
          <a href=\"";
            // line 155
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_read_more_link"] ?? null)), "html", null, true);
            echo "\" class=\"btn-large\">Read more Details</a>
        </div>
      </div>

    </div>
  </div>
</div>
";
        }
        // line 163
        echo "<!--End Home page promo -->


<!--Highlighted-->
";
        // line 167
        if (($context["show_numberscroll"] ?? null)) {
            // line 168
            echo "<div class=\"highlighted-section\">
  <div class=\"container\">
    <div class=\"row\">
        ";
            // line 171
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["numberscroll"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["block"]) {
                echo " 
          <div class=\"col-12 col-sm-6 col-md-3 scroller-block scroll-";
                // line 172
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["loop"], "index", [])), "html", null, true);
                echo "\">
            <div class=\"numscroller\" data-min=\"0\" data-max=\"";
                // line 173
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["block"], "to", [])), "html", null, true);
                echo "\" data-delay=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["block"], "speed", [])), "html", null, true);
                echo "\" data-increment=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["block"], "inc", [])), "html", null, true);
                echo "\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["block"], "to", [])), "html", null, true);
                echo "</div>
            <h3>";
                // line 174
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["block"], "title", [])), "html", null, true);
                echo "</h3>
            <p>";
                // line 175
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["block"], "desc", [])), "html", null, true);
                echo "</p>
          </div>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['block'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 178
            echo "    </div>
  </div>
</div>
";
        }
        // line 182
        echo "<!--End Highlighted-->

<!--Help-->
";
        // line 185
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 186
            echo "  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-md-12\">
        ";
            // line 189
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
            echo "
      </div>
    </div>
  </div>
";
        }
        // line 194
        echo "<!--End Help-->


<!-- Page Title -->
";
        // line 198
        if ($this->getAttribute(($context["page"] ?? null), "page_title", [])) {
            // line 199
            echo "  <div id=\"page-title\">
    <div id=\"page-title-inner\">
      <!-- start: Container -->
      <div class=\"container-fluid\">
        ";
            // line 203
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_title", [])), "html", null, true);
            echo "
      </div>
    </div>
  </div>
";
        }
        // line 208
        echo "<!-- End Page Title -->


<!-- layout -->
<div id=\"wrapper\">
  <!-- start: Container -->
  <div class=\"container-fluid\">
    
    <!--Start Content Top-->
    ";
        // line 217
        if ($this->getAttribute(($context["page"] ?? null), "content_top", [])) {
            // line 218
            echo "    <div class=\"content-top\">
        <div class=\"row\">
\t\t  <div class=\"col-md-12\">
            ";
            // line 221
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top", [])), "html", null, true);
            echo "
\t\t  </div>
        </div>
\t</div>
    ";
        }
        // line 226
        echo "    <!--End Content Top-->
    
    <!--start:Breadcrumbs -->
\t";
        // line 229
        if ( !($context["is_front"] ?? null)) {
            // line 230
            echo "    <div class=\"row\">
      <div class=\"col-md-12\">";
            // line 231
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
            echo "</div>
    </div>
\t";
        }
        // line 234
        echo "    <!--End Breadcrumbs-->
\t
    <div class=\"row layout\">
      <!--- Start Left Sidebar -->
      ";
        // line 238
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 239
            echo "          <div class = \"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarfirst"] ?? null)), "html", null, true);
            echo " sidebar-first\">
            ";
            // line 240
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
          </div>
      ";
        }
        // line 243
        echo "      <!---End Left Sidebar -->

      <!--- Start content -->
      ";
        // line 246
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 247
            echo "          <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contentlayout"] ?? null)), "html", null, true);
            echo " content-layout\">
            ";
            // line 248
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
          </div>
      ";
        }
        // line 251
        echo "      <!---End content -->

      <!--- Start Right Sidebar -->
      ";
        // line 254
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 255
            echo "          <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarsecond"] ?? null)), "html", null, true);
            echo " sidebar-second\">
            ";
            // line 256
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
          </div>

      ";
        }
        // line 260
        echo "      <!---End Right Sidebar -->
      
    </div>
    <!--End Content -->

    <!--Start Content Bottom-->
    ";
        // line 266
        if ($this->getAttribute(($context["page"] ?? null), "content_bottom", [])) {
            // line 267
            echo "    <div class=\"content-bottom\">
        <div class=\"row\">
\t\t  <div class=\"col-md-12\">
            ";
            // line 270
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom", [])), "html", null, true);
            echo "
\t\t  </div>
        </div>
\t</div>
    ";
        }
        // line 275
        echo "    <!--End Content Bottom-->
  </div>
</div>
<!-- End layout -->

<!--Search-->
<div id=\"search\">
  <div class=\"container\">
    <div class=\"row\">
      ";
        // line 284
        if ((($context["is_front"] ?? null) && $this->getAttribute(($context["page"] ?? null), "search", []))) {
            // line 285
            echo "        ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "search", [])), "html", null, true);
            echo "
      ";
        }
        // line 287
        echo "    </div>
  </div>
</div>
<!--End Search-->

<!-- #footer-top -->
<div id=\"footer-top\">
  <div class=\"container\">
    <div class=\"row\">
      <div class=\"col-12\">
        ";
        // line 297
        if ($this->getAttribute(($context["page"] ?? null), "footer_top", [])) {
            // line 298
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_top", [])), "html", null, true);
            echo "
        ";
        }
        // line 300
        echo "      </div>
    </div>
  </div>
</div>
<!-- #footer-top ends here -->

<!-- Start Footer -->
<div class=\"footer-wrap\">
  <div class=\"container\">

    ";
        // line 310
        if (((($this->getAttribute(($context["page"] ?? null), "footer_col_one", []) || $this->getAttribute(($context["page"] ?? null), "footer_col_two", [])) || $this->getAttribute(($context["page"] ?? null), "footer_col_three", [])) || $this->getAttribute(($context["page"] ?? null), "footer_col_four", []))) {
            // line 311
            echo "      <div class=\"footer-widgets\">
        <div class=\"row\">

          <!-- Start Footer Top One Region -->
          ";
            // line 315
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_one", [])) {
                // line 316
                echo "            <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
              ";
                // line 317
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_one", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 320
            echo "          <!-- End Footer Top One Region -->

          <!-- Start Footer Top Two Region -->
          ";
            // line 323
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_two", [])) {
                // line 324
                echo "            <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
              ";
                // line 325
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_two", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 328
            echo "          <!-- End Footer Top Two Region -->

          <!-- Start Footer Top Three Region -->
          ";
            // line 331
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_three", [])) {
                // line 332
                echo "            <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
              ";
                // line 333
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_three", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 336
            echo "          <!-- End Footer Top Three Region -->
\t\t  
\t\t      <!-- Start Footer Top Four Region -->
          ";
            // line 339
            if ($this->getAttribute(($context["page"] ?? null), "footer_col_four", [])) {
                // line 340
                echo "          <div class = \"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_col_class"] ?? null)), "html", null, true);
                echo "\">
            ";
                // line 341
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_four", [])), "html", null, true);
                echo "
          </div>
          ";
            }
            // line 344
            echo "\t\t      <!-- End Footer Top Four Region -->

        </div>
      </div>
    ";
        }
        // line 349
        echo "
    ";
        // line 350
        if (($context["show_footer_bottom_section"] ?? null)) {
            // line 351
            echo "      <div class=\"row footer-bottom\">
        <div class=\"col-md-6\">
          ";
            // line 353
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["copyright"] ?? null)), "html", null, true);
            echo "
        </div>
        <div class=\"col-md-6 footer-social\">
          ";
            // line 356
            if (($context["facebook_url"] ?? null)) {
                // line 357
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-facebook\"><i class=\"fa fa-facebook\"></i></a>
          ";
            }
            // line 359
            echo "          ";
            if (($context["google_plus_url"] ?? null)) {
                // line 360
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["google_plus_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-gplus\"><i class=\"fa fa-google-plus\"></i></a>
          ";
            }
            // line 362
            echo "          ";
            if (($context["twitter_url"] ?? null)) {
                // line 363
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-twitter\"><i class=\"fa fa-twitter\"></i></a>
          ";
            }
            // line 365
            echo "          ";
            if (($context["linkedin_url"] ?? null)) {
                // line 366
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-linkedin\"><i class=\"fa fa-linkedin\"></i></a>
          ";
            }
            // line 368
            echo "          ";
            if (($context["ytube_url"] ?? null)) {
                // line 369
                echo "            <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["ytube_url"] ?? null)), "html", null, true);
                echo "\" class=\"icon-youtube\"><i class=\"fa fa-youtube-play\"></i></a>
          ";
            }
            // line 371
            echo "        </div>
  \t  </div>
    ";
        }
        // line 374
        echo "  </div>
</div>
<!--End Footer -->
";
    }

    public function getTemplateName()
    {
        return "themes/dolphin_theme/templates/layout/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  697 => 374,  692 => 371,  686 => 369,  683 => 368,  677 => 366,  674 => 365,  668 => 363,  665 => 362,  659 => 360,  656 => 359,  650 => 357,  648 => 356,  642 => 353,  638 => 351,  636 => 350,  633 => 349,  626 => 344,  620 => 341,  615 => 340,  613 => 339,  608 => 336,  602 => 333,  597 => 332,  595 => 331,  590 => 328,  584 => 325,  579 => 324,  577 => 323,  572 => 320,  566 => 317,  561 => 316,  559 => 315,  553 => 311,  551 => 310,  539 => 300,  533 => 298,  531 => 297,  519 => 287,  513 => 285,  511 => 284,  500 => 275,  492 => 270,  487 => 267,  485 => 266,  477 => 260,  470 => 256,  465 => 255,  463 => 254,  458 => 251,  452 => 248,  447 => 247,  445 => 246,  440 => 243,  434 => 240,  429 => 239,  427 => 238,  421 => 234,  415 => 231,  412 => 230,  410 => 229,  405 => 226,  397 => 221,  392 => 218,  390 => 217,  379 => 208,  371 => 203,  365 => 199,  363 => 198,  357 => 194,  349 => 189,  344 => 186,  342 => 185,  337 => 182,  331 => 178,  314 => 175,  310 => 174,  300 => 173,  296 => 172,  277 => 171,  272 => 168,  270 => 167,  264 => 163,  253 => 155,  248 => 153,  244 => 152,  240 => 151,  233 => 147,  226 => 142,  224 => 141,  218 => 137,  211 => 132,  193 => 128,  189 => 127,  185 => 126,  178 => 124,  159 => 123,  152 => 118,  150 => 117,  137 => 106,  131 => 104,  129 => 103,  122 => 99,  117 => 97,  113 => 96,  109 => 95,  95 => 83,  89 => 82,  87 => 81,  81 => 77,  75 => 75,  73 => 74,  59 => 63,  55 => 61,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/dolphin_theme/templates/layout/page--front.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\dolphin_theme\\templates\\layout\\page--front.html.twig");
    }
}
