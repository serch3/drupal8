<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_relic/templates/layout/page--front.html.twig */
class __TwigTemplate_28c9b0df205df484584e819dd2a343ae2afc052f9fab35c54860a61b046f6402 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "  <body data-name=\"gototop\">
    
    <!-- Navigation -->
    <div id=\"fh5co-main-nav-wrap\">
      <div class=\"fh5co-nav-overlay\"></div>
      <div class=\"fh5co-nav-inner\">
          ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
        echo "
          <ul class=\"fh5co-social\">
            <li><a href=\"";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>
            <li><a href=\"";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a></li>
            <li><a href=\"";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>
            <li><a href=\"";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a></li>
          </ul>
      </div>

    </div>
    <!-- Navigation -->

    <!-- Header -->
    <header id=\"fh5co-header\" role=\"banner\">
       <a href=\"#\" id=\"menu-btn\" onclick=\"myFunction()\" class=\"js-fh5co-nav-toggle fh5co-nav-toggle fh5co-nav-white\"><i></i></a>
    <div id=\"fh5co-logo\" class=\"text-center\">
          ";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "logo", [])), "html", null, true);
        echo "
      </div>
    </header>
    <!-- Header -->
    
    ";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
    
  <!-- FOOTER SECTION-->
    <footer role=\"contentinfo\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-3\">
            ";
        // line 35
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_1", [])), "html", null, true);
        echo "
          </div>
          <div class=\"col-md-3\">
            ";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_2", [])), "html", null, true);
        echo "
          </div>
          <div class=\"col-md-3\">
            ";
        // line 41
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_col_3", [])), "html", null, true);
        echo "
          </div>          
          <div class=\"col-md-3\">
            <h2 class=\"fh5co-footer-title\">
              ";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_links_title"] ?? null)), "html", null, true);
        echo "
            </h2>
            <ul class=\"fh5co-footer-links\">
            <li><a href=\"";
        // line 48
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter"] ?? null)), "html", null, true);
        echo "\" class=\"fh5co-link\"><i class=\"fa fa-twitter\"></i>&nbsp;&nbsp;Twitter</a></li>
              <li><a href=\"";
        // line 49
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook"] ?? null)), "html", null, true);
        echo "\" class=\"fh5co-link\"><i class=\"fa fa-facebook-square\"></i>&nbsp;&nbsp;Facebook</a></li>
              <li><a href=\"";
        // line 50
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["github"] ?? null)), "html", null, true);
        echo "\" class=\"fh5co-link\"><i class=\"fa fa-github\"></i>&nbsp;&nbsp;Github</a></li>
              <li><a href=\"";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin"] ?? null)), "html", null, true);
        echo "\" class=\"fh5co-link\"><i class=\"fa fa-linkedin-square\"></i>&nbsp;&nbsp;LinkedIn</a></li>
              <li><a href=\"";
        // line 52
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["googleplus"] ?? null)), "html", null, true);
        echo "\" class=\"fh5co-link\"><i class=\"fa fa-google\"></i>&nbsp;&nbsp;Google Plus</a></li>
            </ul>
          </div> 
        </div>
        <div class=\"row\">
          <div class=\"col-md-12\">
            <div class=\"fh5co-copyright\">
              <div class=\"col-md-6\">
                <p>&copy; YG Relic 2018. All Rights Reserved.  Lovely crafted by 
                  <a href=\"http://freehtml5.co\" target=\"_blank\">FREEHTML5.co</a>
                <p> Theme By<a href=\"https://www.drupaldevelopersstudio.com\" target=\"_blank\"> Drupal Developers Studio</a>, A Division of <a href=\"https://www.youngglobes.com\" target=\"_blank\">Young Globes</a></p>
              </div>
              <div class=\"col-md-6\">
                <ul class=\"pull-right fh5co-footer-social\">
                  <li><a href=\"";
        // line 66
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>
                  <li><a href=\"";
        // line 67
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a></li>
                  <li><a href=\"";
        // line 68
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>
                  <li><a href=\"";
        // line 69
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin"] ?? null)), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
<!-- END FOOTER SECTION -->

 <!-- Go To Top -->
    <a href=\"#\" class=\"fh5co-gotop\"><i class=\"ti-angle-up\"></i></a>
    ";
    }

    public function getTemplateName()
    {
        return "themes/yg_relic/templates/layout/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 69,  178 => 68,  174 => 67,  170 => 66,  153 => 52,  149 => 51,  145 => 50,  141 => 49,  137 => 48,  131 => 45,  124 => 41,  118 => 38,  112 => 35,  102 => 28,  94 => 23,  80 => 12,  76 => 11,  72 => 10,  68 => 9,  63 => 7,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_relic/templates/layout/page--front.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\yg_relic\\templates\\layout\\page--front.html.twig");
    }
}
