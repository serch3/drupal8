<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_bold/templates/page--front.html.twig */
class __TwigTemplate_7110706152290896b60cce065d150cbdbaf2df7b1b1e9b2f1b8d184c03f7469a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 20];
        $filters = ["escape" => 6, "render" => 20, "raw" => 22];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape', 'render', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id=\"fh5co-page\">

   <header id=\"fh5co-header\" role=\"banner\">
      <div class=\"container\">
         <div class=\"header-inner\">
            ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "site_branding", [])), "html", null, true);
        echo "
               ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
        echo " 
         </div>
      </div>
   </header>
   
   <!-- Tabs -->
   ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "tabs", [])), "html", null, true);
        echo "
   <!-- Content -->
   ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
   
   <footer id=\"fh5co-footer\" role=\"contentinfo\">
      <div class=\"container\">
         <div class=\"col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            <h3>";
        // line 20
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["about_us_title"] ?? null)))) {
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_us_title"] ?? null)), "html", null, true);
        }
        echo "</h3>
            ";
        // line 21
        if (($context["about_desc"] ?? null)) {
            // line 22
            echo "               <p>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["about_desc"] ?? null)));
            echo "</p>
            ";
        }
        // line 24
        echo "            ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["about_us_url"] ?? null)))) {
            // line 25
            echo "               <p><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_us_url"] ?? null)), "html", null, true);
            echo "\" class=\"btn btn-primary btn-outline with-arrow btn-sm\">Read More <i class=\"icon-arrow-right\"></i></a></p>
            ";
        }
        // line 27
        echo "         </div>
         <div class=\"col-md-3 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            ";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "latest_blog", [])), "html", null, true);
        echo "
         </div>
         <div class=\"col-md-3 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            ";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_menu", [])), "html", null, true);
        echo "
         </div>
         <div class=\"col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            <h3>";
        // line 35
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["social_title"] ?? null)))) {
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_title"] ?? null)), "html", null, true);
        }
        echo "</h3>
            <ul class=\"fh5co-social\">
               ";
        // line 37
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["twitter_url"] ?? null)))) {
            // line 38
            echo "                  <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-twitter\"></i></a></li>
               ";
        }
        // line 40
        echo "               ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["facebook_url"] ?? null)))) {
            // line 41
            echo "                  <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-facebook\"></i></a></li>
               ";
        }
        // line 43
        echo "               ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["google_plus_url"] ?? null)))) {
            // line 44
            echo "                  <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["google_plus_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-google-plus\"></i></a></li>
               ";
        }
        // line 46
        echo "               ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["instagram_url"] ?? null)))) {
            // line 47
            echo "                 <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-instagram\"></i></a></li>
               ";
        }
        // line 48
        echo "     
            </ul>
         </div>
         <div class=\"col-md-12 fh5co-copyright text-center\">
            <p><span> &copy; 2018 <i>YG Bold</i> All Rights Reserved. Designed with <i class=\"icon-heart\"></i> by <a href=\"http://freehtml5.co\" target=\"_blank\">FREEHTML5.co</a><br> Theme By<a href=\"https://www.drupaldevelopersstudio.com\" target=\"_blank\"> Drupal Developers Studio</a>, A Division of <a href=\"http://www.youngglobes.com\" target=\"_blank\">Young Globes</a></span></p>  
         </div>
      </div>
   </footer>
</div>
      
   ";
    }

    public function getTemplateName()
    {
        return "themes/yg_bold/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 48,  163 => 47,  160 => 46,  154 => 44,  151 => 43,  145 => 41,  142 => 40,  136 => 38,  134 => 37,  127 => 35,  121 => 32,  115 => 29,  111 => 27,  105 => 25,  102 => 24,  96 => 22,  94 => 21,  88 => 20,  80 => 15,  75 => 13,  66 => 7,  62 => 6,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_bold/templates/page--front.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\yg_bold\\templates\\page--front.html.twig");
    }
}
