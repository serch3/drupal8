<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/electra/templates/system/page.html.twig */
class __TwigTemplate_b3e755d3370e246286134b7c9f668907e5b1448a55f3bcf0ea25073fadd5a2a9 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'sidebar_right' => [$this, 'block_sidebar_right'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 19, "include" => 22, "block" => 90];
        $filters = ["clean_class" => 7, "escape" => 38, "without" => 75];
        $functions = ["create_attribute" => 1, "file_url" => 15, "include" => 32];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'include', 'block'],
                ['clean_class', 'escape', 'without'],
                ['create_attribute', 'file_url', 'include']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["page_header_attribute"] = $this->env->getExtension('Drupal\Core\Template\TwigExtension')->createAttribute();
        // line 2
        $context["node_has_image"] = $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_image", []), "entity", []), "uri", []), "value", []);
        // line 3
        echo "
";
        // line 5
        $context["page_header_classes"] = [0 => "page-header-wrapper", 1 => (($this->getAttribute(        // line 7
($context["electra"] ?? null), "page_header_style", [])) ? (("type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["electra"] ?? null), "page_header_style", []))))) : ("empty")), 2 => (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(        // line 8
($context["node"] ?? null), "field_image", []), "entity", []), "uri", []), "value", [])) ? (("has-" . "image")) : ("no-image")), 3 => ((($this->getAttribute(        // line 9
($context["electra"] ?? null), "page_header_style", []) == "box-size")) ? ("container") : (""))];
        // line 12
        echo "
";
        // line 14
        $context["page_header_styles"] = [0 => (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(        // line 15
($context["node"] ?? null), "field_image", []), "entity", []), "uri", []), "value", [])) ? ((("background-image: url(" . call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_image", []), "entity", []), "uri", []), "value", []))])) . ");")) : (""))];
        // line 18
        echo "
";
        // line 19
        if (($this->getAttribute(($context["electra"] ?? null), "header_position", []) == "header-left")) {
            // line 20
            echo "  <div class=\"main-content-wrapper\">
";
        }
        // line 22
        echo "  ";
        $this->loadTemplate("@electra/includes/header.html.twig", "themes/electra/templates/system/page.html.twig", 22)->display($context);
        // line 23
        echo "
  <!-- If header position is left then add wrapper div -->
  ";
        // line 25
        if (($this->getAttribute(($context["electra"] ?? null), "header_position", []) == "header-left")) {
            // line 26
            echo "    <div class=\"content-right\">
  ";
        }
        // line 28
        echo "
    <!-- If sample blocks enable -->
    ";
        // line 30
        if ($this->getAttribute(($context["electra"] ?? null), "display_sample_blocks", [])) {
            // line 31
            echo "      ";
            if (($context["is_front"] ?? null)) {
                // line 32
                echo "        ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(twig_include($this->env, $context, "@electra/samples/parallax-block.html.twig", [], true, true));
                echo "
      ";
            }
            // line 34
            echo "    ";
        }
        // line 35
        echo "    <!-- end sample blocks enable -->
  ";
        // line 36
        if ((($this->getAttribute(($context["electra"] ?? null), "page_header_style", []) == "full-size") || ($this->getAttribute(($context["electra"] ?? null), "page_header_style", []) == "box-size"))) {
            // line 37
            echo "    <div
      ";
            // line 38
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page_header_attribute"] ?? null), "addClass", [0 => ($context["page_header_classes"] ?? null)], "method")), "html", null, true);
            echo "
      ";
            // line 39
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page_header_attribute"] ?? null), "setAttribute", [0 => "style", 1 => ($context["page_header_styles"] ?? null)], "method")), "html", null, true);
            echo ">
    ";
        }
        // line 41
        echo "      ";
        if ( !($context["is_front"] ?? null)) {
            // line 42
            echo "        ";
            if ($this->getAttribute(($context["page"] ?? null), "content_header", [])) {
                // line 43
                echo "          ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_header", [])), "html", null, true);
                echo "
        ";
            }
            // line 45
            echo "      ";
        }
        // line 46
        echo "    ";
        if ((($this->getAttribute(($context["electra"] ?? null), "page_header_style", []) == "full-size") || ($this->getAttribute(($context["electra"] ?? null), "page_header_style", []) == "box-size"))) {
            // line 47
            echo "      </div>
    ";
        }
        // line 49
        echo "
    <!-- top Fluid section -->
    ";
        // line 51
        if ($this->getAttribute(($context["page"] ?? null), "content_top_fluid", [])) {
            // line 52
            echo "      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top_fluid", [])), "html", null, true);
            echo "
    ";
        }
        // line 54
        echo "    <!-- end top fluid section -->

    ";
        // line 57
        $context["content_classes"] = [0 => (($this->getAttribute(        // line 58
($context["page"] ?? null), "sidebar_right", [])) ? ("col-md-9") : ("")), 1 => ((twig_test_empty($this->getAttribute(        // line 59
($context["page"] ?? null), "sidebar_right", []))) ? ("col-md-12") : (""))];
        // line 62
        echo "
    <section class=\"content-wrapper\">
      <div class=\"container\">
        <div class=\"row\">
          <div ";
        // line 66
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

            <!-- page top section -->
            ";
        // line 69
        if ($this->getAttribute(($context["page"] ?? null), "content_top", [])) {
            // line 70
            echo "              ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top", [])), "html", null, true);
            echo "
            ";
        }
        // line 72
        echo "            <!-- page top section -->

    ";
        // line 74
        if ((($context["node_has_image"] ?? null) && (($this->getAttribute(($context["electra"] ?? null), "page_header_style", []) == "full-size") || ($this->getAttribute(($context["electra"] ?? null), "page_header_style", []) == "box-size")))) {
            // line 75
            echo "      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->withoutFilter($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "field_image"), "html", null, true);
            echo "
    ";
        } else {
            // line 77
            echo "      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
    ";
        }
        // line 79
        echo "
            <!-- page bottom section -->
            ";
        // line 81
        if ($this->getAttribute(($context["page"] ?? null), "content_bottom", [])) {
            // line 82
            echo "              ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom", [])), "html", null, true);
            echo "
            ";
        }
        // line 84
        echo "            <!-- page bottom section -->

          </div>

          ";
        // line 88
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_right", [])) {
            // line 89
            echo "            <div class=\"col-md-3 sidebar\">
              ";
            // line 90
            $this->displayBlock('sidebar_right', $context, $blocks);
            // line 95
            echo "            </div>
          ";
        }
        // line 97
        echo "
        </div>
      </div>
    </section>

    <!-- bottom Fluid section -->
    ";
        // line 103
        if ($this->getAttribute(($context["page"] ?? null), "content_bottom_fluid", [])) {
            // line 104
            echo "      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom_fluid", [])), "html", null, true);
            echo "
    ";
        }
        // line 106
        echo "    <!-- bottom top fluid section -->

    ";
        // line 108
        $this->loadTemplate("@electra/includes/footer.html.twig", "themes/electra/templates/system/page.html.twig", 108)->display($context);
        // line 109
        echo "
  ";
        // line 110
        if ($this->getAttribute(($context["electra"] ?? null), "header_position", [])) {
            // line 111
            echo "    </div>
  ";
        }
        // line 113
        echo "
";
        // line 114
        if (($this->getAttribute(($context["electra"] ?? null), "header_position", []) == "header-left")) {
            // line 115
            echo "  </div>
";
        }
    }

    // line 90
    public function block_sidebar_right($context, array $blocks = [])
    {
        // line 91
        echo "                <aside role=\"complementary\">
                  ";
        // line 92
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_right", [])), "html", null, true);
        echo "
                </aside>
              ";
    }

    public function getTemplateName()
    {
        return "themes/electra/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 92,  285 => 91,  282 => 90,  276 => 115,  274 => 114,  271 => 113,  267 => 111,  265 => 110,  262 => 109,  260 => 108,  256 => 106,  250 => 104,  248 => 103,  240 => 97,  236 => 95,  234 => 90,  231 => 89,  229 => 88,  223 => 84,  217 => 82,  215 => 81,  211 => 79,  205 => 77,  199 => 75,  197 => 74,  193 => 72,  187 => 70,  185 => 69,  179 => 66,  173 => 62,  171 => 59,  170 => 58,  169 => 57,  165 => 54,  159 => 52,  157 => 51,  153 => 49,  149 => 47,  146 => 46,  143 => 45,  137 => 43,  134 => 42,  131 => 41,  126 => 39,  122 => 38,  119 => 37,  117 => 36,  114 => 35,  111 => 34,  105 => 32,  102 => 31,  100 => 30,  96 => 28,  92 => 26,  90 => 25,  86 => 23,  83 => 22,  79 => 20,  77 => 19,  74 => 18,  72 => 15,  71 => 14,  68 => 12,  66 => 9,  65 => 8,  64 => 7,  63 => 5,  60 => 3,  58 => 2,  56 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/electra/templates/system/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\electra\\templates\\system\\page.html.twig");
    }
}
