<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/sidus_travel/templates/layout/page.html.twig */
class __TwigTemplate_f91671eb26384d0c68984192f979c14c7b3a8df3aee03b3c6f923f3280a0bcaf extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 65];
        $filters = ["escape" => 67];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 57
        echo "




  <!-- ======= Header ======= -->
  <header id=\"header\" class=\"fixed-top header-inner-pages\">
    <div class=\"container-fluid d-flex align-items-center justify-content-between\">
      ";
        // line 65
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 66
            echo "      <div>
          ";
            // line 67
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
      </div>
      ";
        }
        // line 70
        echo "
      ";
        // line 71
        if ($this->getAttribute(($context["page"] ?? null), "primary_menu", [])) {
            // line 72
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
        ";
        }
        // line 74
        echo "
    </div>
  </header><!-- End Header -->

";
        // line 78
        if (($context["is_front"] ?? null)) {
            // line 79
            echo "  <!-- ======= Hero Section ======= -->
  <!-- Start: Slider Section -->
  ";
            // line 81
            if ($this->getAttribute(($context["page"] ?? null), "slider", [])) {
                // line 82
                echo "  <section id=\"hero\"> 
    <div class=\"hero-container\" data-aos=\"fade-up\" data-aos-delay=\"150\">
      ";
                // line 84
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "slider", [])), "html", null, true);
                echo "
    </div>
  </section>
  ";
            }
            // line 88
            echo "  <!-- End: Slider Section -->
  <!-- End Hero -->
";
        }
        // line 91
        echo "

  <main id=\"main\">
    
      <!-- ======= Breadcrumbs ======= -->
    <!--- Start: Breadcrumb -->
    ";
        // line 97
        if ($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])) {
            // line 98
            echo "    <section id=\"breadcrumbs\" class=\"breadcrumbs\">
      <div class=\"container\">

        <div class=\"d-flex justify-content-between align-items-center\">
          <h2>";
            // line 102
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "#title", [], "array")), "html", null, true);
            echo "</h2>
          <!--h2></h2-->
          ";
            // line 104
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
            echo "
        </div>

      </div>
    </section>
    ";
        }
        // line 110
        echo "    <!-- End: Breadcrumb -->
    <!-- End Breadcrumbs -->


    <!-- ======= About Section ======= -->
    <!-- Start: Top Section of Content -->
    ";
        // line 116
        if ($this->getAttribute(($context["page"] ?? null), "content_top", [])) {
            // line 117
            echo "    <section id=\"about\" class=\"about\">
      <div class=\"container\" data-aos=\"fade-up\">
        ";
            // line 119
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 123
        echo "    <!-- End: Top Section of Content -->
    <!-- End About Section -->


<section id=\"about1\" class=\"about1\">
    <div class=\"container\" data-aos=\"fade-up\">

  <div class=\"row layout\">
      <!--- Start Left SideBar -->
      ";
        // line 132
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 133
            echo "        <div class=\"sidebar\" >
          <div class = ";
            // line 134
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarfirst"] ?? null)), "html", null, true);
            echo " >
            ";
            // line 135
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
          </div>
        </div>
      ";
        }
        // line 139
        echo "      <!---End Right SideBar -->

  <!--- Start content -->
      ";
        // line 142
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 143
            echo "            <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contentlayout"] ?? null)), "html", null, true);
            echo ">
            ";
            // line 144
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
            </div>
      ";
        }
        // line 147
        echo "  <!---End content -->

    <!--- Start Right SideBar -->
      ";
        // line 150
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 151
            echo "        <div class=\"sidebar\">
          <div class=";
            // line 152
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarsecond"] ?? null)), "html", null, true);
            echo ">
            ";
            // line 153
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
          </div>
        </div>
      ";
        }
        // line 157
        echo "      <!---End Right SideBar -->

   </div>

</div>
        </section>



    <!-- ======= About Boxes Section ======= -->
     ";
        // line 167
        if (($context["is_front"] ?? null)) {
            // line 168
            echo "     <section id=\"about-boxes\" class=\"about-boxes\">
      <div class=\"container\" data-aos=\"fade-up\"> 
        <div class=\"row\">

          <div class=\"col-lg-4 col-md-6 d-flex align-items-stretch\" data-aos=\"fade-up\" data-aos-delay=\"100\">

            <!-- Start: Content Bottom First Section -->
            ";
            // line 175
            if ($this->getAttribute(($context["page"] ?? null), "content_bottom_first", [])) {
                // line 176
                echo "            <div class=\"card\">
              ";
                // line 177
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom_first", [])), "html", null, true);
                echo "
            </div>
            ";
            }
            // line 180
            echo "            <!-- End: Content Bottom First Section -->

          </div>

          <div class=\"col-lg-4 col-md-6 d-flex align-items-stretch\" data-aos=\"fade-up\" data-aos-delay=\"200\">

            <!-- Start: Content Bottom Second Section -->
            ";
            // line 187
            if ($this->getAttribute(($context["page"] ?? null), "content_bottom_second", [])) {
                // line 188
                echo "            <div class=\"card\">
              ";
                // line 189
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom_second", [])), "html", null, true);
                echo "
            </div>
            ";
            }
            // line 192
            echo "            <!-- End: Content Bottom Second Section -->

          </div>

          <div class=\"col-lg-4 col-md-6 d-flex align-items-stretch\" data-aos=\"fade-up\" data-aos-delay=\"300\">

            <!-- Start: Content Bottom Third Section -->
            ";
            // line 199
            if ($this->getAttribute(($context["page"] ?? null), "content_bottom_third", [])) {
                // line 200
                echo "            <div class=\"card\">
              ";
                // line 201
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom_third", [])), "html", null, true);
                echo "
            </div>
            ";
            }
            // line 204
            echo "            <!-- End: Content Bottom Third Section -->

          </div>

        </div>
      </div>
    </section>
    ";
        }
        // line 212
        echo "    <!-- End About Boxes Section -->


    <!-- ======= Clients Section ======= -->
    <!-- Start: Our Clients -->
    ";
        // line 217
        if ($this->getAttribute(($context["page"] ?? null), "clients", [])) {
            // line 218
            echo "    <section id=\"clients\" class=\"clients\">
      <div class=\"container\" data-aos=\"zoom-in\">
        ";
            // line 220
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "clients", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 224
        echo "    <!-- End: Our Clients -->
    <!-- End Clients Section -->
  
    
   

    <!-- ======= Features Section ======= -->
    <!-- Start: Our Features -->
    ";
        // line 232
        if ($this->getAttribute(($context["page"] ?? null), "features", [])) {
            // line 233
            echo "    <section id=\"features\" class=\"features\">
      <div class=\"container\" data-aos=\"fade-up\">
        ";
            // line 235
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 239
        echo "    <!-- End: Our Features -->
    <!-- End Features Section -->



    <!-- ======= Services Section ======= -->
    <!-- Start: Our Services -->
    ";
        // line 246
        if ($this->getAttribute(($context["page"] ?? null), "services", [])) {
            // line 247
            echo "    <section id=\"services\" class=\"services section-bg\">
      <div class=\"container\" data-aos=\"fade-up\">
        ";
            // line 249
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 253
        echo "    <!-- End: Our Services -->
    <!-- End Services Section -->


    <!-- ======= Testimonials Section ======= -->
    <!-- Start: Our Testimonials -->
    ";
        // line 259
        if ($this->getAttribute(($context["page"] ?? null), "testimonials", [])) {
            // line 260
            echo "    <section id=\"testimonials\" class=\"testimonials\">
      <div class=\"container\" data-aos=\"zoom-in\">
        ";
            // line 262
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "testimonials", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 266
        echo "    <!-- End: Our Testimonials -->
    <!-- End Testimonials Section -->


    <!-- ======= Portfolio Section ======= -->
    <!-- Start: Our Portfolio -->
    ";
        // line 272
        if ($this->getAttribute(($context["page"] ?? null), "portfolio", [])) {
            // line 273
            echo "    <section id=\"portfolio\" class=\"portfolio\">
      <div class=\"container\" data-aos=\"fade-up\">
        ";
            // line 275
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "portfolio", [])), "html", null, true);
            echo "
      </div>
    </section>
    ";
        }
        // line 279
        echo "    <!-- End: Our Portfolio -->
    <!-- End Portfolio Section -->


    <!-- ======= Team Section ======= -->
    <!-- Start: Our Team -->
      ";
        // line 285
        if ($this->getAttribute(($context["page"] ?? null), "team", [])) {
            // line 286
            echo "    <section id=\"team\" class=\"team section-bg\">
      <div class=\"container\" data-aos=\"fade-up\">
        ";
            // line 288
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "team", [])), "html", null, true);
            echo "
      </div>   
    </section>
    ";
        }
        // line 292
        echo "    <!-- End: Our Team -->
    <!-- End Team Section -->



    <!-- ======= Contact Section ======= -->
    <!-- Start: Contact Us Section -->
    ";
        // line 299
        if ($this->getAttribute(($context["page"] ?? null), "contact_us", [])) {
            // line 300
            echo "    <section id=\"contact\" class=\"contact\">
      <div class=\"container\" data-aos=\"fade-up\"\">
      ";
            // line 302
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "contact_us", [])), "html", null, true);
            echo "   
      </div>
    </section>
    ";
        }
        // line 306
        echo "    <!-- End: Contact Us Section -->
    <!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id=\"footer\">
    <div class=\"footer-top\">
      <div class=\"container\">
        <div class=\"row\">

          <div class=\"col-lg-3 col-md-6\">
            
            <!-- Start: Footer First Container -->
            ";
        // line 320
        if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
            // line 321
            echo "            <div class=\"footer-info\">
              ";
            // line 322
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
            echo "
            </div>
            ";
        }
        // line 325
        echo "            <!-- End: Footer First Container -->

          </div>
         
          <!-- Start: Footer Second Container -->
          ";
        // line 330
        if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
            // line 331
            echo "          <div class=\"col-lg-2 col-md-6 footer-links\">
            ";
            // line 332
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 335
        echo "          <!-- End: Footer Second Container -->

          <!-- Start: Footer Third Container -->
          ";
        // line 338
        if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
            // line 339
            echo "          <div class=\"col-lg-3 col-md-6 footer-links\">
            ";
            // line 340
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 343
        echo "          <!-- End: Footer Third Container -->

          <!-- Start: Footer Forth Container -->
          ";
        // line 346
        if ($this->getAttribute(($context["page"] ?? null), "footer_forth", [])) {
            // line 347
            echo "          <div class=\"col-lg-4 col-md-6 footer-newsletter\">
            ";
            // line 348
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_forth", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 351
        echo "          <!-- End: Footer Forth Container -->

        </div>
      </div>
    </div>
   
   <!-- Start: Footer Copyright -->
   ";
        // line 358
        if ($this->getAttribute(($context["page"] ?? null), "footer_copyright", [])) {
            // line 359
            echo "    <div class=\"container\">
      ";
            // line 360
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_copyright", [])), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 363
        echo "     <!-- End: Footer Copyright -->

  </footer><!-- End Footer -->

  <a href=\"#\" class=\"back-to-top\"><i class=\"ri-arrow-up-line\"></i></a>


 

";
    }

    public function getTemplateName()
    {
        return "themes/sidus_travel/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  565 => 363,  559 => 360,  556 => 359,  554 => 358,  545 => 351,  539 => 348,  536 => 347,  534 => 346,  529 => 343,  523 => 340,  520 => 339,  518 => 338,  513 => 335,  507 => 332,  504 => 331,  502 => 330,  495 => 325,  489 => 322,  486 => 321,  484 => 320,  468 => 306,  461 => 302,  457 => 300,  455 => 299,  446 => 292,  439 => 288,  435 => 286,  433 => 285,  425 => 279,  418 => 275,  414 => 273,  412 => 272,  404 => 266,  397 => 262,  393 => 260,  391 => 259,  383 => 253,  376 => 249,  372 => 247,  370 => 246,  361 => 239,  354 => 235,  350 => 233,  348 => 232,  338 => 224,  331 => 220,  327 => 218,  325 => 217,  318 => 212,  308 => 204,  302 => 201,  299 => 200,  297 => 199,  288 => 192,  282 => 189,  279 => 188,  277 => 187,  268 => 180,  262 => 177,  259 => 176,  257 => 175,  248 => 168,  246 => 167,  234 => 157,  227 => 153,  223 => 152,  220 => 151,  218 => 150,  213 => 147,  207 => 144,  202 => 143,  200 => 142,  195 => 139,  188 => 135,  184 => 134,  181 => 133,  179 => 132,  168 => 123,  161 => 119,  157 => 117,  155 => 116,  147 => 110,  138 => 104,  133 => 102,  127 => 98,  125 => 97,  117 => 91,  112 => 88,  105 => 84,  101 => 82,  99 => 81,  95 => 79,  93 => 78,  87 => 74,  81 => 72,  79 => 71,  76 => 70,  70 => 67,  67 => 66,  65 => 65,  55 => 57,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/sidus_travel/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\sidus_travel\\templates\\layout\\page.html.twig");
    }
}
