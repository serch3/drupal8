<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/yg_bold/templates/page.html.twig */
class __TwigTemplate_5593e14532909f28d2bae73f7ea41ce382ebdf69095ba39d0f4e6d29642c71c3 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 16, "if" => 17];
        $filters = ["escape" => 6, "trim" => 16, "striptags" => 16, "render" => 16, "raw" => 48];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape', 'trim', 'striptags', 'render', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id=\"fh5co-page\">
<!-- Header -->
   <header id=\"fh5co-header\" role=\"banner\">
      <div class=\"container\">
         <div class=\"header-inner\">
            ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "site_branding", [])), "html", null, true);
        echo "
               ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
        echo " 
         </div>
      </div>
   </header>

<!-- Tabs -->
  ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "tabs", [])), "html", null, true);
        echo "

<!-- Banner -->
";
        // line 16
        $context["banner"] =  !twig_test_empty(twig_trim_filter(strip_tags($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "banner", []))))));
        // line 17
        if (($context["banner"] ?? null)) {
            // line 18
            echo "  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "banner", [])), "html", null, true);
            echo "
";
        } else {
            // line 20
            echo "  <aside id=\"fh5co-hero\" clsas=\"js-fullheight\">
    <div class=\"flexslider js-fullheight\">
      <ul class=\"slides\">
        <li style=\"background-image: url(";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["basepath"] ?? null)), "html", null, true);
            echo "/themes/yg_bold/images/slide_3.jpg);\">
          <div class=\"overlay-gradient\"></div>
          <div class=\"container\">
            <div class=\"col-md-10 col-md-offset-1 text-center js-fullheight slider-text\">
              <div class=\"slider-text-inner\">
                <h2>";
            // line 28
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "#title", [], "array")), "html", null, true);
            echo "</h2>
                <p class=\"fh5co-lead\">Free Drupal 8 Theme by Drupal Developers Studio <i class=\"icon-heart\"></i> <a href=\"https://www.drupaldevelopersstudio.com\"> Drupal Developers Studio</a></p>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </aside>
";
        }
        // line 37
        echo "  

<!-- Content -->
";
        // line 40
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
   
<!-- Footer -->
   <footer id=\"fh5co-footer\" role=\"contentinfo\">
      <div class=\"container\">
         <div class=\"col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            <h3>";
        // line 46
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["about_us_title"] ?? null)))) {
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_us_title"] ?? null)), "html", null, true);
        }
        echo "</h3>
            ";
        // line 47
        if (($context["about_desc"] ?? null)) {
            // line 48
            echo "              <p>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["about_desc"] ?? null)));
            echo "</p>
            ";
        }
        // line 50
        echo "            ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["about_us_url"] ?? null)))) {
            // line 51
            echo "              <p><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["about_us_url"] ?? null)), "html", null, true);
            echo "\" class=\"btn btn-primary btn-outline with-arrow btn-sm\">Read More <i class=\"icon-arrow-right\"></i></a></p>
            ";
        }
        // line 53
        echo "         </div>
         <div class=\"col-md-3 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            ";
        // line 55
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "latest_blog", [])), "html", null, true);
        echo "
         </div>
         <div class=\"col-md-3 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            ";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_menu", [])), "html", null, true);
        echo "
         </div>
         <div class=\"col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0\">
            <h3>";
        // line 61
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["social_title"] ?? null)))) {
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_title"] ?? null)), "html", null, true);
        }
        echo "</h3>
            <ul class=\"fh5co-social\">
               ";
        // line 63
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["twitter_url"] ?? null)))) {
            // line 64
            echo "                  <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-twitter\"></i></a></li>
               ";
        }
        // line 66
        echo "               ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["facebook_url"] ?? null)))) {
            // line 67
            echo "                  <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-facebook\"></i></a></li>
               ";
        }
        // line 69
        echo "               ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["google_plus_url"] ?? null)))) {
            // line 70
            echo "                  <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["google_plus_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-google-plus\"></i></a></li>
               ";
        }
        // line 72
        echo "               ";
        if ( !twig_test_empty($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["instagram_url"] ?? null)))) {
            // line 73
            echo "                 <li><a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram_url"] ?? null)), "html", null, true);
            echo "\"><i class=\"icon-instagram\"></i></a></li>
               ";
        }
        // line 74
        echo "     
            </ul>
         </div>
         <!-- Copyright -->
          <div class=\"col-md-12 fh5co-copyright text-center\">
              <p><span> &copy; 2018 <i>YG Bold</i> All Rights Reserved. Designed with <i class=\"icon-heart\"></i> by <a href=\"http://freehtml5.co\" target=\"_blank\">FREEHTML5.co</a><br> Theme By<a href=\"https://www.drupaldevelopersstudio.com\" target=\"_blank\"> Drupal Developers Studio</a>, A Division of <a href=\"http://www.youngglobes.com\" target=\"_blank\">Young Globes</a></span></p> 
          </div>
      </div>
   </footer>
</div>
      
   ";
    }

    public function getTemplateName()
    {
        return "themes/yg_bold/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 74,  206 => 73,  203 => 72,  197 => 70,  194 => 69,  188 => 67,  185 => 66,  179 => 64,  177 => 63,  170 => 61,  164 => 58,  158 => 55,  154 => 53,  148 => 51,  145 => 50,  139 => 48,  137 => 47,  131 => 46,  122 => 40,  117 => 37,  104 => 28,  96 => 23,  91 => 20,  85 => 18,  83 => 17,  81 => 16,  75 => 13,  66 => 7,  62 => 6,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/yg_bold/templates/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\yg_bold\\templates\\page.html.twig");
    }
}
