<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/great_zymphonies_theme/templates/layout/page.html.twig */
class __TwigTemplate_2a642ac31a0cf0b2a7892511cc213af03eda31f9f62740a42a5ae9d9e882c4bb extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 71, "for" => 155];
        $filters = ["escape" => 72, "raw" => 156, "date" => 579];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape', 'raw', 'date'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 60
        echo "
<!-- Start: Top Bar -->

<div class=\"container page-wrapper\">

  <div class=\"top-nav\">
    <div class=\"container-\">
      <div class=\"row\">

        <div class=\"col-md-6\">

          ";
        // line 71
        if ($this->getAttribute(($context["page"] ?? null), "message", [])) {
            // line 72
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "message", [])), "html", null, true);
            echo "
          ";
        }
        // line 74
        echo "
        </div>
        
        ";
        // line 77
        if (($context["show_social_icon"] ?? null)) {
            // line 78
            echo "          <div class=\"col-md-6\">
            <p class=\"social-media\">
              ";
            // line 80
            if (($context["facebook_url"] ?? null)) {
                // line 81
                echo "                <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
                echo "\"  class=\"facebook\" target=\"_blank\" ><i class=\"fab fa-facebook-f\"></i></a>
              ";
            }
            // line 83
            echo "              ";
            if (($context["google_plus_url"] ?? null)) {
                // line 84
                echo "                <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["google_plus_url"] ?? null)), "html", null, true);
                echo "\"  class=\"google-plus\" target=\"_blank\" ><i class=\"fab fa-google-plus-g\"></i></a>
              ";
            }
            // line 86
            echo "              ";
            if (($context["twitter_url"] ?? null)) {
                // line 87
                echo "                <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
                echo "\" class=\"twitter\" target=\"_blank\" ><i class=\"fab fa-twitter\"></i></a>
              ";
            }
            // line 89
            echo "              ";
            if (($context["linkedin_url"] ?? null)) {
                // line 90
                echo "                <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin_url"] ?? null)), "html", null, true);
                echo "\" class=\"linkedin\" target=\"_blank\"><i class=\"fab fa-linkedin-in\"></i></a>
              ";
            }
            // line 92
            echo "              ";
            if (($context["pinterest_url"] ?? null)) {
                // line 93
                echo "                <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pinterest_url"] ?? null)), "html", null, true);
                echo "\" class=\"pinterest\" target=\"_blank\" ><i class=\"fab fa-pinterest-p\"></i></a>
              ";
            }
            // line 95
            echo "              ";
            if (($context["rss_url"] ?? null)) {
                // line 96
                echo "                <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["rss_url"] ?? null)), "html", null, true);
                echo "\" class=\"rss\" target=\"_blank\" ><i class=\"fa fa-rss\"></i></a>
              ";
            }
            // line 98
            echo "            </p>
          </div>
        ";
        }
        // line 101
        echo "       
      </div>
    </div>
  </div>

  <!-- End: Top Bar -->


  <!-- Start: Header -->

  <div class=\"header\">
    <div class=\"container-\">
      <div class=\"row\">

        <div class=\"navbar-header col-md-3\">
          
          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#main-navigation\">
            <i class=\"fas fa-bars\"></i>
          </button>

          ";
        // line 121
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 122
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
          ";
        }
        // line 124
        echo "
        </div>

        ";
        // line 127
        if (($this->getAttribute(($context["page"] ?? null), "primary_menu", []) || $this->getAttribute(($context["page"] ?? null), "search", []))) {
            // line 128
            echo "          <div class=\"col-md-9\">

            ";
            // line 130
            if ($this->getAttribute(($context["page"] ?? null), "quicklink", [])) {
                // line 131
                echo "              ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "quicklink", [])), "html", null, true);
                echo "
            ";
            }
            // line 133
            echo "
            ";
            // line 134
            if ($this->getAttribute(($context["page"] ?? null), "primary_menu", [])) {
                // line 135
                echo "              ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
                echo "
            ";
            }
            // line 137
            echo "            
          </div>
        ";
        }
        // line 140
        echo "
      </div>
    </div>
  </div>

  <!-- End: Header -->


  <!-- Start: Slides -->

  ";
        // line 150
        if ((($context["is_front"] ?? null) && ($context["show_slideshow"] ?? null))) {
            // line 151
            echo "    
    <div class=\"container-\">
      <div class=\"flexslider\">
        <ul class=\"slides\">
          ";
            // line 155
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["slider_content"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["slider_contents"]) {
                // line 156
                echo "            ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed($context["slider_contents"]));
                echo "
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider_contents'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 158
            echo "        </ul>
      </div>
    </div>

  ";
        }
        // line 163
        echo "
  <!-- End: Slides -->


  <!-- Start: Clients -->

  ";
        // line 169
        if ($this->getAttribute(($context["page"] ?? null), "clients", [])) {
            echo " 

    <div class=\"clients\" id=\"clients\">

      ";
            // line 173
            if ($this->getAttribute(($context["page"] ?? null), "clients_title", [])) {
                // line 174
                echo "        <div class=\"custom-block-title\" >
          ";
                // line 175
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "clients_title", [])), "html", null, true);
                echo "
        </div>
      ";
            }
            // line 178
            echo "
      <div class=\"container\">
        ";
            // line 180
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "clients", [])), "html", null, true);
            echo "
      </div>

    </div>

  ";
        }
        // line 186
        echo "
  <!--End: Clients -->


  <!--Start: Top message -->

  ";
        // line 192
        if ($this->getAttribute(($context["page"] ?? null), "topmessage", [])) {
            // line 193
            echo "
    <div class=\"top-message\">
      <div class=\"container\">
        ";
            // line 196
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topmessage", [])), "html", null, true);
            echo "
      </div>
    </div>

  ";
        }
        // line 201
        echo "
  <!--End: Top message -->


  <!-- Start: Top widget -->

  ";
        // line 207
        if ((($this->getAttribute(($context["page"] ?? null), "topwidget_first", []) || $this->getAttribute(($context["page"] ?? null), "topwidget_second", [])) || $this->getAttribute(($context["page"] ?? null), "topwidget_third", []))) {
            // line 208
            echo "    
    <div class=\"topwidget\" id=\"topwidget\">
      <div class=\"container\">

        ";
            // line 212
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_title", [])) {
                // line 213
                echo "          <div class=\"custom-block-title\">
            ";
                // line 214
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_title", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 217
            echo "
        <div class=\"row topwidget-list clearfix\">
        
          ";
            // line 220
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_first", [])) {
                // line 221
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_first", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 223
            echo "         
          ";
            // line 224
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_second", [])) {
                // line 225
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_second", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 227
            echo "                 
          ";
            // line 228
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_third", [])) {
                // line 229
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_third", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 231
            echo "
        </div>

      </div>
    </div>

  ";
        }
        // line 238
        echo "
  <!--End: Top widget -->

      
  <!--Start: Highlighted -->

  ";
        // line 244
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 245
            echo "
    <div class=\"highlighted\">
      <div class=\"container\">
        ";
            // line 248
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
      </div>
    </div>

  ";
        }
        // line 253
        echo "
  <!--End: Highlighted -->


  <!--Start: Title -->

  ";
        // line 259
        if (($this->getAttribute(($context["page"] ?? null), "page_title", []) &&  !($context["is_front"] ?? null))) {
            // line 260
            echo "
    <div id=\"page-title\">
      <div id=\"page-title-inner\">
        <div class=\"container\">
          ";
            // line 264
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_title", [])), "html", null, true);
            echo "
        </div>
      </div>
    </div>

  ";
        }
        // line 270
        echo "
  <!--End: Title -->


  <div class=\"main-content\">
    <div class=\"container\">
      <div class=\"\">

        <!--Start: Breadcrumb -->

        ";
        // line 280
        if ( !($context["is_front"] ?? null)) {
            // line 281
            echo "          <div class=\"row\">
            <div class=\"col-md-12\">";
            // line 282
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
            echo "</div>
          </div>
        ";
        }
        // line 285
        echo "
        <!--End: Breadcrumb -->

        <div class=\"row layout\">

          <!--- Start: Left sidebar -->
          ";
        // line 291
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 292
            echo "            <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarfirst"] ?? null)), "html", null, true);
            echo ">
              <div class=\"sidebar\">
                ";
            // line 294
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
              </div>
            </div>
          ";
        }
        // line 298
        echo "          <!-- End Left sidebar -->

          <!--- Start Content -->
          ";
        // line 301
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 302
            echo "            <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contentlayout"] ?? null)), "html", null, true);
            echo ">
              <div class=\"content_layout\">
                ";
            // line 304
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
              </div>              
            </div>
          ";
        }
        // line 308
        echo "          <!-- End: Content -->

          <!-- Start: Right sidebar -->
          ";
        // line 311
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 312
            echo "            <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarsecond"] ?? null)), "html", null, true);
            echo ">
              <div class=\"sidebar\">
                ";
            // line 314
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
              </div>
            </div>
          ";
        }
        // line 318
        echo "          <!-- End: Right sidebar -->
          
        </div>
      
      </div>
    </div>
  </div>

  <!-- End: Main content -->


  <!-- Start: Features -->

  ";
        // line 331
        if ((($this->getAttribute(($context["page"] ?? null), "features_first", []) || $this->getAttribute(($context["page"] ?? null), "features_second", [])) || $this->getAttribute(($context["page"] ?? null), "features_third", []))) {
            // line 332
            echo "
    <div class=\"features\">
      <div class=\"container\">

        ";
            // line 336
            if ($this->getAttribute(($context["page"] ?? null), "features_title", [])) {
                // line 337
                echo "          <div class=\"custom-block-title\" >
            ";
                // line 338
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_title", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 341
            echo "
        <div class=\"row features-list\">

          ";
            // line 344
            if ($this->getAttribute(($context["page"] ?? null), "features_first", [])) {
                // line 345
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_first_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 346
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 349
            echo "
          ";
            // line 350
            if ($this->getAttribute(($context["page"] ?? null), "features_second", [])) {
                // line 351
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 352
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 355
            echo "
          ";
            // line 356
            if ($this->getAttribute(($context["page"] ?? null), "features_third", [])) {
                // line 357
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 358
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 361
            echo "
        </div>

      </div>
    </div>

  ";
        }
        // line 368
        echo "
  <!--End: Features -->


  <!--Start: FAQ -->

  ";
        // line 374
        if ($this->getAttribute(($context["page"] ?? null), "faq", [])) {
            // line 375
            echo "
    <div class=\"faq\" id=\"faq\">
      <div class=\"container\">
        ";
            // line 378
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "faq", [])), "html", null, true);
            echo "
      </div>
    </div>

  ";
        }
        // line 383
        echo "
  <!--End: FAQ -->


  <!-- Start: Updates widgets -->

  ";
        // line 389
        if ((($this->getAttribute(($context["page"] ?? null), "updates_first", []) || $this->getAttribute(($context["page"] ?? null), "updates_second", [])) || $this->getAttribute(($context["page"] ?? null), "updates_third", []))) {
            // line 390
            echo "
    <div class=\"updates\" id=\"updates\">    
      <div class=\"container\">

        ";
            // line 394
            if ($this->getAttribute(($context["page"] ?? null), "updates_title", [])) {
                // line 395
                echo "          <div class=\"custom-block-title\" >
            ";
                // line 396
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_title", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 399
            echo "
        <div class=\"row updates-list\">
          
          ";
            // line 402
            if ($this->getAttribute(($context["page"] ?? null), "updates_first", [])) {
                // line 403
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["updates_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 404
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 407
            echo "
          ";
            // line 408
            if ($this->getAttribute(($context["page"] ?? null), "updates_second", [])) {
                // line 409
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["updates_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 410
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 413
            echo "          
          ";
            // line 414
            if ($this->getAttribute(($context["page"] ?? null), "updates_third", [])) {
                // line 415
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["updates_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 416
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 419
            echo "           
          ";
            // line 420
            if ($this->getAttribute(($context["page"] ?? null), "updates_forth", [])) {
                // line 421
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["updates_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 422
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_forth", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 425
            echo "
        </div>
      </div>
    </div>

  ";
        }
        // line 431
        echo "
  <!--End: Updates widgets -->


  <!-- Start: Middle widgets -->

  ";
        // line 437
        if (((($this->getAttribute(($context["page"] ?? null), "midwidget_first", []) || $this->getAttribute(($context["page"] ?? null), "midwidget_second", [])) || $this->getAttribute(($context["page"] ?? null), "midwidget_third", [])) || $this->getAttribute(($context["page"] ?? null), "midwidget_forth", []))) {
            // line 438
            echo "
    <div class=\"midwidget\" id=\"midwidget\">    
      <div class=\"container\">

        ";
            // line 442
            if ($this->getAttribute(($context["page"] ?? null), "midwidget_title", [])) {
                // line 443
                echo "          <div class=\"custom-block-title\" >
            ";
                // line 444
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "midwidget_title", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 447
            echo "
        <div class=\"row midwidget-list\">
           
          ";
            // line 450
            if ($this->getAttribute(($context["page"] ?? null), "midwidget_first", [])) {
                // line 451
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["midwidget_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 452
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "midwidget_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 455
            echo "
          ";
            // line 456
            if ($this->getAttribute(($context["page"] ?? null), "midwidget_second", [])) {
                // line 457
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["midwidget_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 458
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "midwidget_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 461
            echo "          
          ";
            // line 462
            if ($this->getAttribute(($context["page"] ?? null), "midwidget_third", [])) {
                // line 463
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["midwidget_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 464
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "midwidget_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 467
            echo "
          ";
            // line 468
            if ($this->getAttribute(($context["page"] ?? null), "midwidget_forth", [])) {
                // line 469
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["midwidget_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 470
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "midwidget_forth", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 473
            echo "
        </div>

      </div>
    </div>

  ";
        }
        // line 480
        echo "
  <!--End: Middle widgets -->


  <!-- Start: Bottom widgets -->

  ";
        // line 486
        if (((($this->getAttribute(($context["page"] ?? null), "bottom_first", []) || $this->getAttribute(($context["page"] ?? null), "bottom_second", [])) || $this->getAttribute(($context["page"] ?? null), "bottom_third", [])) || $this->getAttribute(($context["page"] ?? null), "bottom_forth", []))) {
            // line 487
            echo "
    <div class=\"btmwidget\" id=\"btmwidget\">    
      <div class=\"container\">

        ";
            // line 491
            if ($this->getAttribute(($context["page"] ?? null), "bottom_title", [])) {
                // line 492
                echo "          <div class=\"custom-block-title\" >
            ";
                // line 493
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_title", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 496
            echo "
        <div class=\"row btmwidget-list\">
          
          ";
            // line 499
            if ($this->getAttribute(($context["page"] ?? null), "bottom_first", [])) {
                // line 500
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 501
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 504
            echo "
          ";
            // line 505
            if ($this->getAttribute(($context["page"] ?? null), "bottom_second", [])) {
                // line 506
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 507
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 510
            echo "          
          ";
            // line 511
            if ($this->getAttribute(($context["page"] ?? null), "bottom_third", [])) {
                // line 512
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 513
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 516
            echo "
          ";
            // line 517
            if ($this->getAttribute(($context["page"] ?? null), "bottom_forth", [])) {
                // line 518
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bottom_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 519
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_forth", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 522
            echo "
        </div>
      </div>
    </div>

  ";
        }
        // line 528
        echo "
  <!--End: Bottom widgets -->


  <!-- Start: Footer widgets -->

  ";
        // line 534
        if ((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", []))) {
            // line 535
            echo "
    <div class=\"footer\" id=\"footer\">
      <div class=\"container\">

        ";
            // line 539
            if ($this->getAttribute(($context["page"] ?? null), "footer_title", [])) {
                // line 540
                echo "          <div class=\"custom-block-title\" >
            ";
                // line 541
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_title", [])), "html", null, true);
                echo "
          </div>
        ";
            }
            // line 544
            echo "
        <div class=\"row footer-list\">

          ";
            // line 547
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
                // line 548
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 549
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 552
            echo "
          ";
            // line 553
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
                // line 554
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 555
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 558
            echo "
          ";
            // line 559
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
                // line 560
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 561
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 564
            echo "
        </div>
      </div>
    </div>

  ";
        }
        // line 570
        echo "
  <!--End: Footer widgets -->


  <!-- Start: Copyright -->

  <div class=\"copyright\">
    <div class=\"container\">

      <span>Copyright © ";
        // line 579
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo ". All rights reserved.</span>
      ";
        // line 580
        if (($context["show_credit_link"] ?? null)) {
            // line 581
            echo "        <span class=\"credit-link\">Designed By <a href=\"http://www.zymphonies.com\" target=\"_blank\">Zymphonies</a></span>
      ";
        }
        // line 583
        echo "
    </div>
  </div>

</div>

<!-- End: Copyright -->





";
    }

    public function getTemplateName()
    {
        return "themes/great_zymphonies_theme/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1049 => 583,  1045 => 581,  1043 => 580,  1039 => 579,  1028 => 570,  1020 => 564,  1014 => 561,  1009 => 560,  1007 => 559,  1004 => 558,  998 => 555,  993 => 554,  991 => 553,  988 => 552,  982 => 549,  977 => 548,  975 => 547,  970 => 544,  964 => 541,  961 => 540,  959 => 539,  953 => 535,  951 => 534,  943 => 528,  935 => 522,  929 => 519,  924 => 518,  922 => 517,  919 => 516,  913 => 513,  908 => 512,  906 => 511,  903 => 510,  897 => 507,  892 => 506,  890 => 505,  887 => 504,  881 => 501,  876 => 500,  874 => 499,  869 => 496,  863 => 493,  860 => 492,  858 => 491,  852 => 487,  850 => 486,  842 => 480,  833 => 473,  827 => 470,  822 => 469,  820 => 468,  817 => 467,  811 => 464,  806 => 463,  804 => 462,  801 => 461,  795 => 458,  790 => 457,  788 => 456,  785 => 455,  779 => 452,  774 => 451,  772 => 450,  767 => 447,  761 => 444,  758 => 443,  756 => 442,  750 => 438,  748 => 437,  740 => 431,  732 => 425,  726 => 422,  721 => 421,  719 => 420,  716 => 419,  710 => 416,  705 => 415,  703 => 414,  700 => 413,  694 => 410,  689 => 409,  687 => 408,  684 => 407,  678 => 404,  673 => 403,  671 => 402,  666 => 399,  660 => 396,  657 => 395,  655 => 394,  649 => 390,  647 => 389,  639 => 383,  631 => 378,  626 => 375,  624 => 374,  616 => 368,  607 => 361,  601 => 358,  596 => 357,  594 => 356,  591 => 355,  585 => 352,  580 => 351,  578 => 350,  575 => 349,  569 => 346,  564 => 345,  562 => 344,  557 => 341,  551 => 338,  548 => 337,  546 => 336,  540 => 332,  538 => 331,  523 => 318,  516 => 314,  510 => 312,  508 => 311,  503 => 308,  496 => 304,  490 => 302,  488 => 301,  483 => 298,  476 => 294,  470 => 292,  468 => 291,  460 => 285,  454 => 282,  451 => 281,  449 => 280,  437 => 270,  428 => 264,  422 => 260,  420 => 259,  412 => 253,  404 => 248,  399 => 245,  397 => 244,  389 => 238,  380 => 231,  372 => 229,  370 => 228,  367 => 227,  359 => 225,  357 => 224,  354 => 223,  346 => 221,  344 => 220,  339 => 217,  333 => 214,  330 => 213,  328 => 212,  322 => 208,  320 => 207,  312 => 201,  304 => 196,  299 => 193,  297 => 192,  289 => 186,  280 => 180,  276 => 178,  270 => 175,  267 => 174,  265 => 173,  258 => 169,  250 => 163,  243 => 158,  234 => 156,  230 => 155,  224 => 151,  222 => 150,  210 => 140,  205 => 137,  199 => 135,  197 => 134,  194 => 133,  188 => 131,  186 => 130,  182 => 128,  180 => 127,  175 => 124,  169 => 122,  167 => 121,  145 => 101,  140 => 98,  134 => 96,  131 => 95,  125 => 93,  122 => 92,  116 => 90,  113 => 89,  107 => 87,  104 => 86,  98 => 84,  95 => 83,  89 => 81,  87 => 80,  83 => 78,  81 => 77,  76 => 74,  70 => 72,  68 => 71,  55 => 60,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/great_zymphonies_theme/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\great_zymphonies_theme\\templates\\layout\\page.html.twig");
    }
}
