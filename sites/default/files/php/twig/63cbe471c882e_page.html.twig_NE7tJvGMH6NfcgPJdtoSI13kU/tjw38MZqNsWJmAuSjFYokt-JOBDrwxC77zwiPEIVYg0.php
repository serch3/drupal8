<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/hope_zymphonies_theme/templates/layout/page.html.twig */
class __TwigTemplate_6ea324a6ba9f72fb1d12784060aa29f3a5ddfb66b8b36610906f3cbffd9394df extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 67, "for" => 115];
        $filters = ["escape" => 68, "raw" => 116, "date" => 444];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape', 'raw', 'date'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 60
        echo "
<div class=\"main-container\">
  <div class=\"container main-container-wrap\">

    <div class=\"row\">
      <div class=\"col-md-3\">
        <div class=\"left-wrapper\">
          ";
        // line 67
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 68
            echo "            ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
          ";
        }
        // line 70
        echo "
          ";
        // line 71
        if (($context["show_social_icon"] ?? null)) {
            // line 72
            echo "            <div class=\"lhs-bottom-container\">
              ";
            // line 73
            if (($context["show_social_icon"] ?? null)) {
                // line 74
                echo "                <div class=\"social-media\">
                  ";
                // line 75
                if (($context["facebook_url"] ?? null)) {
                    // line 76
                    echo "                    <a href=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
                    echo "\" class=\"facebook\" target=\"_blank\" ><i class=\"fab fa-facebook-f\"></i></a>
                  ";
                }
                // line 78
                echo "                  ";
                if (($context["twitter_url"] ?? null)) {
                    // line 79
                    echo "                    <a href=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
                    echo "\" class=\"twitter\" target=\"_blank\" ><i class=\"fab fa-twitter\"></i></a>
                  ";
                }
                // line 81
                echo "                  ";
                if (($context["instagram_url"] ?? null)) {
                    // line 82
                    echo "                    <a href=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram_url"] ?? null)), "html", null, true);
                    echo "\" class=\"instagram\" target=\"_blank\" ><i class=\"fab fab fa-instagram\"></i></a>
                  ";
                }
                // line 84
                echo "                  ";
                if (($context["linkedin_url"] ?? null)) {
                    // line 85
                    echo "                    <a href=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin_url"] ?? null)), "html", null, true);
                    echo "\" class=\"linkedin\" target=\"_blank\"><i class=\"fab fa-linkedin-in\"></i></a>
                  ";
                }
                // line 87
                echo "                  ";
                if (($context["rss_url"] ?? null)) {
                    // line 88
                    echo "                    <a href=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["rss_url"] ?? null)), "html", null, true);
                    echo "\" class=\"rss\" target=\"_blank\" ><i class=\"fa fa-rss\"></i></a>
                  ";
                }
                // line 90
                echo "                </div>
              ";
            }
            // line 92
            echo "            </div>
          ";
        }
        // line 94
        echo "          
          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#main-navigation\">
            <i class=\"fas fa-bars\"></i>
          </button>

          <div class=\"main-menu-toggle\">
            ";
        // line 100
        if ($this->getAttribute(($context["page"] ?? null), "primary_menu", [])) {
            // line 101
            echo "              <div class=\"lhs-menu-container\">
                ";
            // line 102
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
              </div>
            ";
        }
        // line 105
        echo "          </div>

        </div>
      </div>
      <div class=\"col-md-9 content-wrapper-col\">
        <div class=\"content-wrapper\">

        ";
        // line 112
        if (($context["show_slideshow"] ?? null)) {
            // line 113
            echo "          <div class=\"flexslider\">
            <ul class=\"slides\">
              ";
            // line 115
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["slider_content"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["slider_contents"]) {
                // line 116
                echo "                ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed($context["slider_contents"]));
                echo "
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider_contents'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 118
            echo "            </ul>
          </div>
        ";
        }
        // line 121
        echo "
        <div class=\"content-container\">

          ";
        // line 124
        if ($this->getAttribute(($context["page"] ?? null), "top_message", [])) {
            // line 125
            echo "            <div class=\"top-block\">
              <div class=\"row\">
                ";
            // line 127
            if ($this->getAttribute(($context["page"] ?? null), "top_message", [])) {
                // line 128
                echo "                  <div class=\"col-md\">
                    ";
                // line 129
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_message", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 132
            echo "              </div>
            </div>
          ";
        }
        // line 135
        echo "
          <!-- Start: Top widget -->
          ";
        // line 137
        if ((($this->getAttribute(($context["page"] ?? null), "topwidget_first", []) || $this->getAttribute(($context["page"] ?? null), "topwidget_second", [])) || $this->getAttribute(($context["page"] ?? null), "topwidget_third", []))) {
            // line 138
            echo "            <div class=\"topwidget\" id=\"topwidget\">
              <div class=\"\">
                ";
            // line 140
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_title", [])) {
                // line 141
                echo "                  <div class=\"custom-block-title\" >
                    ";
                // line 142
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_title", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 145
            echo "                  <div class=\"row topwidget-list clearfix\">
                    ";
            // line 146
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_first", [])) {
                // line 147
                echo "                      <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_first", [])), "html", null, true);
                echo "</div>
                    ";
            }
            // line 149
            echo "                    ";
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_second", [])) {
                // line 150
                echo "                      <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_second", [])), "html", null, true);
                echo "</div>
                    ";
            }
            // line 152
            echo "                    ";
            if ($this->getAttribute(($context["page"] ?? null), "topwidget_third", [])) {
                // line 153
                echo "                      <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["topwidget_class"] ?? null)), "html", null, true);
                echo ">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topwidget_third", [])), "html", null, true);
                echo "</div>
                    ";
            }
            // line 155
            echo "                  </div>
              </div>
            </div>
          ";
        }
        // line 159
        echo "          <!--End: widget -->

          <!--Start: Highlighted -->
          ";
        // line 162
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 163
            echo "            <div class=\"highlighted\">
              <div class=\"container\">
                ";
            // line 165
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
              </div>
            </div>
          ";
        }
        // line 169
        echo "          <!--End: widget -->

          <!--Start: Top Message -->
          ";
        // line 172
        if ($this->getAttribute(($context["page"] ?? null), "topmessage", [])) {
            // line 173
            echo "            <div class=\"top-message\">
              <div class=\"\">
                ";
            // line 175
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topmessage", [])), "html", null, true);
            echo "
              </div>
            </div>
          ";
        }
        // line 179
        echo "          <!--End: widget -->

          <!--Start: Title -->
          ";
        // line 182
        if (($this->getAttribute(($context["page"] ?? null), "page_title", []) &&  !($context["is_front"] ?? null))) {
            // line 183
            echo "            <div id=\"page-title\">
              <div id=\"page-title-inner\">
                ";
            // line 185
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_title", [])), "html", null, true);
            echo "
              </div>
            </div>
          ";
        }
        // line 189
        echo "          <!--End: widget -->

          <div class=\"main-content\">
            <div class=\"\">

              ";
        // line 194
        if ( !($context["is_front"] ?? null)) {
            // line 195
            echo "                ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
            echo "
              ";
        }
        // line 197
        echo "
              <div class=\"row layout\">

                ";
        // line 200
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 201
            echo "                  <div class=\"col-sm-3\">
                    <div class=\"sidebar\">
                      ";
            // line 203
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
                    </div>
                  </div>
                ";
        }
        // line 207
        echo "
                ";
        // line 208
        if (($this->getAttribute(($context["page"] ?? null), "content", []) || ($context["show_slideshow"] ?? null))) {
            // line 209
            echo "                  <div class=\"col-sm\">

                    <div class=\"content_layout\">
                      ";
            // line 212
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
                    </div>

                  </div>
                ";
        }
        // line 217
        echo "
                ";
        // line 218
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 219
            echo "                  <div class=\"col-sm-3\">
                    <div class=\"sidebar\">
                      ";
            // line 221
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
                    </div>
                  </div>
                ";
        }
        // line 225
        echo "
              </div>

            </div>
          </div>
          <!--End: widget -->

          <!-- Start: Features -->
          ";
        // line 233
        if ((($this->getAttribute(($context["page"] ?? null), "features_first", []) || $this->getAttribute(($context["page"] ?? null), "features_second", [])) || $this->getAttribute(($context["page"] ?? null), "features_third", []))) {
            // line 234
            echo "
            <div class=\"features\">
              <div class=\"\">

                ";
            // line 238
            if ($this->getAttribute(($context["page"] ?? null), "features_title", [])) {
                // line 239
                echo "                  <div class=\"custom-block-title\" >
                    ";
                // line 240
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_title", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 243
            echo "
                <div class=\"row\">

                  ";
            // line 246
            if ($this->getAttribute(($context["page"] ?? null), "features_first", [])) {
                // line 247
                echo "                    <div class=\"col-sm\">
                      ";
                // line 248
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_first", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 251
            echo "                  
                  ";
            // line 252
            if ($this->getAttribute(($context["page"] ?? null), "features_second", [])) {
                // line 253
                echo "                    <div class=\"col-sm\">
                      ";
                // line 254
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_second", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 257
            echo "
                  ";
            // line 258
            if ($this->getAttribute(($context["page"] ?? null), "features_third", [])) {
                // line 259
                echo "                    <div class=\"col-sm\">
                      ";
                // line 260
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_third", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 263
            echo "
                </div>
              </div>
            </div>

          ";
        }
        // line 269
        echo "          <!--End: widget -->

          <!-- Start: Updates widgets -->
          ";
        // line 272
        if ((($this->getAttribute(($context["page"] ?? null), "updates_first", []) || $this->getAttribute(($context["page"] ?? null), "updates_second", [])) || $this->getAttribute(($context["page"] ?? null), "updates_third", []))) {
            // line 273
            echo "
            <div class=\"updates\" id=\"updates\">    
              <div class=\"\">

                ";
            // line 277
            if ($this->getAttribute(($context["page"] ?? null), "updates_title", [])) {
                // line 278
                echo "                  <div class=\"custom-block-title\" >
                    ";
                // line 279
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_title", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 282
            echo "
                <div class=\"row\">
                  ";
            // line 284
            if ($this->getAttribute(($context["page"] ?? null), "updates_first", [])) {
                // line 285
                echo "                    <div class=\"col-sm\">
                      ";
                // line 286
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_first", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 288
            echo "              
                  ";
            // line 289
            if ($this->getAttribute(($context["page"] ?? null), "updates_second", [])) {
                // line 290
                echo "                    <div class=\"col-sm\">
                      ";
                // line 291
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_second", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 293
            echo "            
                  ";
            // line 294
            if ($this->getAttribute(($context["page"] ?? null), "updates_third", [])) {
                // line 295
                echo "                    <div class=\"col-sm\">
                      ";
                // line 296
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_third", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 299
            echo "                  ";
            if ($this->getAttribute(($context["page"] ?? null), "updates_forth", [])) {
                // line 300
                echo "                    <div class=\"col-sm\">
                      ";
                // line 301
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "updates_forth", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 304
            echo "                </div>
              </div>
            </div>

          ";
        }
        // line 309
        echo "          <!--End: widget -->

          <!-- Start: Services widgets -->
          ";
        // line 312
        if (((($this->getAttribute(($context["page"] ?? null), "services_first", []) || $this->getAttribute(($context["page"] ?? null), "services_second", [])) || $this->getAttribute(($context["page"] ?? null), "services_third", [])) || $this->getAttribute(($context["page"] ?? null), "services_forth", []))) {
            // line 313
            echo "            <div class=\"services\" id=\"services\">    
              <div class=\"\">
                ";
            // line 315
            if ($this->getAttribute(($context["page"] ?? null), "services_title", [])) {
                // line 316
                echo "                  <div class=\"custom-block-title\" >
                    ";
                // line 317
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services_title", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 320
            echo "                <div class=\"row services-list\">
                
                  ";
            // line 322
            if ($this->getAttribute(($context["page"] ?? null), "services_first", [])) {
                // line 323
                echo "                    <div class=\"col-sm\">
                      ";
                // line 324
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services_first", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 327
            echo "                  
                  ";
            // line 328
            if ($this->getAttribute(($context["page"] ?? null), "services_second", [])) {
                // line 329
                echo "                    <div class=\"col-sm\">
                      ";
                // line 330
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services_second", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 333
            echo "                      
                  ";
            // line 334
            if ($this->getAttribute(($context["page"] ?? null), "services_third", [])) {
                // line 335
                echo "                    <div class=\"col-sm\">
                      ";
                // line 336
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services_third", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 339
            echo "                  
                  ";
            // line 340
            if ($this->getAttribute(($context["page"] ?? null), "services_forth", [])) {
                // line 341
                echo "                    <div class=\"col-sm\">
                      ";
                // line 342
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services_forth", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 345
            echo "                  
                </div>
              </div>
            </div>

          ";
        }
        // line 351
        echo "          <!--End: widget -->


          <!-- Start: Bottom widgets -->
          ";
        // line 355
        if (((($this->getAttribute(($context["page"] ?? null), "bottom_first", []) || $this->getAttribute(($context["page"] ?? null), "bottom_second", [])) || $this->getAttribute(($context["page"] ?? null), "bottom_third", [])) || $this->getAttribute(($context["page"] ?? null), "bottom_forth", []))) {
            // line 356
            echo "
            <div class=\"bottom-widget\" id=\"bottom-widget\">    
              <div class=\"\">

                ";
            // line 360
            if ($this->getAttribute(($context["page"] ?? null), "bottom_title", [])) {
                // line 361
                echo "                  <div class=\"custom-block-title\" >
                    ";
                // line 362
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_title", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 365
            echo "
                <div class=\"row\">
                      
                  ";
            // line 368
            if ($this->getAttribute(($context["page"] ?? null), "bottom_first", [])) {
                // line 369
                echo "                    <div class=\"col-sm\">
                      ";
                // line 370
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_first", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 373
            echo "                  
                  ";
            // line 374
            if ($this->getAttribute(($context["page"] ?? null), "bottom_second", [])) {
                // line 375
                echo "                    <div class=\"col-sm\">
                      ";
                // line 376
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_second", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 379
            echo "
                  ";
            // line 380
            if ($this->getAttribute(($context["page"] ?? null), "bottom_third", [])) {
                // line 381
                echo "                    <div class=\"col-sm\">
                      ";
                // line 382
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_third", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 385
            echo "                  
                  ";
            // line 386
            if ($this->getAttribute(($context["page"] ?? null), "bottom_forth", [])) {
                // line 387
                echo "                    <div class=\"col-sm\">
                      ";
                // line 388
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "bottom_forth", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 391
            echo "
                </div>
              </div>
            </div>

          ";
        }
        // line 397
        echo "          <!--End: widgets -->


          <!-- Start: Footer widgets -->
          ";
        // line 401
        if ((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", []))) {
            // line 402
            echo "
            <div class=\"footer\" id=\"footer\">
              <div class=\"\">

                ";
            // line 406
            if ($this->getAttribute(($context["page"] ?? null), "footer_title", [])) {
                // line 407
                echo "                  <div class=\"custom-block-title\" >
                    ";
                // line 408
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_title", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 411
            echo "
                <div class=\"row\">

                  ";
            // line 414
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
                // line 415
                echo "                    <div class=\"col-sm\">
                      ";
                // line 416
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 419
            echo "
                  ";
            // line 420
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
                // line 421
                echo "                    <div class=\"col-sm\">
                      ";
                // line 422
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 425
            echo "                  
                  ";
            // line 426
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
                // line 427
                echo "                    <div class=\"col-sm\">
                      ";
                // line 428
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
                echo "
                    </div>
                  ";
            }
            // line 431
            echo "
                </div>
              </div>
            </div>

          ";
        }
        // line 437
        echo "          <!--End: widgets -->

        </div>

        <!-- Start: Copyright -->
        <div class=\"copyright\">
            <div class=\"\">
              <span>Copyright © ";
        // line 444
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo ". All rights reserved.</span>
              ";
        // line 445
        if (($context["show_credit_link"] ?? null)) {
            // line 446
            echo "                <span class=\"credit-link\">Designed By <a href=\"https://www.zymphonies.com\" target=\"_blank\">Zymphonies</a></span>
              ";
        }
        // line 448
        echo "          </div>
        </div>
        <!--End: widget -->

        </div>
      </div>
    </div>

  </div>
</div>



  


  



";
    }

    public function getTemplateName()
    {
        return "themes/hope_zymphonies_theme/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  840 => 448,  836 => 446,  834 => 445,  830 => 444,  821 => 437,  813 => 431,  807 => 428,  804 => 427,  802 => 426,  799 => 425,  793 => 422,  790 => 421,  788 => 420,  785 => 419,  779 => 416,  776 => 415,  774 => 414,  769 => 411,  763 => 408,  760 => 407,  758 => 406,  752 => 402,  750 => 401,  744 => 397,  736 => 391,  730 => 388,  727 => 387,  725 => 386,  722 => 385,  716 => 382,  713 => 381,  711 => 380,  708 => 379,  702 => 376,  699 => 375,  697 => 374,  694 => 373,  688 => 370,  685 => 369,  683 => 368,  678 => 365,  672 => 362,  669 => 361,  667 => 360,  661 => 356,  659 => 355,  653 => 351,  645 => 345,  639 => 342,  636 => 341,  634 => 340,  631 => 339,  625 => 336,  622 => 335,  620 => 334,  617 => 333,  611 => 330,  608 => 329,  606 => 328,  603 => 327,  597 => 324,  594 => 323,  592 => 322,  588 => 320,  582 => 317,  579 => 316,  577 => 315,  573 => 313,  571 => 312,  566 => 309,  559 => 304,  553 => 301,  550 => 300,  547 => 299,  541 => 296,  538 => 295,  536 => 294,  533 => 293,  527 => 291,  524 => 290,  522 => 289,  519 => 288,  513 => 286,  510 => 285,  508 => 284,  504 => 282,  498 => 279,  495 => 278,  493 => 277,  487 => 273,  485 => 272,  480 => 269,  472 => 263,  466 => 260,  463 => 259,  461 => 258,  458 => 257,  452 => 254,  449 => 253,  447 => 252,  444 => 251,  438 => 248,  435 => 247,  433 => 246,  428 => 243,  422 => 240,  419 => 239,  417 => 238,  411 => 234,  409 => 233,  399 => 225,  392 => 221,  388 => 219,  386 => 218,  383 => 217,  375 => 212,  370 => 209,  368 => 208,  365 => 207,  358 => 203,  354 => 201,  352 => 200,  347 => 197,  341 => 195,  339 => 194,  332 => 189,  325 => 185,  321 => 183,  319 => 182,  314 => 179,  307 => 175,  303 => 173,  301 => 172,  296 => 169,  289 => 165,  285 => 163,  283 => 162,  278 => 159,  272 => 155,  264 => 153,  261 => 152,  253 => 150,  250 => 149,  242 => 147,  240 => 146,  237 => 145,  231 => 142,  228 => 141,  226 => 140,  222 => 138,  220 => 137,  216 => 135,  211 => 132,  205 => 129,  202 => 128,  200 => 127,  196 => 125,  194 => 124,  189 => 121,  184 => 118,  175 => 116,  171 => 115,  167 => 113,  165 => 112,  156 => 105,  150 => 102,  147 => 101,  145 => 100,  137 => 94,  133 => 92,  129 => 90,  123 => 88,  120 => 87,  114 => 85,  111 => 84,  105 => 82,  102 => 81,  96 => 79,  93 => 78,  87 => 76,  85 => 75,  82 => 74,  80 => 73,  77 => 72,  75 => 71,  72 => 70,  66 => 68,  64 => 67,  55 => 60,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/hope_zymphonies_theme/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\hope_zymphonies_theme\\templates\\layout\\page.html.twig");
    }
}
