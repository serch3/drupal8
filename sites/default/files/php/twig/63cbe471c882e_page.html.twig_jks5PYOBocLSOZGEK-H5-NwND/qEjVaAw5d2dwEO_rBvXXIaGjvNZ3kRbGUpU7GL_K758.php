<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/alif_zymphonies_theme/templates/layout/page.html.twig */
class __TwigTemplate_3228c7e4070850d9455264d60075b3c320a415db10274efbf46ae329e291086a extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 62, "for" => 105];
        $filters = ["escape" => 65, "raw" => 106];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 60
        echo "
<!--Start: Top Message -->
";
        // line 62
        if ($this->getAttribute(($context["page"] ?? null), "top_message", [])) {
            // line 63
            echo "  <div class=\"container-\">
    <div class=\"top-message\">
      ";
            // line 65
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_message", [])), "html", null, true);
            echo "
    </div>
  </div>
";
        }
        // line 69
        echo "<!--End: Top Message -->


<!-- Start: Top Bar -->

<div class=\"header\">
  <div class=\"container-\">
    <div class=\"row\">
      <div class=\"navbar-header col-md\">
        ";
        // line 78
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 79
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
        ";
        }
        // line 81
        echo "        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#main-navigation\">
          <i class=\"fas fa-bars\"></i>
        </button>
        ";
        // line 84
        if ($this->getAttribute(($context["page"] ?? null), "primary_menu", [])) {
            // line 85
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
        ";
        }
        // line 87
        echo "      </div>
    </div>
  </div>
</div>

<div class=\"container\">

  <!-- Start: Header -->

  
  <!-- End: Header -->


  <!-- Start: Slides -->
  ";
        // line 101
        if ((($context["is_front"] ?? null) && ($context["show_slideshow"] ?? null))) {
            // line 102
            echo "    <div class=\"container-\">
      <div class=\"flexslider\">
        <ul class=\"slides\">
          ";
            // line 105
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["slider_content"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["slider_contents"]) {
                // line 106
                echo "            ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed($context["slider_contents"]));
                echo "
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slider_contents'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 108
            echo "        </ul>
      </div>
    </div>
  ";
        }
        // line 112
        echo "  <!-- End: Slides -->

  <div class=\"regions-group-\">

    ";
        // line 116
        if (($this->getAttribute(($context["page"] ?? null), "topblock1_image", []) || $this->getAttribute(($context["page"] ?? null), "topblock1_desc", []))) {
            // line 117
            echo "      <div class=\"topblock regions-group\">
        <div class=\"row topwidget-list clearfix\">
          ";
            // line 119
            if ($this->getAttribute(($context["page"] ?? null), "topblock1_image", [])) {
                // line 120
                echo "            <div class=\"col-sm-4 block-image\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topblock1_image", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 122
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "topblock1_desc1", [])) {
                // line 123
                echo "            <div class=\"col-sm block-content-wrap\">
              <div class=\"block-content\">";
                // line 124
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topblock1_desc1", [])), "html", null, true);
                echo "</div>
            </div>
          ";
            }
            // line 127
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "topblock1_desc2", [])) {
                // line 128
                echo "            <div class=\"col-sm block-content-wrap\">
              <div class=\"block-content\">";
                // line 129
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topblock1_desc2", [])), "html", null, true);
                echo "</div>
            </div>
          ";
            }
            // line 132
            echo "        </div>
      </div>
    ";
        }
        // line 135
        echo "  
    ";
        // line 136
        if (($this->getAttribute(($context["page"] ?? null), "topblock2_image", []) || $this->getAttribute(($context["page"] ?? null), "topblock2_desc", []))) {
            // line 137
            echo "      <div class=\"topblock regions-group\">
        <div class=\"row topwidget-list clearfix\">
          ";
            // line 139
            if ($this->getAttribute(($context["page"] ?? null), "topblock2_desc", [])) {
                // line 140
                echo "            <div class=\"col-sm block-content-wrap\">
              <div class=\"block-content\">";
                // line 141
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topblock2_desc", [])), "html", null, true);
                echo "</div>
            </div>
          ";
            }
            // line 144
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "topblock2_image", [])) {
                // line 145
                echo "            <div class=\"col-sm-4 block-image\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "topblock2_image", [])), "html", null, true);
                echo "</div>
          ";
            }
            // line 147
            echo "        </div>
      </div>
    ";
        }
        // line 150
        echo "
  </div>

      
  <!--Start: Highlighted -->
  ";
        // line 155
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 156
            echo "    <div class=\"highlighted\">
      <div class=\"container-\">
        ";
            // line 158
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
      </div>
    </div>
  ";
        }
        // line 162
        echo "  <!--End: Highlighted -->
  

  <!--Start: Title -->
  ";
        // line 166
        if (($this->getAttribute(($context["page"] ?? null), "page_title", []) &&  !($context["is_front"] ?? null))) {
            // line 167
            echo "    <div id=\"page-title\">
      <div id=\"page-title-inner\">
        <div class=\"container-\">
          ";
            // line 170
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_title", [])), "html", null, true);
            echo "
        </div>
      </div>
    </div>
  ";
        }
        // line 175
        echo "  <!--End: Title -->


  <div class=\"main-content regions-group\">
    <div class=\"container-\">
      <div class=\"\">
        <!--Start: Breadcrumb -->
        ";
        // line 182
        if ( !($context["is_front"] ?? null)) {
            // line 183
            echo "          <div class=\"row\">
            <div class=\"col-md-12\">";
            // line 184
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
            echo "</div>
          </div>
        ";
        }
        // line 187
        echo "        <!--End: Breadcrumb -->
        <div class=\"row layout\">
          <!--- Start: Left SideBar -->
          ";
        // line 190
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 191
            echo "            <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarfirst"] ?? null)), "html", null, true);
            echo ">
              <div class=\"sidebar\">
                ";
            // line 193
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
              </div>
            </div>
          ";
        }
        // line 197
        echo "          <!-- End Left SideBar -->
          <!--- Start Content -->
          ";
        // line 199
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 200
            echo "            <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["contentlayout"] ?? null)), "html", null, true);
            echo ">
              <div class=\"content_layout\">
                ";
            // line 202
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
              </div>              
            </div>
          ";
        }
        // line 206
        echo "          <!-- End: Content -->
          <!-- Start: Right SideBar -->
          ";
        // line 208
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 209
            echo "            <div class=";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebarsecond"] ?? null)), "html", null, true);
            echo ">
              <div class=\"sidebar\">
                ";
            // line 211
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
              </div>
            </div>
          ";
        }
        // line 215
        echo "          <!-- End: Right SideBar -->
        </div>
      </div>
    </div>
  </div>
  <!-- End: Main content -->


  ";
        // line 223
        if ($this->getAttribute(($context["page"] ?? null), "products", [])) {
            // line 224
            echo "    <div class=\"products regions-group\">
      ";
            // line 225
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "products", [])), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 228
        echo "

  <!-- Start: Features -->
  ";
        // line 231
        if ((($this->getAttribute(($context["page"] ?? null), "features_first", []) || $this->getAttribute(($context["page"] ?? null), "features_second", [])) || $this->getAttribute(($context["page"] ?? null), "features_third", []))) {
            // line 232
            echo "    <div class=\"features regions-group\">
      <div class=\"container-\">
        ";
            // line 234
            if ($this->getAttribute(($context["page"] ?? null), "features_title", [])) {
                // line 235
                echo "          <h2 class=\"custom-block-title\" >
            ";
                // line 236
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_title", [])), "html", null, true);
                echo "
          </h2>
        ";
            }
            // line 239
            echo "        <div class=\"row features-list\">
          ";
            // line 240
            if ($this->getAttribute(($context["page"] ?? null), "features_first", [])) {
                // line 241
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_first_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 242
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 245
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "features_second", [])) {
                // line 246
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 247
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 250
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "features_third", [])) {
                // line 251
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["features_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 252
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "features_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 255
            echo "        </div>
      </div>
    </div>
  ";
        }
        // line 259
        echo "  <!--End: Features -->


  <!-- Start: Footer widgets -->
  ";
        // line 263
        if ((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", []))) {
            // line 264
            echo "    <div class=\"footer regions-group\" id=\"footer\">
      <div class=\"container-\">
        ";
            // line 266
            if ($this->getAttribute(($context["page"] ?? null), "footer_title", [])) {
                // line 267
                echo "          <h2 class=\"custom-block-title\" >
            ";
                // line 268
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_title", [])), "html", null, true);
                echo "
          </h2>
        ";
            }
            // line 271
            echo "        <div class=\"row\">
          ";
            // line 272
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
                // line 273
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 274
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 277
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
                // line 278
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 279
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 282
            echo "          ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
                // line 283
                echo "            <div class = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_class"] ?? null)), "html", null, true);
                echo ">
              ";
                // line 284
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
                echo "
            </div>
          ";
            }
            // line 287
            echo "        </div>
      </div>
    </div>
  ";
        }
        // line 291
        echo "  <!--End: Footer widgets -->


  <!-- Start: Copyright -->
  <div class=\"copyright regions-group\">
    <div class=\"container-\">
      ";
        // line 297
        if (($context["show_credit_link"] ?? null)) {
            // line 298
            echo "        <span class=\"credit-link\"><a href=\"https://www.zymphonies.com\" target=\"_blank\">Zymphonies</a> صمم بواسطة</span>
      ";
        }
        // line 300
        echo "      <span>";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["copyright_content"] ?? null)), "html", null, true);
        echo "</span>
    </div>
  </div>
  <!-- End: Copyright -->

</div>





";
    }

    public function getTemplateName()
    {
        return "themes/alif_zymphonies_theme/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  528 => 300,  524 => 298,  522 => 297,  514 => 291,  508 => 287,  502 => 284,  497 => 283,  494 => 282,  488 => 279,  483 => 278,  480 => 277,  474 => 274,  469 => 273,  467 => 272,  464 => 271,  458 => 268,  455 => 267,  453 => 266,  449 => 264,  447 => 263,  441 => 259,  435 => 255,  429 => 252,  424 => 251,  421 => 250,  415 => 247,  410 => 246,  407 => 245,  401 => 242,  396 => 241,  394 => 240,  391 => 239,  385 => 236,  382 => 235,  380 => 234,  376 => 232,  374 => 231,  369 => 228,  363 => 225,  360 => 224,  358 => 223,  348 => 215,  341 => 211,  335 => 209,  333 => 208,  329 => 206,  322 => 202,  316 => 200,  314 => 199,  310 => 197,  303 => 193,  297 => 191,  295 => 190,  290 => 187,  284 => 184,  281 => 183,  279 => 182,  270 => 175,  262 => 170,  257 => 167,  255 => 166,  249 => 162,  242 => 158,  238 => 156,  236 => 155,  229 => 150,  224 => 147,  218 => 145,  215 => 144,  209 => 141,  206 => 140,  204 => 139,  200 => 137,  198 => 136,  195 => 135,  190 => 132,  184 => 129,  181 => 128,  178 => 127,  172 => 124,  169 => 123,  166 => 122,  160 => 120,  158 => 119,  154 => 117,  152 => 116,  146 => 112,  140 => 108,  131 => 106,  127 => 105,  122 => 102,  120 => 101,  104 => 87,  98 => 85,  96 => 84,  91 => 81,  85 => 79,  83 => 78,  72 => 69,  65 => 65,  61 => 63,  59 => 62,  55 => 60,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/alif_zymphonies_theme/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\alif_zymphonies_theme\\templates\\layout\\page.html.twig");
    }
}
