<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/particles_orange/templates/layout/page.html.twig */
class __TwigTemplate_66c0dd4fd934d6c8031dd245c479cdd299e2bbf35a50027f67de173ffa21eb58 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'action_links' => [$this, 'block_action_links'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 55, "block" => 109, "set" => 124];
        $filters = ["escape" => 57, "striptags" => 89, "t" => 91];
        $functions = ["attach_library" => 83];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'block', 'set'],
                ['escape', 'striptags', 't'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 50
        echo "<div id=\"page\">
\t<div id=\"top\"></div>
    <header id=\"masthead\" role=\"banner\" class=\"site-header\">
      <div class=\"container\">
        <div class=\"row\">
          ";
        // line 55
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 56
            echo "          <div id=\"logo\" class=\"col-sm-4 site-branding\">
            ";
            // line 57
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
          </div>
          ";
        }
        // line 60
        echo "          <div class=\"col-sm-8 mainmenu\">
            <nav id=\"navigation\" role=\"navigation\">
              <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#main-navigation\">
                  <span class=\"icon-bar\"></span>
                  <span class=\"icon-bar\"></span>
                  <span class=\"icon-bar\"></span>
                </button>
                <!--End Navigation -->
                <div id=\"main-menu\">
                  ";
        // line 70
        if ($this->getAttribute(($context["page"] ?? null), "main_navigation", [])) {
            // line 71
            echo "                  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_navigation", [])), "html", null, true);
            echo "
                  ";
        }
        // line 73
        echo "                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>

    ";
        // line 81
        if (($context["is_front"] ?? null)) {
            // line 82
            echo "    ";
            if (($context["banner_display"] ?? null)) {
                // line 83
                echo "    ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("particles_orange/particle-style"), "html", null, true);
                echo "
      <div class=\"site-banner\">
        <div class=\"container-fluid\">
          <img class=\"img-bann\" src=\"";
                // line 86
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["banner_image"] ?? null)), "html", null, true);
                echo "\"/>
          ";
                // line 87
                if ((((($context["banner_head"] ?? null) || ($context["banner_desc"] ?? null)) || ($context["banner_url"] ?? null)) || ($context["banner_url_text"] ?? null))) {
                    // line 88
                    echo "          <div class=\"flex-caption\">
            <h2>";
                    // line 89
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["banner_head"] ?? null))), "html", null, true);
                    echo "</h2>
            <p>";
                    // line 90
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["banner_desc"] ?? null))), "html", null, true);
                    echo "</p>
            <a class=\"frmore\" href=\"";
                    // line 91
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, strip_tags($this->sandbox->ensureToStringAllowed(($context["banner_url"] ?? null))), "html", null, true);
                    echo "\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t($this->sandbox->ensureToStringAllowed(($context["banner_url_text"] ?? null))));
                    echo "</a>
          </div>
          <div id=\"particleCanvas-Orange\" class=\"e-particles-orange\"></div>
          <div id=\"particleCanvas-Blue\" class=\"e-particles-blue\"></div>
          ";
                }
                // line 96
                echo "        </div>
      </div>
    ";
            }
            // line 99
            echo "    ";
        }
        // line 100
        echo "
    ";
        // line 102
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "top_banner", [])) {
            // line 103
            echo "    <div id=\"top-banner\">
      ";
            // line 104
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_banner", [])), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 107
        echo "
  ";
        // line 109
        echo "  ";
        $this->displayBlock('main', $context, $blocks);
        // line 172
        echo "

  <div id=\"bottom\">
    ";
        // line 175
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", [])) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", []))) {
            // line 176
            echo "    <div class=\"container\">
      <div class=\"row\">
        ";
            // line 178
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
                // line 179
                echo "        <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
          ";
                // line 180
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
                echo "
        </div>
        ";
            }
            // line 183
            echo "        ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
                // line 184
                echo "        <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
          ";
                // line 185
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
                echo "
        </div>
        ";
            }
            // line 188
            echo "        ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
                // line 189
                echo "        <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
          ";
                // line 190
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
                echo "
        </div>
        ";
            }
            // line 193
            echo "        ";
            if ($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])) {
                // line 194
                echo "        <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ("footer-block col-sm-" . $this->sandbox->ensureToStringAllowed(($context["footer_col"] ?? null))), "html", null, true);
                echo "\">
          ";
                // line 195
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])), "html", null, true);
                echo "
        </div>
        ";
            }
            // line 198
            echo "      </div>
    </div>
    ";
        }
        // line 201
        echo "  </div>
  <footer id=\"colophon\" class=\"site-footer\" role=\"contentinfo\">
    <div class=\"container\">
      ";
        // line 204
        if (($context["display_scroller"] ?? null)) {
            // line 205
            echo "      ";
            if ((($context["scroller"] ?? null) == "Right")) {
                // line 206
                echo "      <div class=\"scroll-top scroll-right\" style=\"display: block;\">
        <a href=\"#\"><i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i></a>
      </div>
      ";
            } elseif ((            // line 209
($context["scroller"] ?? null) == "Left")) {
                // line 210
                echo "      <div class=\"scroll-top scroll-left\" style=\"display: block;\">
        <a href=\"#\"><i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i></a>
      </div>
      ";
            }
            // line 214
            echo "      ";
        }
        // line 215
        echo "      <div class=\"row\">
        ";
        // line 216
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 217
            echo "        <div id=\"footer-block\">
          ";
            // line 218
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
        </div>
        ";
        }
        // line 221
        echo "        <div class=\"bottom-footer\">
          <div class=\"social-icons\">
            ";
        // line 223
        if (($context["show_social_icon"] ?? null)) {
            // line 224
            echo "            <div class=\"social-media\">
              ";
            // line 225
            if (($context["facebook_url"] ?? null)) {
                // line 226
                echo "              <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["facebook_url"] ?? null)), "html", null, true);
                echo "\"  class=\"facebook\" target=\"_blank\" ><i class=\"fa fa-facebook\"></i></a>
              ";
            }
            // line 228
            echo "              ";
            if (($context["instagram_url"] ?? null)) {
                // line 229
                echo "              <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["instagram_url"] ?? null)), "html", null, true);
                echo "\"  class=\"instagram\" target=\"_blank\" ><i class=\"fa fa-instagram\"></i></a>
              ";
            }
            // line 231
            echo "              ";
            if (($context["twitter_url"] ?? null)) {
                // line 232
                echo "              <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["twitter_url"] ?? null)), "html", null, true);
                echo "\" class=\"twitter\" target=\"_blank\" ><i class=\"fa fa-twitter\"></i></a>
              ";
            }
            // line 234
            echo "              ";
            if (($context["youtube_url"] ?? null)) {
                // line 235
                echo "              <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["youtube_url"] ?? null)), "html", null, true);
                echo "\" class=\"youtube\" target=\"_blank\" ><i class=\"fa fa-youtube-play\"></i></a>
              ";
            }
            // line 237
            echo "              ";
            if (($context["linkedin_url"] ?? null)) {
                // line 238
                echo "              <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["linkedin_url"] ?? null)), "html", null, true);
                echo "\" class=\"linkedin\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a>
              ";
            }
            // line 240
            echo "            </div>
            ";
        }
        // line 242
        echo "          </div>
          <div class=\"copyright-text\">
            ";
        // line 244
        if (($context["show_copyright"] ?? null)) {
            // line 245
            echo "            <p>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["copyright_text"] ?? null)), "html", null, true);
            echo "</p>
            ";
        }
        // line 247
        echo "          </div>
        </div>
      </div>
    </div>
  </footer>
</div>";
    }

    // line 109
    public function block_main($context, array $blocks = [])
    {
        // line 110
        echo "  <div role=\"main\" class=\"container js-quickedit-main-content main-container ";
        if ((($context["banner_display"] ?? null) == "0")) {
            echo "ban-hide";
        }
        echo "\">
    <div class=\"row\">

      ";
        // line 114
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 115
            echo "      ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 120
            echo "      ";
        }
        // line 121
        echo "
      ";
        // line 123
        echo "      ";
        // line 124
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 125
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 126
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 127
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 128
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 131
        echo "      <section ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 134
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 135
            echo "        ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 138
            echo "        ";
        }
        // line 139
        echo "
        ";
        // line 141
        echo "        ";
        if (($context["breadcrumb"] ?? null)) {
            // line 142
            echo "        ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 145
            echo "        ";
        }
        // line 146
        echo "
        ";
        // line 148
        echo "        ";
        if (($context["action_links"] ?? null)) {
            // line 149
            echo "        ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 152
            echo "        ";
        }
        // line 153
        echo "
        ";
        // line 155
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 159
        echo "      </section>

      ";
        // line 162
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 163
            echo "      ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 168
            echo "      ";
        }
        // line 169
        echo "    </div>
  </div>
  ";
    }

    // line 115
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 116
        echo "      <aside class=\"col-sm-3\" role=\"complementary\">
        ";
        // line 117
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
      </aside>
      ";
    }

    // line 135
    public function block_highlighted($context, array $blocks = [])
    {
        // line 136
        echo "        <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
        ";
    }

    // line 142
    public function block_breadcrumb($context, array $blocks = [])
    {
        // line 143
        echo "        ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["breadcrumb"] ?? null)), "html", null, true);
        echo "
        ";
    }

    // line 149
    public function block_action_links($context, array $blocks = [])
    {
        // line 150
        echo "        <ul class=\"action-links\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["action_links"] ?? null)), "html", null, true);
        echo "</ul>
        ";
    }

    // line 155
    public function block_content($context, array $blocks = [])
    {
        // line 156
        echo "        <a id=\"main-content\"></a>
        ";
        // line 157
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 163
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 164
        echo "      <aside class=\"col-sm-3\" role=\"complementary\">
        ";
        // line 165
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
      </aside>
      ";
    }

    public function getTemplateName()
    {
        return "themes/particles_orange/templates/layout/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  524 => 165,  521 => 164,  518 => 163,  512 => 157,  509 => 156,  506 => 155,  499 => 150,  496 => 149,  489 => 143,  486 => 142,  479 => 136,  476 => 135,  469 => 117,  466 => 116,  463 => 115,  457 => 169,  454 => 168,  451 => 163,  448 => 162,  444 => 159,  441 => 155,  438 => 153,  435 => 152,  432 => 149,  429 => 148,  426 => 146,  423 => 145,  420 => 142,  417 => 141,  414 => 139,  411 => 138,  408 => 135,  405 => 134,  399 => 131,  397 => 128,  396 => 127,  395 => 126,  394 => 125,  393 => 124,  391 => 123,  388 => 121,  385 => 120,  382 => 115,  379 => 114,  370 => 110,  367 => 109,  358 => 247,  352 => 245,  350 => 244,  346 => 242,  342 => 240,  336 => 238,  333 => 237,  327 => 235,  324 => 234,  318 => 232,  315 => 231,  309 => 229,  306 => 228,  300 => 226,  298 => 225,  295 => 224,  293 => 223,  289 => 221,  283 => 218,  280 => 217,  278 => 216,  275 => 215,  272 => 214,  266 => 210,  264 => 209,  259 => 206,  256 => 205,  254 => 204,  249 => 201,  244 => 198,  238 => 195,  233 => 194,  230 => 193,  224 => 190,  219 => 189,  216 => 188,  210 => 185,  205 => 184,  202 => 183,  196 => 180,  191 => 179,  189 => 178,  185 => 176,  183 => 175,  178 => 172,  175 => 109,  172 => 107,  166 => 104,  163 => 103,  160 => 102,  157 => 100,  154 => 99,  149 => 96,  139 => 91,  135 => 90,  131 => 89,  128 => 88,  126 => 87,  122 => 86,  115 => 83,  112 => 82,  110 => 81,  100 => 73,  94 => 71,  92 => 70,  80 => 60,  74 => 57,  71 => 56,  69 => 55,  62 => 50,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/particles_orange/templates/layout/page.html.twig", "C:\\xampp\\htdocs\\drupal-8.8.12\\themes\\particles_orange\\templates\\layout\\page.html.twig");
    }
}
